<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFatPedidoTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'FAT_PEDIDO';

    /**
     * Run the migrations.
     * @table FAT_PEDIDO
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->unsignedInteger('ID_CLIENTE');
            $table->unsignedInteger('ID_MESA');
            $table->unsignedInteger('ID_RESTAURANTE');
            $table->unsignedInteger('ID_CUPOM_PESSOA');
            $table->char('SITUACAO', 1)->nullable();
            $table->dateTime('EMISSAO')->nullable();
            $table->decimal('TOTAL', 9, 2)->nullable();
            $table->dateTime('DATAFECHAMENTO')->nullable();
            $table->decimal('PERCDESCONTO', 5, 2)->nullable();
            $table->decimal('VALORCUPOM', 10, 2)->nullable();
            $table->text('OBS')->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_CUPOM_PESSOA"], 'fk_FAT_PEDIDO_FAT_CUPOM_PESSOA1_idx');

            $table->index(["ID_RESTAURANTE"], 'fk_FAT_PEDIDO_CRM_PESSOA1_idx');

            $table->index(["ID_MESA"], 'fk_fat_pedido_est_restaurante_mesa1_idx');

            $table->index(["ID_CLIENTE"], 'fk_fat_pedido_fin_cliente1_idx');


            $table->foreign('ID_CLIENTE', 'fk_fat_pedido_fin_cliente1_idx')
                ->references('ID')->on('CRM_PESSOA')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_MESA', 'fk_fat_pedido_est_restaurante_mesa1_idx')
                ->references('ID')->on('EST_RESTAURANTE_MESA')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_RESTAURANTE', 'fk_FAT_PEDIDO_CRM_PESSOA1_idx')
                ->references('ID')->on('CRM_PESSOA')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_CUPOM_PESSOA', 'fk_FAT_PEDIDO_FAT_CUPOM_PESSOA1_idx')
                ->references('ID')->on('FAT_CUPOM_PESSOA')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
