<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFatPedidoPesquisaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'FAT_PEDIDO_PESQUISA';

    /**
     * Run the migrations.
     * @table FAT_PEDIDO_PESQUISA
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->unsignedInteger('ID_PEDIDO');
            $table->char('RESPOSTA1', 1)->nullable();
            $table->char('RESPOSTA2', 1)->nullable();
            $table->char('RESPOSTA3', 1)->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_PEDIDO"], 'fk_fat_pedido_pesquisa_fat_pedido1_idx');


            $table->foreign('ID_PEDIDO', 'fk_fat_pedido_pesquisa_fat_pedido1_idx')
                ->references('ID')->on('FAT_PEDIDO')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
