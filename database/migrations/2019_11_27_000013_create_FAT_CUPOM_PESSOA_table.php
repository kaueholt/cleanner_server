<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFatCupomPessoaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'FAT_CUPOM_PESSOA';

    /**
     * Run the migrations.
     * @table FAT_CUPOM_PESSOA
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->unsignedInteger('ID_CUPOM');
            $table->unsignedInteger('ID_PESSOA');
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_CUPOM"], 'fk_FAT_CUPOM_PESSOA_FAT_CUPOM1_idx');

            $table->index(["ID_PESSOA"], 'fk_FAT_CUPOM_PESSOA_CRM_PESSOA1_idx');


            $table->foreign('ID_CUPOM', 'fk_FAT_CUPOM_PESSOA_FAT_CUPOM1_idx')
                ->references('ID')->on('FAT_CUPOM')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_PESSOA', 'fk_FAT_CUPOM_PESSOA_CRM_PESSOA1_idx')
                ->references('ID')->on('CRM_PESSOA')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
