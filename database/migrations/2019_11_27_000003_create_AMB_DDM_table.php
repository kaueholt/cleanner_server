<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbDdmTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'AMB_DDM';

    /**
     * Run the migrations.
     * @table AMB_DDM
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->string('ALIAS', 25)->nullable();
            $table->integer('NRCAMPO')->nullable();
            $table->string('CAMPO', 45)->nullable();
            $table->string('TIPO', 4)->nullable();
            $table->smallInteger('TAMANHO')->nullable()->default('0');
            $table->smallInteger('DECIMAIS')->nullable()->default('0');
            $table->string('FORMATO', 8)->nullable();
            $table->char('REQUERIDO', 1)->nullable();
            $table->string('PADRAO')->nullable();
            $table->string('LEGENDA', 25)->nullable();
            $table->string('MASCARA', 25)->nullable();
            $table->string('NOMEEXTERNO', 40)->nullable();
            $table->date('ALTERACAO')->nullable();
            $table->text('DESCRICAO')->nullable();
            $table->text('SQL')->nullable();
            $table->text('MENSAGEM')->nullable();
            $table->text('MENU')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
