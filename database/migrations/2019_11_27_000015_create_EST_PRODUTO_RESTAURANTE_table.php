<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstProdutoRestauranteTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'EST_PRODUTO_RESTAURANTE';

    /**
     * Run the migrations.
     * @table EST_PRODUTO_RESTAURANTE
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->unsignedInteger('ID_PRODUTO');
            $table->unsignedInteger('ID_RESTAURANTE');
            $table->char('ATIVO', 1)->nullable();
            $table->decimal('PRECOPROMOCAO', 9, 2)->nullable();
            $table->date('INICIOPROMOCAO')->nullable();
            $table->date('FIMPROMOCAO')->nullable();
            $table->decimal('PRECOVENDA', 14, 2)->nullable();
            $table->text('OBS')->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_PRODUTO"], 'fk_est_produto_restaurante_est_produto1_idx');

            $table->index(["ID_RESTAURANTE"], 'fk_est_produto_restaurante_fin_cliente1_idx');


            $table->foreign('ID_PRODUTO', 'fk_est_produto_restaurante_est_produto1_idx')
                ->references('ID')->on('EST_PRODUTO')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_RESTAURANTE', 'fk_est_produto_restaurante_fin_cliente1_idx')
                ->references('ID')->on('CRM_PESSOA')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
