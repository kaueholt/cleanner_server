<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCrmPessoaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'CRM_PESSOA';

    /**
     * Run the migrations.
     * @table CRM_PESSOA
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->string('NOME', 50)->nullable();
            $table->string('NOMEREDUZIDO', 20)->nullable();
            $table->char('PESSOA', 1)->nullable()->default('F');
            $table->string('CNPJCPF', 14)->nullable();
            $table->string('INSCESTRG', 15)->nullable();
            $table->string('INSCMUNICIPAL', 14)->nullable();
            $table->integer('CEP')->nullable()->default('80000000');
            $table->string('ENDERECO', 60)->nullable();
            $table->string('NUMERO', 7)->nullable()->default('S/N');
            $table->string('COMPLEMENTO', 15)->nullable();
            $table->string('BAIRRO', 20)->nullable();
            $table->string('CIDADE', 40)->nullable();
            $table->string('UF', 2)->nullable()->default('PR');
            $table->string('FONE', 15)->nullable();
            $table->string('CELULAR', 15)->nullable();
            $table->text('EMAIL')->nullable();
            $table->text('HOMEPAGE')->nullable();
            $table->string('CLASSE', 2);
            $table->string('ATIVIDADE', 4);
            $table->string('USERLOGIN', 30)->nullable();
            $table->string('PASSWORD', 30)->nullable();
            $table->text('OBS')->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ATIVIDADE"], 'fk_fin_cliente_fin_atividade1_idx');

            $table->index(["CLASSE"], 'fk_fin_cliente_fin_classe1_idx');


            $table->foreign('ATIVIDADE', 'fk_fin_cliente_fin_atividade1_idx')
                ->references('ATIVIDADE')->on('FIN_ATIVIDADE')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('CLASSE', 'fk_fin_cliente_fin_classe1_idx')
                ->references('CLASSE')->on('FIN_CLASSE')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
