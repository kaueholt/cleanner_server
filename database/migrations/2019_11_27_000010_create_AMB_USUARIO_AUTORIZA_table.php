<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbUsuarioAutorizaTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'AMB_USUARIO_AUTORIZA';

    /**
     * Run the migrations.
     * @table AMB_USUARIO_AUTORIZA
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->unsignedInteger('USUARIO');
            $table->string('PROGRAMA', 50)->nullable();
            $table->string('DIREITOS', 6)->nullable();

            $table->index(["USUARIO"], 'fk_amb_usuario_autoriza_amb_usuario1_idx');


            $table->foreign('USUARIO', 'fk_amb_usuario_autoriza_amb_usuario1_idx')
                ->references('USUARIO')->on('AMB_USUARIO')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
