<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFinAtividadeTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'FIN_ATIVIDADE';

    /**
     * Run the migrations.
     * @table FIN_ATIVIDADE
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->string('ATIVIDADE',4)->primary();
            $table->string('DESCRICAO', 30)->nullable();
            $table->char('TIPO', 1)->nullable()->comment('C - CLIENTE
F - FORNECEDOR
B - COLABORADOR
T - TRANSPORTADOR
A - AMBOS');
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();
});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
