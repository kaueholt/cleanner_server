<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEstGrupoRestauranteTable extends Migration
{
    /**
     * Schema table name to migrate
     * @var string
     */
    public $tableName = 'EST_GRUPO_RESTAURANTE';

    /**
     * Run the migrations.
     * @table EST_GRUPO_RESTAURANTE
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->increments('ID');
            $table->string('GRUPO', 15);
            $table->unsignedInteger('ID_RESTAURANTE');
            $table->char('ATIVO', 1)->nullable();
            $table->timestamp('CREATED_AT')->nullable();
            $table->integer('USERINSERT')->nullable();
            $table->timestamp('UPDATED_AT')->nullable();
            $table->integer('USERUPDATE')->nullable();

            $table->index(["ID_RESTAURANTE"], 'fk_est_grupo_restaurante_fin_cliente1_idx');

            $table->index(["GRUPO"], 'fk_EST_GRUPO_RESTAURANTE_est_grupo1_idx');


            $table->foreign('GRUPO', 'fk_EST_GRUPO_RESTAURANTE_est_grupo1_idx')
                ->references('GRUPO')->on('EST_GRUPO')
                ->onDelete('no action')
                ->onUpdate('no action');

            $table->foreign('ID_RESTAURANTE', 'fk_est_grupo_restaurante_fin_cliente1_idx')
                ->references('ID')->on('CRM_PESSOA')
                ->onDelete('no action')
                ->onUpdate('no action');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
     public function down()
     {
       Schema::dropIfExists($this->tableName);
     }
}
