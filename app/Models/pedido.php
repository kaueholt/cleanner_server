<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class pedido
 *
 * @property int $ID
 * @property int $ID_CLIENTE
 * @property int $ID_COMISSAO
 * @property int $ID_CUPOM_PESSOA
 * @property int $ID_ENDERECO_PESSOA
 * @property int $ID_GAVETA_ARMARIO_ENTREGA
 * @property int $ID_GAVETA_ARMARIO_RETIRADA
 * @property int $ID_PRAZO
 * @property int $ID_PESSOA_PESSOAPRESTADOR
 * @property int $ID_STATUS
 * @property int $ID_TIPO_SERVICO_ROUPAS
 * @property int $ID_TIPO_SERVICO_FRETE
 * @property string $COR_HEXADECIMAL
 * @property string $COR_DESCRICAO
 * @property string $INVOICE_IUGU
 * @property Carbon $DATAFECHAMENTO
 * @property Carbon $EMISSAO
 * @property string $LINKQRCODE
 * @property string $PONTO_REFERENCIA_ENTREGA
 * @property float $TOTAL
 * @property Carbon $DATA_ENTREGA
 * @property Carbon $DATA_RETIRADA
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property CRMENDERECOPESSOA $c_r_m_e_n_d_e_r_e_c_o_p_e_s_s_o_a
 * @property FATCUPOMPESSOA $f_a_t_c_u_p_o_m_p_e_s_s_o_a
 * @property ESTARMARIOGAVETUM $e_s_t_a_r_m_a_r_i_o_g_a_v_e_t_u_m
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 * @property FATPRESTADORTEMPOENTREGA $f_a_t_p_r_e_s_t_a_d_o_r_t_e_m_p_o_e_n_t_r_e_g_a
 * @property FATSTATU $f_a_t_s_t_a_t_u
 * @property FATTIPOSERVICO $f_a_t_t_i_p_o_s_e_r_v_i_c_o
 *
 * @package App\Models
 */
class pedido extends Model
{
	public $table = 'FAT_PEDIDO';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_CLIENTE' => 'int',
		'ID_COMISSAO' => 'int',
		'ID_CUPOM_PESSOA' => 'int',
		'ID_ENDERECO_PESSOA' => 'int',
		'ID_GAVETA_ARMARIO_ENTREGA' => 'int',
		'ID_GAVETA_ARMARIO_RETIRADA' => 'int',
		'ID_PRAZO' => 'int',
		'ID_PESSOA_PRESTADOR' => 'int',
		'ID_TIPO_SERVICO_ROUPAS' => 'int',
		'ID_TIPO_SERVICO_FRETE' => 'int',
		'ID_STATUS_SERVICO_FRETE' => 'int',
		'ID_STATUS_SERVICO_ROUPAS' => 'int',
		'TOTAL' => 'float',
		'TAXA_ENTREGA_FRETE_REAIS' => 'float',
		'PERCENT_DESCONTO_PRESTADOR_TEMPO_ENTREGA' => 'float',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'DATAFECHAMENTO',
		'EMISSAO',
		'DATA_ENTREGA',
		'DATA_RETIRADA',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_CLIENTE',
		'ID_COMISSAO',
		'ID_CUPOM_PESSOA',
		'ID_ENDERECO_PESSOA',
		'ID_GAVETA_ARMARIO_ENTREGA',
		'ID_GAVETA_ARMARIO_RETIRADA',
		'ID_PRAZO',
		'ID_PESSOA_PRESTADOR',
		'ID_TIPO_SERVICO_ROUPAS',
		'ID_TIPO_SERVICO_FRETE',
		'ID_STATUS_SERVICO_FRETE',
		'ID_STATUS_SERVICO_ROUPAS',
		'COR_HEXADECIMAL',
		'COR_DESCRICAO',
		'DATAFECHAMENTO',
		'EMISSAO',
		'INVOICE_IUGU',
		'LINKQRCODE',
		'PONTO_REFERENCIA_ENTREGA',
		'TOTAL',
		'TAXA_ENTREGA_FRETE_REAIS',
		'PERCENT_DESCONTO_PRESTADOR_TEMPO_ENTREGA',
		'DATA_ENTREGA',
		'DATA_RETIRADA',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function c_r_m_e_n_d_e_r_e_c_o_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMENDERECOPESSOA::class, 'ID_ENDERECO_PESSOA');
	}

	public function f_a_t_c_u_p_o_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(FATCUPOMPESSOA::class, 'ID_CUPOM_PESSOA');
	}

	public function e_s_t_a_r_m_a_r_i_o_g_a_v_e_t_u_m()
	{
		return $this->belongsTo(ESTARMARIOGAVETUM::class, 'ID_GAVETA_ARMARIO_RETIRADA');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_COMISSAO');
	}

	public function f_a_t_p_r_e_s_t_a_d_o_r_t_e_m_p_o_e_n_t_r_e_g_a()
	{
		return $this->belongsTo(FATPRESTADORTEMPOENTREGA::class, 'ID_PRAZO');
	}

	public function f_a_t_s_t_a_t_u()
	{
		return $this->belongsTo(FATSTATU::class, 'ID_STATUS');
	}

	public function f_a_t_t_i_p_o_s_e_r_v_i_c_o()
	{
		return $this->belongsTo(FATTIPOSERVICO::class, 'ID_TIPO_SERVICO_ROUPAS');
	}
}
