<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class tipomensagem
 *
 * @property int $ID
 * @property string $DESCRICAO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @package App\Models
 */
class tipomensagem extends Model
{
	public $table = 'FAT_TIPO_MENSAGEM';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'DESCRICAO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];
}
