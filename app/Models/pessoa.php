<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class pessoa
 *
 * @property int $ID
 * @property string $CUSTOMER_TOKEN_IUGU
 * @property string $CLASSE
 * @property string $ATIVIDADE
 * @property string $EMAIL
 * @property string $SENHA
 * @property string $NOME
 * @property string $NOMEREDUZIDO
 * @property string $TIPOPESSOA
 * @property int $CPF
 * @property string $INSCESTRG
 * @property string $INSCMUNICIPAL
 * @property string $CELULAR
 * @property string $TELEFONE_FIXO
 * @property string $HOMEPAGE
 * @property string $OBSERVACAO
 * @property Carbon $DATA_NASCIMENTO
 * @property string $SEXO
 * @property string $UID
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FINCLASSE $f_i_n_c_l_a_s_s_e
 * @property FINATIVIDADE $f_i_n_a_t_i_v_i_d_a_d_e
 *
 * @package App\Models
 */
class pessoa extends Model
{
	public $table = 'CRM_PESSOA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'DATA_NASCIMENTO',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'CUSTOMER_TOKEN_IUGU',
		'CLASSE',
		'ATIVIDADE',
		'EMAIL',
		'SENHA',
		'NOME',
		'NOMEREDUZIDO',
		'FIREBASE_USER',
		'TIPOPESSOA',
		'CPF',
		'INSCESTRG',
		'INSCMUNICIPAL',
		'CELULAR',
		'TELEFONE_FIXO',
		'HOMEPAGE',
		'OBSERVACAO',
		'DATA_NASCIMENTO',
		'SEXO',
		'UID',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_i_n_c_l_a_s_s_e()
	{
		return $this->belongsTo(FINCLASSE::class, 'CLASSE');
	}

	public function f_i_n_a_t_i_v_i_d_a_d_e()
	{
		return $this->belongsTo(FINATIVIDADE::class, 'ATIVIDADE');
	}
}
