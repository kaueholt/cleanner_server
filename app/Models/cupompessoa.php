<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class cupompessoa
 *
 * @property int $ID
 * @property int $ID_CUPOM
 * @property int $ID_PESSOA
 * @property string $UTILIZADO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATCUPOM $f_a_t_c_u_p_o_m
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class cupompessoa extends Model
{
	public $table = 'FAT_CUPOM_PESSOA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_CUPOM' => 'int',
		'ID_PESSOA' => 'int',
		'UTILIZADO' => 'boolean',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_CUPOM',
		'ID_PESSOA',
		'UTILIZADO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_c_u_p_o_m()
	{
		return $this->belongsTo(FATCUPOM::class, 'ID_CUPOM');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA');
	}
}
