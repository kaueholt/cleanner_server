<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class prestadorfavoritocliente
 *
 * @property int $ID
 * @property int $ID_PESSOA_CLIENTE
 * @property int $ID_PESSOA_PRESTADOR
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class prestadorfavoritocliente extends Model
{
	protected $table = 'CRM_PRESTADOR_FAVORITO_CLIENTE';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PESSOA_CLIENTE' => 'int',
		'ID_PESSOA_PRESTADOR' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PESSOA_CLIENTE',
		'ID_PESSOA_PRESTADOR',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA_PRESTADOR');
	}
}
