<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class FATPEDIDOAVALIACAO
 *
 * @property int $ID
 * @property int $ID_PEDIDO
 * @property int $RESPOSTA1
 * @property int $RESPOSTA2
 * @property int $RESPOSTA3
 * @property string $PERGUNTA1
 * @property string $PERGUNTA2
 * @property string $PERGUNTA3
 * @property string $OBSERVACAO
 * @property float $MEDIA
 * @property \Carbon\Carbon $CREATED_AT
 * @property int $USERINSERT
 * @property \Carbon\Carbon $UPDATED_AT
 * @property int $USERUPDATE
 *
 * @property \App\Models\FATPEDIDO $f_a_t_p_e_d_i_d_o
 *
 * @package App\Models
 */
class pedidoavaliacao extends Eloquent
{
	protected $table = 'FAT_PEDIDO_AVALIACAO';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PEDIDO' => 'int',
		'USERINSERT' => 'int',
		'USERUPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PEDIDO',
		'RESPOSTA1',
		'RESPOSTA2',
		'RESPOSTA3',
		'PERGUNTA1',
		'PERGUNTA2',
		'PERGUNTA3',
		'OBSERVACAO',
		'MEDIA',
		'CREATED_AT',
		'USERINSERT',
		'UPDATED_AT',
		'USERUPDATE'
	];

	public function f_a_t_p_e_d_i_d_o()
	{
		return $this->belongsTo(\App\Models\FATPEDIDO::class, 'ID_PEDIDO');
	}
}
