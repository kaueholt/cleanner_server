<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class grupoprestador
 *
 * @property int $ID
 * @property int $ID_GRUPO
 * @property int $ID_TEMPO_ENTREGA_MINIMO
 * @property int $ID_PESSOA_PESSOAPRESTADOR
 * @property boolean $ATIVO
 * @property float $DESCONTO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property ESTGRUPO $e_s_t_g_r_u_p_o
 * @property FATTEMPOENTREGA $f_a_t_t_e_m_p_o_e_n_t_r_e_g_a
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class grupoprestador extends Model
{
	public $table = 'EST_GRUPO_PRESTADOR';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_GRUPO' => 'int',
		'ID_TEMPO_ENTREGA_MINIMO' => 'int',
		'ID_PESSOA_PESSOAPRESTADOR' => 'int',
		'ATIVO' => 'boolean',
		'DESCONTO' => 'float',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_GRUPO',
		'ID_TEMPO_ENTREGA_MINIMO',
		'ID_PESSOA_PESSOAPRESTADOR',
		'ATIVO',
		'DESCONTO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function e_s_t_g_r_u_p_o()
	{
		return $this->belongsTo(ESTGRUPO::class, 'ID_GRUPO');
	}

	public function f_a_t_t_e_m_p_o_e_n_t_r_e_g_a()
	{
		return $this->belongsTo(FATTEMPOENTREGA::class, 'ID_TEMPO_ENTREGA_MINIMO');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA_PESSOAPRESTADOR');
	}
}
