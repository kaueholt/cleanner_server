<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class armariogaveta
 *
 * @property int $ID
 * @property int $ID_PESSOA
 * @property string $DESCRICAO
 * @property string $DISPONIVEL
 * @property string $OBSERVACAO
 * @property string $SENHA
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class armariogaveta extends Model
{
	public $table = 'EST_ARMARIO_GAVETA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PESSOA' => 'int',
		'DISPONIVEL' => 'boolean',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PESSOA',
		'DESCRICAO',
		'DISPONIVEL',
		'OBSERVACAO',
		'SENHA',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA');
	}
}
