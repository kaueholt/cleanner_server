<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class tempoentrega
 *
 * @property int $ID
 * @property int $TEMPO_ENTREGA_HORAS
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @package App\Models
 */
class tempoentrega extends Model
{
	public $table = 'FAT_TEMPO_ENTREGA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'TEMPO_ENTREGA_HORAS' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'TEMPO_ENTREGA_HORAS',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];
}
