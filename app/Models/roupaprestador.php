<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class roupaprestador
 *
 * @property int $ID
 * @property int $ID_ROUPA
 * @property int $ID_PESSOA_PESSOAPRESTADOR
 * @property string $ATIVO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property ESTROUPA $e_s_t_r_o_u_p_a
 * @property FATTEMPOENTREGA $f_a_t_t_e_m_p_o_e_n_t_r_e_g_a
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class roupaprestador extends Model
{
	public $table = 'EST_ROUPA_PRESTADOR';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_ROUPA' => 'int',
		'ID_PESSOA_PESSOAPRESTADOR' => 'int',
		'ATIVO' => 'boolean',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_ROUPA',
		'ID_PESSOA_PESSOAPRESTADOR',
		'ATIVO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function e_s_t_r_o_u_p_a()
	{
		return $this->belongsTo(ESTROUPA::class, 'ID_ROUPA');
	}

	public function f_a_t_t_e_m_p_o_e_n_t_r_e_g_a()
	{
		return $this->belongsTo(FATTEMPOENTREGA::class, 'ID_TEMPO_ENTREGA');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA_PESSOAPRESTADOR');
	}
}
