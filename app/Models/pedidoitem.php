<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class pedidoitem
 *
 * @property int $ID
 * @property int $ID_PEDIDO
 * @property int $ID_ROUPA
 * @property int $STATUS
 * @property float $VALOR_UNITARIO
 * @property float $VALOR_TOTAL
 * @property float $COMISSAO
 * @property string $OBSERVACAO_CANCELAMENTO
 * @property string $OBSERVACAO_CLIENTE
 * @property string $OBSERVACAO_LAVANDERIA
 * @property string $NOVA_OBSERVACAO_CLIENTE
 * @property string $NOVA_OBSERVACAO_LAVANDERIA
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATPEDIDO $f_a_t_p_e_d_i_d_o
 * @property ESTROUPASPRESTADOR $e_s_t_r_o_u_p_a_s_p_r_e_s_t_a_d_o_r
 *
 * @package App\Models
 */
class pedidoitem extends Model
{
	public $table = 'FAT_PEDIDO_ITEM';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PEDIDO' => 'int',
		'ID_ROUPA' => 'int',
		'QUANTIDADE' => 'int',
		'STATUS' => 'int',
		'VALOR_UNITARIO' => 'float',
		'VALOR_TOTAL' => 'float',
		'COMISSAO' => 'float',
		'NOVA_OBSERVACAO_CLIENTE' => 'boolean',
		'NOVA_OBSERVACAO_LAVANDERIA' => 'boolean',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PEDIDO',
		'ID_ROUPA',
		'QUANTIDADE',
		'STATUS',
		'VALOR_UNITARIO',
		'VALOR_TOTAL',
		'COMISSAO',
		'OBSERVACAO_CANCELAMENTO',
		'OBSERVACAO_CLIENTE',
		'OBSERVACAO_LAVANDERIA',
		'NOVA_OBSERVACAO_CLIENTE',
		'NOVA_OBSERVACAO_LAVANDERIA',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_p_e_d_i_d_o()
	{
		return $this->belongsTo(FATPEDIDO::class, 'ID_PEDIDO');
	}

	public function e_s_t_r_o_u_p_a()
	{
		return $this->belongsTo(ESTROUPA::class, 'ID_ROUPA');
	}
}
