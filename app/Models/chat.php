<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class chat
 *
 * @property int $ID
 * @property int $ID_PEDIDO
 * @property int $ID_PESSOA_DESTINATARIA
 * @property string $MENSAGEM
 * @property boolean $IMAGEM
 * @property Carbon $DATA_ENVIO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATPEDIDO $f_a_t_p_e_d_i_d_o
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class chat extends Model
{
	public $table = 'AMB_CHAT';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PEDIDO' => 'int',
		'ID_PESSOA_DESTINATARIA' => 'int',
		'IMAGEM' => 'boolean',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'DATA_ENVIO',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PEDIDO',
		'ID_PESSOA_DESTINATARIA',
		'MENSAGEM',
		'IMAGEM',
		'DATA_ENVIO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_p_e_d_i_d_o()
	{
		return $this->belongsTo(FATPEDIDO::class, 'ID_PEDIDO');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'USER_INSERT');
	}
}
