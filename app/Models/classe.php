<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class classe
 *
 * @property string $ID
 * @property string $DESCRICAO
 * @property string $BLOQUEADO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @package App\Models
 */
class classe extends Model
{
	public $table = 'FIN_CLASSE';
	public $primaryKey = 'ID';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'BLOQUEADO' => 'boolean',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'DESCRICAO',
		'BLOQUEADO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];
}
