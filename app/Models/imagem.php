<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class imagem
 *
 * @property int $ID
 * @property int $ID_DOC
 * @property string $MODULO
 * @property string $DESTAQUE
 * @property Carbon $CREATED_AT
 * @property int $USER_INSERT
 * @property Carbon $UPDATED_AT
 * @property int $USER_UPDATE
 *
 * @package App\Models
 */
class imagem extends Model
{
	public $table = 'AMB_IMAGEM';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_DOC' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_DOC',
		'MODULO',
		'DESTAQUE',
		'CREATED_AT',
		'USER_INSERT',
		'UPDATED_AT',
		'USER_UPDATE'
	];
}
