<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class feed
 *
 * @property int $ID
 * @property int $ID_MENSAGEM
 * @property int $ID_PESSOA_DESTINATARIA
 * @property Carbon $DATA_ENVIO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATMENSAGEM $f_a_t_m_e_n_s_a_g_e_m
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class feed extends Model
{
	public $table = 'FAT_FEED';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_MENSAGEM' => 'int',
		'ID_PESSOA_DESTINATARIA' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'DATA_ENVIO',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_MENSAGEM',
		'ID_PESSOA_DESTINATARIA',
		'DATA_ENVIO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_m_e_n_s_a_g_e_m()
	{
		return $this->belongsTo(FATMENSAGEM::class, 'ID_MENSAGEM');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'USER_INSERT');
	}
}
