<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class historicopedido
 *
 * @property int $ID
 * @property int $ID_PEDIDO
 * @property int $ID_STATUS_ANTERIOR
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATPEDIDO $f_a_t_p_e_d_i_d_o
 * @property FATSTATU $f_a_t_s_t_a_t_u
 *
 * @package App\Models
 */
class historicopedido extends Model
{
	public $table = 'FAT_HISTORICO_PEDIDO';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PEDIDO' => 'int',
		'ID_STATUS_ANTERIOR' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PEDIDO',
		'ID_STATUS_ANTERIOR',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_p_e_d_i_d_o()
	{
		return $this->belongsTo(FATPEDIDO::class, 'ID_PEDIDO');
	}

	public function f_a_t_s_t_a_t_u()
	{
		return $this->belongsTo(FATSTATU::class, 'ID_STATUS_ANTERIOR');
	}
}
