<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class grupo
 *
 * @property string $GRUPO
 * @property string $DESCRICAO
 * @property string $TIPO
 * @property int $NIVEL
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @package App\Models
 */
class grupo extends Model
{
	public $table = 'EST_GRUPO';
	public $primaryKey = 'GRUPO';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'NIVEL' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int',
		'COMISSAO' => 'float'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'DESCRICAO',
		'TIPO',
		'NIVEL',
		'COMISSAO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];
}
