<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class prestadortempoentrega
 *
 * @property int $ID
 * @property int $ID_PESSOA_PESSOAPRESTADOR
 * @property int $ID_TEMPOENTREGA
 * @property float $DESCONTO
 * @property string $UNIDADE_DESCONTO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATTEMPOENTREGA $f_a_t_t_e_m_p_o_e_n_t_r_e_g_a
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class prestadortempoentrega extends Model
{
	public $table = 'FAT_PRESTADOR_TEMPO_ENTREGA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PESSOA_PESSOAPRESTADOR' => 'int',
		'ID_TEMPOENTREGA' => 'int',
		'ATIVO' => 'boolean',
		'DESCONTO' => 'float',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PESSOA_PESSOAPRESTADOR',
		'ID_TEMPOENTREGA',
		'DESCONTO',
		'ATIVO',
		'UNIDADE_DESCONTO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_t_e_m_p_o_e_n_t_r_e_g_a()
	{
		return $this->belongsTo(FATTEMPOENTREGA::class, 'ID_TEMPOENTREGA');
	}

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA_PESSOAPRESTADOR');
	}
}
