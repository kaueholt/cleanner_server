<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class parametro
 *
 * @property string $API_GOOGLE_KEY
 *
 * @package App\Models
 */
class parametro extends Model
{
	public $table = 'AMB_PARAMETRO';
	public $timestamps = false;

	protected $casts = [];

	protected $dates = [];

	protected $fillable = [
		'API_GOOGLE_KEY'
	];

}
