<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class pessoaprestador
 *
 * @property int $ID
 * @property int $ID_PESSOA
 * @property string $CNPJ
 * @property boolean $LOGO
 * @property int $MAX_CLIENTES_DIA
 * @property int $MAX_DISTANCIA_ATENDIMENTO
 * @property string $NOMECONTATO
 * @property float $RATING
 * @property int $NUMERO_DE_RATINGS
 * @property float $VALOR_MINIMO_PEDIDO
 * @property float $TAXA_ENTREGA_ARMARIO_REAIS
 * @property float $TAXA_ENTREGA_DOMICILIO_REAIS
 * @property float $TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS
 * @property string $CODIGO_BANCO
 * @property string $AGENCIA_BANCO
 * @property string $CONTA_BANCO
 * @property Carbon $INICIOMANHA
 * @property Carbon $FIMMANHA
 * @property Carbon $INICIOTARDE
 * @property Carbon $FIMTARDE
 * @property Carbon $INICIOSABADOMANHA
 * @property Carbon $FIMSABADOMANHA
 * @property Carbon $INICIOSABADOTARDE
 * @property Carbon $FIMSABADOTARDE
 * @property Carbon $INICIODOMINGOMANHA
 * @property Carbon $FIMDOMINGOMANHA
 * @property Carbon $INICIODOMINGOTARDE
 * @property Carbon $FIMDOMINGOTARDE
 * @property string $DIASEMANA
 * @property string $PAUSADO
 * @property string $BLOQUEADO
 * @property string $LAVA
 * @property string $PASSA
 * @property string $ENTREGA_ARMARIO
 * @property string $ENTREGA_DOMICILIO
 * @property string $ENTREGA_CLIENTE_BUSCA_PEDIDO
 * @property string $PRODUTOS_UTILIZADOS
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class pessoaprestador extends Model
{
	public $table = 'CRM_PESSOA_PRESTADOR';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PESSOA' => 'int',
		'LOGO' => 'boolean',
		'MAX_CLIENTES_DIA' => 'int',
		'MAX_DISTANCIA_ATENDIMENTO' => 'int',
		'RATING' => 'float',
		'NUMERO_DE_RATINGS' => 'int',
		'VALOR_MINIMO_PEDIDO' => 'float',
		'TAXA_ENTREGA_ARMARIO_REAIS' => 'float',
		'TAXA_ENTREGA_DOMICILIO_REAIS' => 'float',
		'TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS' => 'float',
		'PAUSADO' => 'boolean',
		'BLOQUEADO' => 'boolean',
		'LAVA' => 'boolean',
		'PASSA' => 'boolean',
		'ENTREGA_ARMARIO' => 'boolean',
		'ENTREGA_DOMICILIO' => 'boolean',
		'ENTREGA_CLIENTE_BUSCA_PEDIDO' => 'boolean',
		'INICIOMANHA' => 'string',
		'FIMMANHA' => 'string',
		'INICIOTARDE' => 'string',
		'FIMTARDE' => 'string',
		'INICIOSABADOMANHA' => 'string',
		'INICIOSABADOTARDE' => 'string',
		'FIMSABADOMANHA' => 'string',
		'FIMSABADOTARDE' => 'string',
		'INICIODOMINGOMANHA' => 'string',
		'INICIODOMINGOTARDE' => 'string',
		'FIMDOMINGOMANHA' => 'string',
		'FIMDOMINGOTARDE' => 'string',
		'DIASEMANA' => 'string',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
    ];
    
	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PESSOA',
		'CNPJ',
		'LOGO',
		'MAX_CLIENTES_DIA',
		'MAX_DISTANCIA_ATENDIMENTO',
		'NOMECONTATO',
		'RATING',
		'NUMERO_DE_RATINGS',
		'VALOR_MINIMO_PEDIDO',
		'CODIGO_BANCO',
		'AGENCIA_BANCO',
		'CONTA_BANCO',
		'INICIOMANHA',
		'FIMMANHA',
		'INICIOTARDE',
		'FIMTARDE',
		'INICIOSABADOMANHA',
		'FIMSABADOMANHA',
		'INICIOSABADOTARDE',
		'FIMSABADOTARDE',
		'INICIODOMINGOMANHA',
		'FIMDOMINGOMANHA',
		'INICIODOMINGOTARDE',
		'FIMDOMINGOTARDE',
		'DIASEMANA',
		'PAUSADO',
		'BLOQUEADO',
		'LAVA',
		'PASSA',
		'ENTREGA_ARMARIO',
		'TAXA_ENTREGA_ARMARIO_REAIS',
		'ENTREGA_DOMICILIO',
		'TAXA_ENTREGA_DOMICILIO_REAIS',
		'ENTREGA_CLIENTE_BUSCA_PEDIDO',
		'TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS',
		'PRODUTOS_UTILIZADOS',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA');
	}
}
