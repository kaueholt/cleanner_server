<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class cupom
 *
 * @property int $ID
 * @property string $DESCRICAO
 * @property float $DESCONTO
 * @property string $PERCENTUAL_OU_REAL
 * @property Carbon $VALIDADEINICIO
 * @property Carbon $VALIDADEFIM
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_CREATE
 * @property int $USER_UPDATE
 *
 * @package App\Models
 */
class cupom extends Model
{
	public $table = 'FAT_CUPOM';
	public $primaryKey = 'ID';
	// public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'DESCONTO' => 'float',
		'USER_CREATE' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'VALIDADEINICIO',
		'VALIDADEFIM',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'DESCRICAO',
		'DESCONTO',
		'PERCENTUAL_OU_REAL',
		'VALIDADEINICIO',
		'VALIDADEFIM',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_CREATE',
		'USER_UPDATE'
	];
}
