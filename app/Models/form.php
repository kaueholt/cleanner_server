<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class form
 *
 * @property int $ID
 * @property string $NOMEFORM
 * @property string $DESCRICAO
 * @property string $HELP
 *
 * @package App\Models
 */
class form extends Model
{
	public $table = 'AMB_FORM';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $fillable = [
		'NOMEFORM',
		'DESCRICAO',
		'HELP'
	];
}
