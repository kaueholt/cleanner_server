<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class tiposervico
 *
 * @property int $ID
 * @property string $DESCRICAO
 * @property string $FRETE
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @package App\Models
 */
class tiposervico extends Model
{
	public $table = 'FAT_TIPOSERVICO';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'FRETE' => 'boolean',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID',
		'DESCRICAO',
		'FRETE',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];
}
