<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class enderecopessoa
 *
 * @property int $ID
 * @property int $ID_PESSOA
 * @property string $FAVORITO
 * @property string $ENDERECO
 * @property string $NUMERO
 * @property string $COMPLEMENTO
 * @property string $BAIRRO
 * @property string $CEP
 * @property float $LATITUDE
 * @property float $LONGITUDE
 * @property string $CIDADE
 * @property string $UF
 * @property string $TAG
 * @property int $TELEFONE
 * @property string $ICONE
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class enderecopessoa extends Model
{
	public $table = 'CRM_ENDERECO_PESSOA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PESSOA' => 'int',
		'FAVORITO' => 'boolean',
		'LATITUDE' => 'float',
		'LONGITUDE' => 'float',
		'TELEFONE' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PESSOA',
		'FAVORITO',
		'ENDERECO',
		'NUMERO',
		'COMPLEMENTO',
		'BAIRRO',
		'CEP',
		'LATITUDE',
		'LONGITUDE',
		'CIDADE',
		'UF',
		'TAG',
		'TELEFONE',
		'ICONE',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA');
	}
}
