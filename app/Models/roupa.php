<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class roupa
 *
 * @property int $ID
 * @property string $DESCRICAO
 * @property string $GRUPO
 * @property string $OBSERVACAO
 * @property float $PRECO_LAVAR
 * @property float $PRECO_PASSAR
 * @property float $PRECO_LAVAR_E_PASSAR
 * @property string $UNIDADE
 * @property float $COMISSAO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property ESTGRUPO $e_s_t_g_r_u_p_o
 *
 * @package App\Models
 */
class roupa extends Model
{
	public $table = 'EST_ROUPA';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'PRECO_LAVAR' => 'float',
		'PRECO_PASSAR' => 'float',
		'PRECO_LAVAR_E_PASSAR' => 'float',
		'COMISSAO' => 'float',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'DESCRICAO',
		'GRUPO',
		'OBSERVACAO',
		'PRECO_LAVAR',
		'PRECO_PASSAR',
		'PRECO_LAVAR_E_PASSAR',
		'UNIDADE',
		'COMISSAO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function e_s_t_g_r_u_p_o()
	{
		return $this->belongsTo(ESTGRUPO::class, 'GRUPO');
	}
}
