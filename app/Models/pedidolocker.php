<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class pedidolocker
 *
 * @property int `ID`
 * @property int `ID_PEDIDO`
 * @property string `LOCKER_NAME`
 * @property string `IDENTIFICACAO_GAVETA`
 * @property string `STATUS`
 * @property string `RESERVATION_CODE`
 * @property string `DESCRIPTION`
 * @property string `KEY_DELIVERER`
 * @property string `KEY_RECEIVER`
 * @property Carbon `DATE_RESERVE`
 * @property int `USER_INSERT`
 * @property int `USER_UPDATE`
 * @property Carbon `CREATED_AT`
 * @property Carbon `UPDATED_AT`
 *
 * @property CRMPESSOA $c_r_m_p_e_s_s_o_a
 *
 * @package App\Models
 */
class pedidolocker extends Model
{
	public $table = 'FAT_PEDIDO_LOCKER';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PEDIDO' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'DATE_RESERVE',
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PEDIDO',
		'LOCKER_NAME',
		'IDENTIFICACAO_GAVETA',
		'STATUS',
		'RESERVATION_CODE',
		'DESCRIPTION',
		'KEY_DELIVERER',
		'KEY_RECEIVER',
		'DATE_RESERVE',
		'USER_INSERT',
		'USER_UPDATE',
		'CREATED_AT',
		'UPDATED_AT',
	];

	public function c_r_m_p_e_s_s_o_a()
	{
		return $this->belongsTo(CRMPESSOA::class, 'ID_PESSOA');
	}
}
