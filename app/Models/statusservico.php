<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class statusservico
 *
 * @property int $ID
 * @property int $ID_STATUS
 * @property int $ID_TIPOSERVICO
 * @property int $SEQUENCIA
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATSTATU $f_a_t_s_t_a_t_u
 * @property FATTIPOSERVICO $f_a_t_t_i_p_o_s_e_r_v_i_c_o
 *
 * @package App\Models
 */
class statusservico extends Model
{
	public $table = 'FAT_STATUS_SERVICO';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_STATUS' => 'int',
		'ID_TIPOSERVICO' => 'int',
		'SEQUENCIA' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_STATUS',
		'ID_TIPOSERVICO',
		'SEQUENCIA',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_s_t_a_t_u()
	{
		return $this->belongsTo(FATSTATUS::class, 'ID_STATUS');
	}

	public function f_a_t_t_i_p_o_s_e_r_v_i_c_o()
	{
		return $this->belongsTo(FATTIPOSERVICO::class, 'ID_TIPOSERVICO');
	}
}
