<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class atividade
 *
 * @property string $ATIVIDADE
 * @property string $DESCRICAO
 * @property string $TIPO
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @package App\Models
 */
class atividade extends Model
{
	public $table = 'FIN_ATIVIDADE';
	public $primaryKey = 'ATIVIDADE';
	public $incrementing = false;
	public $timestamps = false;

	protected $casts = [
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'DESCRICAO',
		'TIPO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];
}
