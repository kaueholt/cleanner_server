<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class pagamentoiugu
 *
 * @property int $ID
 * @property int $ID_PEDIDO
 * @property string $NOME_OPERADORA
 * @property int $NUMERO_CONTA
 * @property string $TOKEN_VALIDACAO
 * @property Carbon $DATA
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATPEDIDO $f_a_t_p_e_d_i_d_o
 *
 * @package App\Models
 */
class pagamentoiugu extends Model
{
	public $table = 'FIN_PAGAMENTO_IUGU';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_PEDIDO' => 'int',
		'NUMERO_CONTA' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_PEDIDO',
		'NOME_OPERADORA',
		'NUMERO_CONTA',
		'TOKEN_VALIDACAO',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_p_e_d_i_d_o()
	{
		return $this->belongsTo(FATPEDIDO::class, 'ID_PEDIDO');
	}
}
