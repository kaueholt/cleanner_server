<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class mensagem
 *
 * @property int $ID
 * @property int $ID_TIPO_MENSAGEM
 * @property string $MENSAGEM
 * @property string $LINK
 * @property Carbon $CREATED_AT
 * @property Carbon $UPDATED_AT
 * @property int $USER_INSERT
 * @property int $USER_UPDATE
 *
 * @property FATTIPOMENSAGEM $f_a_t_t_i_p_o_m_e_n_s_a_g_e_m
 *
 * @package App\Models
 */
class mensagem extends Model
{
	public $table = 'FAT_MENSAGEM';
	public $primaryKey = 'ID';
	public $timestamps = false;

	protected $casts = [
		'ID_TIPO_MENSAGEM' => 'int',
		'USER_INSERT' => 'int',
		'USER_UPDATE' => 'int'
	];

	protected $dates = [
		'CREATED_AT',
		'UPDATED_AT'
	];

	protected $fillable = [
		'ID_TIPO_MENSAGEM',
		'MENSAGEM',
		'LINK',
		'CREATED_AT',
		'UPDATED_AT',
		'USER_INSERT',
		'USER_UPDATE'
	];

	public function f_a_t_t_i_p_o_m_e_n_s_a_g_e_m()
	{
		return $this->belongsTo(FATTIPOMENSAGEM::class, 'ID_TIPO_MENSAGEM');
	}
}
