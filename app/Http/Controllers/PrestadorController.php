<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;
use App\Http\Controllers\PessoaController;


class PrestadorController extends Controller{

    public function showPrestador($id){       
        $sql  = "SELECT PP.ID AS PRESTADOR_ID, PP.CNPJ as PRESTADOR_CNPJ, PP.LOGO as PRESTADOR_LOGO, 
                    PP.MAX_CLIENTES_DIA as PRESTADOR_MAX_CLIENTES_DIA, PP.MAX_DISTANCIA_ATENDIMENTO as PRESTADOR_MAX_DISTANCIA_ATENDIMENTO, 
                    PP.NOMECONTATO as PRESTADOR_NOMECONTATO, PP.RATING as PRESTADOR_RATING, PP.VALOR_MINIMO_PEDIDO 
                    as PRESTADOR_VALOR_MINIMO_PEDIDO, PP.CODIGO_BANCO as PRESTADOR_CODIGO_BANCO, 
                    PP.AGENCIA_BANCO as PRESTADOR_AGENCIA_BANCO, PP.CONTA_BANCO as PRESTADOR_CONTA_BANCO, 
                    PP.INICIOMANHA as PRESTADOR_INICIOMANHA, PP.FIMMANHA as PRESTADOR_FIMMANHA, PP.INICIOTARDE as PRESTADOR_INICIOTARDE, 
                    PP.FIMTARDE as PRESTADOR_FIMTARDE, PP.INICIOSABADOMANHA as PRESTADOR_INICIOSABADOMANHA, 
                    PP.FIMSABADOMANHA as PRESTADOR_FIMSABADOMANHA, PP.INICIOSABADOTARDE as PRESTADOR_INICIOSABADOTARDE, 
                    PP.FIMSABADOTARDE as PRESTADOR_FIMSABADOTARDE, PP.INICIODOMINGOMANHA as PRESTADOR_INICIODOMINGOMANHA, 
                    PP.FIMDOMINGOMANHA as PRESTADOR_FIMDOMINGOMANHA, PP.INICIODOMINGOTARDE as PRESTADOR_INICIODOMINGOTARDE, 
                    PP.FIMDOMINGOTARDE as PRESTADOR_FIMDOMINGOTARDE, PP.DIASEMANA as PRESTADOR_DIASEMANA, PP.PAUSADO as PRESTADOR_PAUSADO, 
                    PP.BLOQUEADO as PRESTADOR_BLOQUEADO, PP.LAVA as PRESTADOR_LAVA, PP.PASSA as PRESTADOR_PASSA, PP.ENTREGA_ARMARIO as PRESTADOR_ENTREGA_ARMARIO, 
                    PP.TAXA_ENTREGA_ARMARIO_REAIS as PRESTADOR_TAXA_ENTREGA_ARMARIO_REAIS, PP.ENTREGA_DOMICILIO as 
                    PRESTADOR_ENTREGA_DOMICILIO, PP.TAXA_ENTREGA_DOMICILIO_REAIS as PRESTADOR_TAXA_ENTREGA_DOMICILIO_REAIS, 
                    PP.ENTREGA_CLIENTE_BUSCA_PEDIDO as PRESTADOR_ENTREGA_CLIENTE_BUSCA_PEDIDO, PP.TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS 
                    as PRESTADOR_TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS, PP.PRODUTOS_UTILIZADOS as PRESTADOR_PRODUTOS_UTILIZADOS, 
                    PP.CREATED_AT as PRESTADOR_CREATED_AT, PP.UPDATED_AT as PRESTADOR_UPDATED_AT, PP.USER_INSERT as PRESTADOR_USER_INSERT, 
                    PP.USER_UPDATE as PRESTADOR_USER_UPDATE, P.ID as PESSOA_ID, P.CLASSE as PESSOA_CLASSE, 
                    P.ATIVIDADE as PESSOA_ATIVIDADE, P.EMAIL as PESSOA_EMAIL, P.SENHA as PESSOA_SENHA, P.NOME as PESSOA_NOME, 
                    P.NOMEREDUZIDO as PESSOA_NOMEREDUZIDO, P.TIPOPESSOA as PESSOA_TIPOPESSOA, P.CPF as PESSOA_CPF, 
                    P.INSCESTRG as PESSOA_INSCESTRG, P.INSCMUNICIPAL as PESSOA_INSCMUNICIPAL, P.CELULAR as PESSOA_CELULAR, 
                    P.TELEFONE_FIXO as PESSOA_TELEFONE_FIXO, P.HOMEPAGE as PESSOA_HOMEPAGE, P.OBSERVACAO as PESSOA_OBSERVACAO, 
                    P.DATA_NASCIMENTO as PESSOA_DATA_NASCIMENTO, P.SEXO as PESSOA_SEXO, P.UID as PESSOA_UID, 
                    P.CREATED_AT as PESSOA_CREATED_AT, P.UPDATED_AT as PESSOA_UPDATED_AT, P.USER_INSERT as PESSOA_USER_INSERT, 
                    P.USER_UPDATE as PESSOA_USER_UPDATE 
                FROM CRM_PESSOA_PRESTADOR PP 
                LEFT JOIN CRM_PESSOA P ON P.ID = PP.ID_PESSOA 
                WHERE PP.ID_PESSOA = {$id}
                LIMIT 1";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }    
    
    public function getRating($id){
        $sql = "SELECT 
                    ID, ID_PESSOA, IF(BLOQUEADO = TRUE, 0, RATING) AS RATING
                FROM
                    CRM_PESSOA_PRESTADOR
                WHERE 
                    ID_PESSOA = {$id}";
        $response = DB::select($sql);  
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
             : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);       
    }

    public function showPrestadorTempoEntrega($id){    
        // $sql = "SELECT 
        //             PTE.ID,
        //             TE.ID AS ID_TEMPOENTREGA,
        //             TE.TEMPO_ENTREGA_HORAS,
        //             IF(PTE.ATIVO <> 0, TRUE, FALSE) AS ATIVO,
        //             PTE.DESCONTO,
        //             PTE.UNIDADE_DESCONTO
        //         FROM
        //             FAT_PRESTADOR_TEMPO_ENTREGA PTE
        //         LEFT JOIN
        //             FAT_TEMPO_ENTREGA TE 
        //             ON PTE.ID_TEMPOENTREGA = TE.ID
        //         WHERE
        //             ID_PESSOA_PESSOAPRESTADOR = {$id}
        //         ORDER BY 
        //             TE.TEMPO_ENTREGA_HORAS ASC";

        $sql = "SELECT PTE.ID, TE.ID AS ID_TEMPOENTREGA, TE.TEMPO_ENTREGA_HORAS, IF(PTE.ATIVO <> 0, TRUE, FALSE) AS ATIVO, ";
        $sql .= "PTE.DESCONTO, PTE.UNIDADE_DESCONTO ";
        $sql .= "FROM FAT_TEMPO_ENTREGA TE ";
        $sql .= "LEFT JOIN FAT_PRESTADOR_TEMPO_ENTREGA PTE ON PTE.ID_TEMPOENTREGA = TE.ID AND PTE.ID_PESSOA_PESSOAPRESTADOR = ".$id." ";
        $sql .= "ORDER BY TE.TEMPO_ENTREGA_HORAS ASC";


        $response = DB::select($sql);  
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function getPrazoPrestadorEmHoras($id,$pessoaPrestador){
        $sql = "SELECT 
                    TE.TEMPO_ENTREGA_HORAS,
                    IF(PTE.ATIVO <> 0, TRUE, FALSE) AS ATIVO
                FROM
                    FAT_PRESTADOR_TEMPO_ENTREGA PTE
                LEFT JOIN
                    FAT_TEMPO_ENTREGA TE 
                    ON PTE.ID_TEMPOENTREGA = TE.ID
                WHERE
                    PTE.ID = {$id}
                    AND PTE.ID_PESSOA_PESSOAPRESTADOR = {$pessoaPrestador}
                LIMIT 1";
        $response = DB::select($sql);
        if(!$response){
            $sql = "SELECT
                        PTE.ID,
                        TE.TEMPO_ENTREGA_HORAS,
                        IF(PTE.ATIVO <> 0, TRUE, FALSE) AS ATIVO
                    FROM
                        FAT_PRESTADOR_TEMPO_ENTREGA PTE
                    LEFT JOIN
                        FAT_TEMPO_ENTREGA TE 
                        ON PTE.ID_TEMPOENTREGA = TE.ID
                    WHERE
                        PTE.ID_PESSOA_PESSOAPRESTADOR = {$pessoaPrestador}";
            $response = DB::select($sql);
            return $response  
                ? Handles::jsonResponse(false, 'Prazo incorreto.', array('FAT_PRESTADOR_TEMPO_ENTREGA.ID (FAT_PEDIDO.ID_PRAZO)'=>$id,'PESSOA_PRESTADOR'=>$pessoaPrestador, 'FAT_PRESTADOR_TEMPO_ENTREGA.IDS_DESTE_PRESTADOR'=>$response), 401)
                : Handles::jsonResponse(false, 'O prestador não possui tempos de entrega configurados!', array('FAT_PRESTADOR_TEMPO_ENTREGA.ID (FAT_PEDIDO.ID_PRAZO)'=>$id,'PESSOA_PRESTADOR'=>$pessoaPrestador), 404);
        }
        return $response 
            ? $response[0]->ATIVO 
                ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
                : Handles::jsonResponse(false, "O prestador não trabalha com o prazo de {$response[0]->TEMPO_ENTREGA_HORAS} horas!", $response[0], 480)
            : Handles::jsonResponse(false, 'Nenhum tempo de entrega encontrado!', array('FAT_PRESTADOR_TEMPO_ENTREGA.ID (FAT_PEDIDO.ID_PRAZO)'=>$id,'PESSOA_PRESTADOR'=>$pessoaPrestador), 404);
    }

    public function prestadorPossuiGruposDesatualizados($id){    
        $sqlGruposPersonalizadosPrestador = 
        "SELECT DISTINCT
            GP.ID,
            GP.ID_GRUPO,
            TE.TEMPO_ENTREGA_HORAS AS TEMPO_ENTREGA_MINIMO_HORAS,
            IF(GP.ATIVO <> FALSE, TRUE, FALSE) AS ATIVO
        FROM
            EST_GRUPO_PRESTADOR GP
        LEFT JOIN FAT_PRESTADOR_TEMPO_ENTREGA PTE 
            ON PTE.ID_PESSOA_PESSOAPRESTADOR = GP.ID_PESSOA_PESSOAPRESTADOR
        LEFT JOIN FAT_TEMPO_ENTREGA TE 
            ON GP.ID_TEMPO_ENTREGA_MINIMO = TE.ID
        WHERE
            PTE.ATIVO <> FALSE 
            AND GP.ID_PESSOA_PESSOAPRESTADOR = {$id}";
        $result = DB::select($sqlGruposPersonalizadosPrestador);  
        $countPersonalizados = count($result);
        $countGrupos = DB::table('EST_GRUPO')->count();
        return $countPersonalizados == $countGrupos ? Handles::jsonResponse(true, 'Todos os grupos estão configurados!', $result, 200)
            : Handles::jsonResponse(false, 'O número de grupos personalizados ('.$countPersonalizados.' grupos personalizados) não bate com o número de grupos existentes ('.$countGrupos.' grupos).', $result, 500);
    }

    public function calculaValoresRoupasAtendidasPeloPrestador($id){
        // $sql = "SELECT R.ID AS ID_ROUPA, CONCAT(UPPER(SUBSTRING(R.DESCRICAO,1,1)), LOWER(SUBSTRING(R.DESCRICAO,2))) AS DESCRICAO_ROUPA, 
        //             R.GRUPO, CONCAT(UPPER(SUBSTRING(G.DESCRICAO,1,1)), LOWER(SUBSTRING(G.DESCRICAO,2))) AS DESCRICAO_GRUPO, G.COMISSAO AS PERCENT_COMISSAO_GRUPO, 
        //             R.UNIDADE, IF(GP.ID, GP.ID_TEMPO_ENTREGA_MINIMO, G.ID_TEMPO_ENTREGA_MINIMO) AS ID_TEMPO_ENTREGA_MINIMO,
        //             T.TEMPO_ENTREGA_HORAS AS TEMPO_ENTREGA_MINIMO_HORAS, PTE.ID_TEMPOENTREGA, TT.TEMPO_ENTREGA_HORAS AS TEMPO_ENTREGA_HORAS, PTE.DESCONTO,
        //             R.PRECO_LAVAR AS PRECO_BRUTO_LAVAR,
        //             ROUND(R.PRECO_LAVAR * (1- (PTE.DESCONTO / 100)),2) as TOTAL_LAVAR, 
        //             R.PRECO_PASSAR AS PRECO_BRUTO_PASSAR,
        //             ROUND(R.PRECO_PASSAR * (1- (PTE.DESCONTO / 100)),2) as TOTAL_PASSAR, 
        //             R.PRECO_LAVAR_E_PASSAR AS PRECO_BRUTO_LAVAR_E_PASSAR, 
        //             ROUND(R.PRECO_LAVAR_E_PASSAR * (1- (PTE.DESCONTO / 100)),2) as TOTAL_LAVAR_E_PASSAR
        //         FROM EST_ROUPA R
        //         LEFT JOIN EST_GRUPO G ON G.GRUPO = R.GRUPO
        //         LEFT JOIN EST_GRUPO_PRESTADOR GP ON GP.ID_GRUPO = G.GRUPO 
        //         LEFT JOIN FAT_TEMPO_ENTREGA T ON T.ID = GP.ID_TEMPO_ENTREGA_MINIMO
        //         LEFT JOIN FAT_PRESTADOR_TEMPO_ENTREGA PTE ON PTE.ID_PESSOA_PESSOAPRESTADOR = GP.ID_PESSOA_PESSOAPRESTADOR
        //         LEFT JOIN FAT_TEMPO_ENTREGA TT ON TT.ID = PTE.ID_TEMPOENTREGA
        //         WHERE GP.ATIVO <> FALSE
        //                 AND PTE.ATIVO <> FALSE
        //                 AND T.TEMPO_ENTREGA_HORAS <= TT.TEMPO_ENTREGA_HORAS
        //                 AND GP.ID_PESSOA_PESSOAPRESTADOR = {$id}
        //         ORDER BY G.GRUPO ASC, DESCRICAO_ROUPA ASC, TEMPO_ENTREGA_HORAS ASC";
        $sql = "SELECT R.ID AS ID_ROUPA, CONCAT(UPPER(SUBSTRING(R.DESCRICAO,1,1)), LOWER(SUBSTRING(R.DESCRICAO,2))) AS DESCRICAO_ROUPA, ";
        $sql .= "R.GRUPO, CONCAT(UPPER(SUBSTRING(G.DESCRICAO,1,1)), LOWER(SUBSTRING(G.DESCRICAO,2))) AS DESCRICAO_GRUPO, ";
        $sql .= "G.COMISSAO AS PERCENT_COMISSAO_GRUPO, R.UNIDADE, TT.TEMPO_ENTREGA_HORAS, PTE.DESCONTO, ";
        $sql .= "R.PRECO_LAVAR AS PRECO_BRUTO_LAVAR, ROUND(R.PRECO_LAVAR * (1- (PTE.DESCONTO / 100)) - (R.PRECO_LAVAR * GP.DESCONTO/100),2) as TOTAL_LAVAR, ";
        $sql .= "R.PRECO_PASSAR AS PRECO_BRUTO_PASSAR, ROUND(R.PRECO_PASSAR * (1- (PTE.DESCONTO / 100)) - - (R.PRECO_PASSAR * GP.DESCONTO/100),2) as TOTAL_PASSAR, ";
        $sql .= "R.PRECO_LAVAR_E_PASSAR AS PRECO_BRUTO_LAVAR_E_PASSAR, ROUND(R.PRECO_LAVAR_E_PASSAR * (1- (PTE.DESCONTO / 100)) - - (R.PRECO_LAVAR_E_PASSAR * GP.DESCONTO/100),2) as TOTAL_LAVAR_E_PASSAR ";
        $sql .= "FROM EST_ROUPA R ";
        $sql .= "INNER JOIN EST_GRUPO_PRESTADOR GP ON GP.ID_GRUPO = R.GRUPO ";
        $sql .= "INNER JOIN EST_GRUPO G ON G.GRUPO = R.GRUPO ";
        $sql .= "LEFT JOIN FAT_PRESTADOR_TEMPO_ENTREGA PTE ON PTE.ID_PESSOA_PESSOAPRESTADOR = GP.ID_PESSOA_PESSOAPRESTADOR ";
        $sql .= "LEFT JOIN FAT_TEMPO_ENTREGA TT ON TT.ID = PTE.ID_TEMPOENTREGA ";
        $sql .= "WHERE PTE.ATIVO <> FALSE AND GP.ID_PESSOA_PESSOAPRESTADOR = {$id} ";
        $sql .= "ORDER BY G.GRUPO, DESCRICAO_ROUPA, TT.TEMPO_ENTREGA_HORAS";

        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function prestadorPossuiTemposDesatualizados($id){    
        $sqlTemposPersonalizadosPrestador = "SELECT * FROM FAT_PRESTADOR_TEMPO_ENTREGA WHERE ID_PESSOA_PESSOAPRESTADOR = {$id}";
        $result = DB::select($sqlTemposPersonalizadosPrestador);  
        $countPersonalizados = count($result);
        $countTempos = DB::table('FAT_TEMPO_ENTREGA')->count();
        return $countPersonalizados == $countTempos ? Handles::jsonResponse(true, 'Todos os tempos estão configurados!', $result, 200)
            : Handles::jsonResponse(false, 'O número de tempos personalizados ('.$countPersonalizados.' tempos personalizados) não bate com o número de tempos existentes ('.$countTempos.' tempos).', $result, 500);
    }
    
    public function getPrestadoresRaioDoClienteEndereco($raio = 100,$id){
        $sqlCliente = "SELECT LATITUDE, LONGITUDE FROM CRM_ENDERECO_PESSOA WHERE ID = {$id} LIMIT 1";
        $responseCliente = DB::select($sqlCliente);
        $sqlPrestadores = " SELECT E.*,
                                PP.MAX_DISTANCIA_ATENDIMENTO,
                                PP.PAUSADO
                            FROM CRM_ENDERECO_PESSOA E
                            LEFT JOIN CRM_PESSOA P 
                                ON P.ID = E.ID_PESSOA
                            LEFT JOIN CRM_PESSOA_PRESTADOR PP
                                ON PP.ID_PESSOA = P.ID
                            WHERE
                                P.TIPOPESSOA = 'P'
                                AND PP.BLOQUEADO <> TRUE";
                                // AND
                                // (PP.MAX_DISTANCIA_ATENDIMENTO IS NULL
                                // OR
                                //     ( PP.MAX_DISTANCIA_ATENDIMENTO IS NOT NULL
                                //     AND 
                                //         PP.MAX_DISTANCIA_ATENDIMENTO <= {$raio} ))
        $responsePrestadores = DB::select($sqlPrestadores);
        if(!$responseCliente || !$responsePrestadores)
            return Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
        $pessoa = new PessoaController();  
        for($i=0; $i < sizeof($responsePrestadores); $i++){
            $responsePrestadores[$i]->DISTANCIAKM = $pessoa->calculaDistanciaAPartirDasLatitudesLongitudes($responseCliente[0]->LATITUDE, $responseCliente[0]->LONGITUDE, $responsePrestadores[$i]->LATITUDE, $responsePrestadores[$i]->LONGITUDE);
            $responsePrestadores[$i]->DISTANCIAKM > $raio ? array_splice($responsePrestadores, $i--, 1) : null;  
        }
        return $responsePrestadores ? Handles::jsonResponse(true, 'Registros encontrados!', $responsePrestadores, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    } 

    public function showPrestadorGrupo($id){    
        // $sql = "SELECT 
        //             GP.ID_GRUPO,
        //             G.COMISSAO as PERCENT_COMISSAO_GRUPO,
        //             G.DESCRICAO, GP.DESCONTO, 
        //             IF(GP.ATIVO <> FALSE, TRUE, FALSE) as ATIVO,
        //             GP.ID_TEMPO_ENTREGA_MINIMO,
        //             TE.TEMPO_ENTREGA_HORAS
        //         FROM EST_GRUPO_PRESTADOR GP
        //         LEFT JOIN EST_GRUPO G ON GP.ID_GRUPO = G.GRUPO
        //         LEFT JOIN FAT_TEMPO_ENTREGA TE ON GP.ID_TEMPO_ENTREGA_MINIMO = TE.ID
        //         WHERE ID_PESSOA_PESSOAPRESTADOR = {$id}
        //         ORDER BY ATIVO DESC, GRUPO, TEMPO_ENTREGA_HORAS ASC";
        $sql = "SELECT G.GRUPO, G.DESCRICAO, P.DESCONTO, P.ID_GRUPO, ";
        $sql .= "IF(P.ATIVO <> FALSE, TRUE, FALSE) as ATIVO, G.COMISSAO  as PERCENT_COMISSAO_GRUPO, ";
        $sql .= "P.ID_TEMPO_ENTREGA_MINIMO, T.TEMPO_ENTREGA_HORAS ";
        $sql .= "FROM EST_GRUPO G ";
        $sql .= "LEFT JOIN EST_GRUPO_PRESTADOR P ON P.ID_GRUPO=G.GRUPO AND ID_PESSOA_PESSOAPRESTADOR = ".$id." ";
        $sql .= "LEFT JOIN FAT_TEMPO_ENTREGA T ON P.ID_TEMPO_ENTREGA_MINIMO = T.ID ";
        $sql .= "ORDER BY G.GRUPO ";

        $response = DB::select($sql);  
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function showPrestadorGrupoTempos($id, $grupos = null){
        if($grupos){
            $listaDeIdGrupos = [];
            for($i=0; $i<count($grupos); $i++){
                $listaDeIdGrupos[$i] = $grupos[$i]->GRUPO;
            }
            $idGruposStringged = '(\''. implode('\',\'',$listaDeIdGrupos).'\')';  
        }
        $sql = "SELECT DISTINCT
                    TE.ID AS ID_PRESTADOR_TEMPO_ENTREGA,
                    TE.ID_TEMPOENTREGA AS ID_TEMPO_ENTREGA,
                    T.TEMPO_ENTREGA_HORAS AS TEMPO_ENTREGA_HORAS,
                    IF(TE.DESCONTO > 0, TE.DESCONTO, 0) AS DESCONTO
                FROM
                    EST_GRUPO_PRESTADOR GP
                LEFT JOIN FAT_PRESTADOR_TEMPO_ENTREGA TE
                    ON TE.ID_PESSOA_PESSOAPRESTADOR = GP.ID_PESSOA_PESSOAPRESTADOR
                LEFT JOIN FAT_TEMPO_ENTREGA T
                    ON TE.ID_TEMPOENTREGA = T.ID
                LEFT JOIN FAT_TEMPO_ENTREGA TT
                    ON GP.ID_TEMPO_ENTREGA_MINIMO = TT.ID
                WHERE
                    TE.ATIVO <> FALSE 
                    AND GP.ATIVO <> FALSE
                    AND T.TEMPO_ENTREGA_HORAS >= TT.TEMPO_ENTREGA_HORAS
                    AND GP.ID_PESSOA_PESSOAPRESTADOR = {$id} ";
                $grupos ? $sql .= "AND GP.ID_GRUPO IN {$idGruposStringged} " : null;
                $sql .= "ORDER BY 
                    TE.DESCONTO DESC";
        $response = DB::select($sql);  
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function showGruposTemposMinimosPrestador($id){
        $sql = 'SELECT IF(GP.ID_GRUPO IS NOT NULL, GP.ID_GRUPO, G.GRUPO) AS GRUPO,
                    IF(GP.ID_GRUPO IS NOT NULL, IF(GP.ATIVO IS TRUE, TRUE, FALSE), TRUE) as ATIVO,
                    T.TEMPO_ENTREGA_HORAS AS TEMPO_MINIMO_HORAS, G.COMISSAO as PERCENT_COMISSAO_GRUPO, 
                    CONCAT(UPPER(SUBSTRING(G.DESCRICAO,1,1)),LOWER(SUBSTRING(G.DESCRICAO,2))) AS DESCRICAO,
                    IF(TP.ID_TEMPOENTREGA IS NOT NULL, TP.DESCONTO, 0) AS DESCONTO, 
                    IF(TP.ID_TEMPOENTREGA IS NOT NULL, TP.UNIDADE_DESCONTO, null) AS UNIDADE_DESCONTO,
                    CONCAT(IF(TP.UNIDADE_DESCONTO = \'P\',TP.DESCONTO,\'\'),IF(TP.UNIDADE_DESCONTO = \'P\',\' %\',\'R$ \'),
                        IF(TP.UNIDADE_DESCONTO = \'P\',\'\',TP.DESCONTO)) AS CONCAT_DESCONTO
                FROM EST_GRUPO_PRESTADOR GP
                RIGHT JOIN EST_GRUPO G 
                    ON GP.ID_GRUPO = G.GRUPO
                    AND GP.ID_PESSOA_PESSOAPRESTADOR = '.$id.' '.' 
                LEFT JOIN FAT_TEMPO_ENTREGA T 
                    ON GP.ID_GRUPO IS NOT NULL AND T.ID = GP.ID_TEMPO_ENTREGA_MINIMO
                    OR GP.ID_GRUPO IS NULL AND T.ID = G.ID_TEMPO_ENTREGA_MINIMO        
                LEFT JOIN FAT_PRESTADOR_TEMPO_ENTREGA TP
                    ON GP.ID_GRUPO IS NOT NULL 
                        AND TP.ID_TEMPOENTREGA = GP.ID_TEMPO_ENTREGA_MINIMO
                        AND TP.ID_PESSOA_PESSOAPRESTADOR = '.$id.' '.' 
                    OR GP.ID_GRUPO IS NULL 
                        AND TP.ID_TEMPOENTREGA = G.ID_TEMPO_ENTREGA_MINIMO 
                        AND TP.ID_PESSOA_PESSOAPRESTADOR = '.$id.' '.' 
                ORDER BY DESCRICAO ASC';
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function idPessoaPrestadorBloqueado($id){
        $sql = "SELECT BLOQUEADO FROM CRM_PESSOA_PRESTADOR WHERE ID_PESSOA = {$id} LIMIT 1";
        $response = DB::select($sql);
        return !$response ? 1 : $response[0]->BLOQUEADO ? 1 : 0;
    }
    
    public function idPessoaPrestadorPausado($id){
        $sql = "SELECT PAUSADO FROM CRM_PESSOA_PRESTADOR WHERE ID_PESSOA = {$id} LIMIT 1";
        $response = DB::select($sql);
        return !$response ? 1 : $response[0]->PAUSADO ? 1 : 0;
    }

    public function getTempoMinimoPrestadorParaListaDeRoupasEspecificada($roupas, $id){
        $listaDeIdRoupas = [];
        for($i=0; $i<count($roupas); $i++){
            $listaDeIdRoupas[$i] = $roupas[$i]['ID_ROUPA'];
        }
        $idRoupasStringged = '('. implode(',',$listaDeIdRoupas).')';
        $sqlGrupoRoupas = "SELECT DISTINCT R.GRUPO FROM admser_cleaner.EST_ROUPA R
                WHERE R.ID IN {$idRoupasStringged}";
        $responseGrupoRoupas = DB::select($sqlGrupoRoupas);
        $gruposInativos = [];
        for($i=0; $i<count($responseGrupoRoupas); $i++){
            $sqlTempoMinimo = 
                "SELECT 
                    GP.ID_GRUPO,
                    GP.ATIVO AS ATIVO,
                    IF(ISNULL(PTE.DESCONTO),0, PTE.DESCONTO) AS DESCONTO,
                    TE.TEMPO_ENTREGA_HORAS AS TEMPO_MINIMO
                FROM 
                    EST_GRUPO_PRESTADOR GP
                LEFT JOIN 
                    FAT_TEMPO_ENTREGA TE 
                    ON GP.ID_TEMPO_ENTREGA_MINIMO = TE.ID
                LEFT JOIN 
                    FAT_PRESTADOR_TEMPO_ENTREGA PTE 
                    ON GP.ID_TEMPO_ENTREGA_MINIMO = PTE.ID
                WHERE 
                    GP.ID_GRUPO = '{$responseGrupoRoupas[$i]->GRUPO}'
                    AND GP.ID_PESSOA_PESSOAPRESTADOR = {$id}
                LIMIT 1";
            $responseTempoMinimo = DB::select($sqlTempoMinimo);   
            if(count($responseTempoMinimo)){
                if(!$responseTempoMinimo[0]->ATIVO){ 
                    array_push($gruposInativos,$responseTempoMinimo[0]->ID_GRUPO);
                }else{
                    if(!isset($tempoMinimo)){
                        $tempoMinimo = $responseTempoMinimo[0]->TEMPO_MINIMO;
                        $data = $responseTempoMinimo[0];
                    }else if($tempoMinimo < $responseTempoMinimo[0]->TEMPO_MINIMO){
                        $tempoMinimo = $responseTempoMinimo[0]->TEMPO_MINIMO;
                        $data = $responseTempoMinimo[0];
                    }
                }
            } 
        }
        return count($gruposInativos) ? 
            Handles::jsonResponse(false, 'Há grupos não atendidos por este prestador!', $gruposInativos, 400) 
                : !isset($data) ? 
                    Handles::jsonResponse(false, 'Há grupos não configurados por este prestador!', $id, 402) 
                        : Handles::jsonResponse(true, 'Todas as roupas são atendidas pelo prestador!', $data, 200);
    }

    public function getPrestadorTaxasEntrega($id){       
        $sql = "SELECT PP.TAXA_ENTREGA_ARMARIO_REAIS as PRESTADOR_TAXA_ENTREGA_ARMARIO_REAIS,
                    PP.TAXA_ENTREGA_DOMICILIO_REAIS as PRESTADOR_TAXA_ENTREGA_DOMICILIO_REAIS, 
                    PP.TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS as PRESTADOR_TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS
                FROM CRM_PESSOA_PRESTADOR PP 
                WHERE PP.ID_PESSOA = {$id} LIMIT 1";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }    

    public function getPrestadorTaxaEntrega($id,$idTipoServico){       
        switch($idTipoServico){
            case 3:
                $campoFrete = 'TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS';
            break;
            case 4:
                $campoFrete = 'TAXA_ENTREGA_DOMICILIO_REAIS';
            break;
            case 5:
                $campoFrete = 'TAXA_ENTREGA_ARMARIO_REAIS';
            break;
        }
        $sql = "SELECT 
                    PP.{$campoFrete} AS PRESTADOR_TAXA_ENTREGA_REAIS
                FROM
                    CRM_PESSOA_PRESTADOR PP
                WHERE
                    PP.ID_PESSOA = {$id}
                    AND PP.BLOQUEADO <> TRUE
                LIMIT 1";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }    

    public function getDescontoTempoEntrega($id,$idPrazo){  
        $sql = "SELECT DESCONTO
                FROM
                    FAT_PRESTADOR_TEMPO_ENTREGA PTE
                LEFT JOIN
                    FAT_TEMPO_ENTREGA TE ON PTE.ID_TEMPOENTREGA = TE.ID
                WHERE
                    ID_PESSOA_PESSOAPRESTADOR = {$id}
                    AND TE.ID = {$idPrazo}
                ORDER BY ATIVO DESC, DESCONTO DESC LIMIT 1";
        $response = DB::select($sql);  
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function updateRating($id){
        $sql = "SELECT P.ID_PESSOA_PRESTADOR,
                AVG(A.MEDIA) AS RATING,
                COUNT(A.ID) AS NUMERO_DE_RATINGS
            FROM FAT_PEDIDO_AVALIACAO A
            LEFT JOIN FAT_PEDIDO P 
                ON A.ID_PEDIDO = P.ID
            WHERE P.ID_PESSOA_PRESTADOR = {$id}
            GROUP BY P.ID_PESSOA_PRESTADOR
            LIMIT 1";
        $ratingPrestador = DB::select($sql);
        if(!$ratingPrestador)
            return Handles::jsonResponse(false, "Rating do prestador {$id} não encontrado!", [], 404);
        $rating = round($ratingPrestador[0]->RATING,2);
        $numeroRatings = $ratingPrestador[0]->NUMERO_DE_RATINGS;
        $response = DB::table('CRM_PESSOA_PRESTADOR')
            ->where('ID_PESSOA',$ratingPrestador[0]->ID_PESSOA_PRESTADOR)
            ->update(
                ['RATING'=> $rating,
                'NUMERO_DE_RATINGS'=>$numeroRatings]            
            );
        if(!$response)
            return Handles::jsonResponse(false, "Não foi possível atualizar o rating!", $ratingPrestador, 500);
        else{
            $response = array('rating'=>$rating,'numero_ratings'=>$numeroRatings);
            return Handles::jsonResponse(true, 'Rating do prestador atualizado!', $response, 200);
        }
    }
}

