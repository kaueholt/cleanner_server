<?php

namespace App\Http\Controllers;

class PagamentoController extends Controller{
  
  public function cobrancaDireta($payment){
    /* https://dev.iugu.com/reference#cobranca-direta */    
    $jsonString = json_encode($payment);
    if(strlen($jsonString) > 2 && $this->isJson($jsonString)){ //json string mais comprido que '{}' (lenght 2, objeto vazio) e tb é json
      $ch = curl_init('https://api.iugu.com/v1/charge');
      curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
      curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonString);                                                                  
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
      curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
          'Content-Type: application/json',                                                                                
          'Content-Length: ' . strlen($jsonString))
        );
      $res = curl_exec($ch);
      curl_close($ch);
    }else{
      $res = '{"errors": "Parâmetros inválidos!"}';
    }
    return json_decode($res);
  }
  // resulted in a `401 Unauthorized` response:
  // use GuzzleHttp\Client;
  // $client = new Client();    
  //$res = $client->request('POST', 'https://api.iugu.com/v1/charge', $objectArray);

  // retorno OK:
  //   {
  //     "message": "Autorizado",
  //     "errors": [],
  //     "success": true,
  //     "url": "https://faturas.iugu.com/de6f1547-97ac-4fc5-ae23-59bb4a100324-d33b",
  //     "pdf": "https://faturas.iugu.com/de6f1547-97ac-4fc5-ae23-59bb4a100324-d33b.pdf",
  //     "identification": null,
  //     "invoice_id": "DE6F154797AC4FC5AE2359BB4A100324",
  //     "LR": "00"
  // }
  
  function isJson($string) {
    json_decode($string);
    return (json_last_error() == JSON_ERROR_NONE);
  }
  
  
 
 
}