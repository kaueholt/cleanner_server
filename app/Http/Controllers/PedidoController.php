<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;
use App\Http\Controllers\ServicoController;

class PedidoController extends Controller{

    public function getFluxos(){
        $sql = "SELECT 
                    SS.ID,
                    TS.ID AS ID_TIPOSERVICO,
                    TS.DESCRICAO AS SERVICO,
                    S.ID AS ID_STATUS,
                    S.DESCRICAO AS STATUS,
                    SS.SEQUENCIA AS SEQUENCIA
                FROM
                    FAT_STATUS_SERVICO SS
                        LEFT JOIN
                    FAT_STATUS S ON S.ID = SS.ID_STATUS
                        LEFT JOIN
                    FAT_TIPOSERVICO TS ON TS.ID = SS.ID_TIPOSERVICO
                ORDER BY 
                    TS.ID,
                    SEQUENCIA ASC";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function getFluxoServico($id){
        $sql = "SELECT TS.DESCRICAO AS SERVICO, S.DESCRICAO AS STATUS, SS.SEQUENCIA AS SEQUENCIA
                FROM FAT_STATUS_SERVICO SS
                LEFT JOIN FAT_STATUS S ON S.ID = SS.ID_STATUS
                LEFT JOIN FAT_TIPOSERVICO TS ON TS.ID = SS.ID_TIPOSERVICO
                WHERE TS.ID = {$id}
                ORDER BY TS.ID, SEQUENCIA ASC";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function getAvaliacao($id, $ano, $mes){
        $sql = "SELECT A.*, 
                    P.ID_CLIENTE,
                    P.ID_PESSOA_PRESTADOR,
                    P.EMISSAO,
                    P.DATA_ENTREGA,
                    P.DATAFECHAMENTO,
                    P.TOTAL,
                    CLI.NOME
                FROM FAT_PEDIDO_AVALIACAO A
                LEFT JOIN FAT_PEDIDO P ON A.ID_PEDIDO = P.ID
                LEFT JOIN CRM_PESSOA CLI ON P.ID_CLIENTE = CLI.ID
                WHERE P.ID_PESSOA_PRESTADOR = {$id}
                    AND EXTRACT(MONTH FROM P.DATAFECHAMENTO) = {$mes}
                    AND EXTRACT(YEAR FROM P.DATAFECHAMENTO) = {$ano}
                ORDER BY P.ID DESC";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function showPedidosCliente($id){
        $sql  = "SELECT 
                    PED.*,
                    PES.NOME AS NOME_CLIENTE,
                    PRE.NOMEREDUZIDO AS NOME_PRESTADOR,
                    ST.DESCRICAO AS DESC_STATUS,
                    PES.FIREBASE_USER AS FIREBASE_CLIENTE,
                    PRE.FIREBASE_USER AS FIREBASE_PRESTADOR,
                    TE.TEMPO_ENTREGA_HORAS
                FROM FAT_PEDIDO PED
                INNER JOIN CRM_PESSOA PES ON PED.ID_CLIENTE = PES.ID
                INNER JOIN CRM_PESSOA PRE ON PED.ID_PESSOA_PRESTADOR = PRE.ID
                LEFT JOIN FAT_PRESTADOR_TEMPO_ENTREGA PTE ON PED.ID_PRAZO = PTE.ID
                LEFT JOIN FAT_TEMPO_ENTREGA TE ON PTE.ID_TEMPOENTREGA = TE.ID
                LEFT JOIN FAT_STATUS ST ON ST.ID = PED.STATUS
                WHERE
                    PED.ID_CLIENTE = {$id}";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function showPedidoDetalhado($id){
        $sql = "SELECT PED.*, 
                    PES.CUSTOMER_TOKEN_IUGU,
                    PES.NOME AS NOME_CLIENTE, 
                    EPED.ENDERECO AS ENDERECO_PEDIDO,
                    EPED.TELEFONE AS TELEFONE_ENDERECO_PEDIDO,
                    CONCAT(EPED.ENDERECO,', ',EPED.NUMERO,' - ',EPED.COMPLEMENTO,', ',EPED.BAIRRO, ' - ',EPED.CEP, ' - ',EPED.CIDADE,'(',EPED.UF,')') AS CONCAT_ENDERECO_PEDIDO,
                    EP.ENDERECO AS ENDERECO_PRESTADOR,
                    EP.TELEFONE AS TELEFONE_ENDERECO_PRESTADOR,
                    CONCAT(EP.ENDERECO,', ',EP.NUMERO,' - ',EP.COMPLEMENTO,', ',EP.BAIRRO, ' - ',EP.CEP, ' - ',EP.CIDADE,'(',EP.UF,')') AS CONCAT_ENDERECO_PRESTADOR,
                    TE.TEMPO_ENTREGA_HORAS AS PRAZO_HORAS,
                    PES.FIREBASE_USER AS FIREBASE_CLIENTE, 
                    PRE.FIREBASE_USER AS FIREBASE_PRESTADOR,
                    SSF.SEQUENCIA AS SEQUENCIA_FRETE,
                    TSF.DESCRICAO AS TIPO_SERVICO_FRETE,
                    SF.DESCRICAO AS STATUS_FRETE,
                    SSR.SEQUENCIA AS SEQUENCIA_ROUPAS,
                    TSR.DESCRICAO AS TIPO_SERVICO_ROUPAS,
                    SR.DESCRICAO AS STATUS_ROUPAS,
                    GE.ID AS ID_ARMARIO_ENTREGA,
                    PED.ID_GAVETA_ARMARIO_ENTREGA,
                    GE.SENHA AS SENHA_GAVETA_ENTREGA,
                    EE.ENDERECO AS ENDERECO_ARMARIO_ENTREGA,
                    CONCAT(EE.ENDERECO,', ',EE.NUMERO,' - ',EE.COMPLEMENTO,', ',EE.BAIRRO, ' - ',EE.CEP, ' - ',EE.CIDADE,'(',EE.UF,')') AS CONCAT_ENDERECO_ARMARIO_ENTREGA,
                    GR.ID AS ID_ARMARIO_RETIRADA,
                    ER.ENDERECO AS ENDERECO_ARMARIO_RETIRADA,
                    CONCAT(ER.ENDERECO,', ',ER.NUMERO,' - ',ER.COMPLEMENTO,', ',ER.BAIRRO, ' - ',ER.CEP, ' - ',ER.CIDADE,'(',ER.UF,')') AS CONCAT_ENDERECO_ARMARIO_RETIRADA,
                    PED.ID_GAVETA_ARMARIO_RETIRADA,
                    GR.SENHA AS SENHA_GAVETA_RETIRADA,
                    C.DESCRICAO AS CUPOM_DESCRICAO,
                    IF(C.PERCENTUAL_OU_REAL = 'P', CONCAT(C.DESCONTO,'%'), CONCAT('R$ ', C.DESCONTO)) AS CUPOM_DESCONTO
                FROM FAT_PEDIDO PED 
                LEFT JOIN FAT_CUPOM_PESSOA CP 
                    ON PED.ID_CUPOM_PESSOA = CP.ID
                LEFT JOIN FAT_CUPOM C 
                    ON CP.ID_CUPOM = C.ID 
                LEFT JOIN CRM_PESSOA PES 
                    ON PED.ID_CLIENTE = PES.ID 
                LEFT JOIN CRM_PESSOA PRE 
                    ON PED.ID_PESSOA_PRESTADOR = PRE.ID 
                LEFT JOIN FAT_PRESTADOR_TEMPO_ENTREGA PTE
                    ON PED.ID_PRAZO = PTE.ID
                LEFT JOIN FAT_TEMPO_ENTREGA TE
                    ON PTE.ID_TEMPOENTREGA = TE.ID
                LEFT JOIN FAT_STATUS_SERVICO SSR 
                    ON SSR.ID = PED.ID_STATUS_SERVICO_ROUPAS
                LEFT JOIN FAT_STATUS SR 
                    ON SR.ID = SSR.ID_STATUS
                LEFT JOIN FAT_TIPOSERVICO TSR
                    ON TSR.ID = SSR.ID_TIPOSERVICO
                LEFT JOIN FAT_STATUS_SERVICO SSF 
                    ON SSF.ID = PED.ID_STATUS_SERVICO_FRETE
                LEFT JOIN FAT_STATUS SF
                    ON SF.ID = SSF.ID_STATUS
                LEFT JOIN FAT_TIPOSERVICO TSF
                    ON TSF.ID = SSF.ID_TIPOSERVICO
                LEFT JOIN EST_ARMARIO_GAVETA GE 
                    ON GE.ID = PED.ID_GAVETA_ARMARIO_ENTREGA
                LEFT JOIN EST_ARMARIO_GAVETA GR 
                    ON GR.ID = PED.ID_GAVETA_ARMARIO_RETIRADA
                LEFT JOIN CRM_PESSOA PE
                    ON GE.ID_PESSOA = PE.ID
                    AND PE.TIPOPESSOA = 'W'
                LEFT JOIN CRM_PESSOA PR 
                    ON GR.ID_PESSOA = PR.ID
                    AND PR.TIPOPESSOA = 'W'
                LEFT JOIN CRM_ENDERECO_PESSOA EPED
                    ON PED.ID_ENDERECO_PESSOA = EPED.ID
                LEFT JOIN CRM_ENDERECO_PESSOA EP
                    ON PED.ID_PESSOA_PRESTADOR = EP.ID_PESSOA
                LEFT JOIN CRM_ENDERECO_PESSOA EE
                    ON PE.ID = EE.ID_PESSOA
                LEFT JOIN CRM_ENDERECO_PESSOA ER
                    ON PR.ID = ER.ID_PESSOA
                WHERE PED.ID = {$id}";      
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function calculaComissaoPedido($id){
        $valorTotal = 0;        
        $valorTotalVezesComissao = 0;        
        $sql = "SELECT IF(VALOR_TOTAL != 0, VALOR_TOTAL, (VALOR_UNITARIO * QUANTIDADE)) AS VALOR_TOTAL,
                    COMISSAO
                FROM
                    FAT_PEDIDO_ITEM
                WHERE
                    ID_PEDIDO = {$id}";
        $response = DB::select($sql);
        foreach($response as $comissao){
            $valorTotal += $comissao->VALOR_TOTAL;
            $valorTotalVezesComissao += ($comissao->VALOR_TOTAL * $comissao->COMISSAO);
        }
        $percentTotalComissao = round(($valorTotalVezesComissao / $valorTotal),4);
        $valorTotalComissao = round(($valorTotal * $percentTotalComissao / 100),2);
        $response = array(
            'percentualTotalComissao' => $percentTotalComissao,
            'valorTotalComissao' => $valorTotalComissao
        );
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    // end point para retornar pedidos com taxa de adm e valor liquido a receber do prestador
    public function showPedidosValorReceber($id, $dataIni, $dataFim){
        $sql  = "SELECT P.ID, P.EMISSAO, P.TOTAL, P.TAXA_ENTREGA_FRETE_REAIS, AVG(COMISSAO) AS TAXA_ADM, ";
        $sql  .= "((P.TOTAL - P.TAXA_ENTREGA_FRETE_REAIS) * (AVG(I.COMISSAO)/100)) AS VALOR_TAXA_ADM, ";
        $sql  .= "(P.TOTAL - ((P.TOTAL - P.TAXA_ENTREGA_FRETE_REAIS) * (AVG(I.COMISSAO)/100))) AS VALOR_LIQUIDO ";
        $sql  .= "FROM FAT_PEDIDO P ";
        $sql  .= "INNER JOIN FAT_PEDIDO_ITEM I ON P.ID=I.ID_PEDIDO ";
        $sql  .= "WHERE P.ID_PESSOA_PRESTADOR=".$id;
        $sql  .= " AND P.EMISSAO>='".$dataIni."' and P.EMISSAO <='".$dataFim."' ";
        $sql  .= "GROUP BY P.EMISSAO, P.ID, P.TOTAL, P.TAXA_ENTREGA_FRETE_REAIS ";
        $sql  .= "ORDER BY P.EMISSAO";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    // param id = id pessoa prestador
    public function showPedidosPrestador($id){
        $sql  = "SELECT PED.*, PES.NOME AS NOME_CLIENTE, 
                    PES.FIREBASE_USER AS FIREBASE_CLIENTE, 
                    PRE.FIREBASE_USER AS FIREBASE_PRESTADOR,
                    SSF.SEQUENCIA AS SEQUENCIA_FRETE,
                    TSF.DESCRICAO AS TIPO_SERVICO_FRETE,
                    SF.DESCRICAO AS STATUS_FRETE,
                    SSR.SEQUENCIA AS SEQUENCIA_ROUPAS,
                    TSR.DESCRICAO AS TIPO_SERVICO_ROUPAS,
                    SR.DESCRICAO AS STATUS_ROUPAS
                FROM FAT_PEDIDO PED 
                LEFT JOIN FAT_STATUS_SERVICO SSR ON SSR.ID = PED.ID_STATUS_SERVICO_ROUPAS
                LEFT JOIN FAT_STATUS SR ON SR.ID = PED.STATUS
                LEFT JOIN FAT_TIPOSERVICO TSR ON TSR.ID = SSR.ID_TIPOSERVICO
                LEFT JOIN FAT_STATUS_SERVICO SSF ON SSF.ID = PED.ID_STATUS_SERVICO_FRETE
                LEFT JOIN FAT_STATUS SF ON SF.ID = SSF.ID_STATUS
                LEFT JOIN FAT_TIPOSERVICO TSF ON TSF.ID = SSF.ID_TIPOSERVICO
                LEFT JOIN CRM_PESSOA PES ON PED.ID_CLIENTE = PES.ID 
                LEFT JOIN CRM_PESSOA PRE ON PED.ID_PESSOA_PRESTADOR = PRE.ID 
                WHERE PED.ID_PESSOA_PRESTADOR = {$id}";
        $response = DB::select($sql);
        $valorTotal = 0;        
        $valorTotalVezesComissao = 0;        
        foreach($response as $pedido){
            $sql = "SELECT 
                        PI.ID,
                        PI.ID_ROUPA,
                        R.DESCRICAO,
                        PI.QUANTIDADE,
                        PI.VALOR_UNITARIO,
                        PI.VALOR_TOTAL,
                        PI.COMISSAO
                    FROM
                        FAT_PEDIDO_ITEM PI
                            LEFT JOIN
                        EST_ROUPA R ON R.ID = PI.ID_ROUPA
                    WHERE
                        PI.ID_PEDIDO = {$pedido->ID} ";
            $responsePedidoItem = DB::select($sql);
            $pedido->ITENS_PEDIDO = $responsePedidoItem;
            foreach($responsePedidoItem as $comissao){
                $valorTotal += $comissao->VALOR_TOTAL;
                $valorTotalVezesComissao += ($comissao->VALOR_TOTAL * $comissao->COMISSAO);
            }
            $percentTotalComissao = $valorTotal ? round(($valorTotalVezesComissao / $valorTotal),4) : 0;
            $valorTotalComissao = round(($valorTotal * $percentTotalComissao / 100),2);
            $responseComissao = array(
                'percentualTotalComissao' => $percentTotalComissao,
                'valorTotalComissao' => $valorTotalComissao
            );
            $pedido->COMISSAO = $responseComissao;
        }
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function getStatusServicoRoupa($id){
        $sql = 'SELECT SS.ID, TS.DESCRICAO AS SERVICO, S.DESCRICAO AS STATUS, SS.SEQUENCIA
                FROM FAT_PEDIDO P
                LEFT JOIN FAT_STATUS_SERVICO SS ON P.ID_STATUS_SERVICO_ROUPAS = SS.ID
                LEFT JOIN FAT_STATUS S ON SS.ID_STATUS = S.ID
                LEFT JOIN FAT_TIPOSERVICO TS ON TS.ID = SS.ID_TIPOSERVICO
                WHERE P.ID = '.$id;     
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function calculaDataPrevistaEntrega($horas){
        $segundos = intval($horas) * 60 * 60;
        $dataEntrega = date('Y-m-d H:i:s');
        while($segundos > 0){
            $diaSemana = intval(date('w', strtotime($dataEntrega)));
            if($diaSemana === 0) //domingo
                $dataEntrega = date('Y-m-d H:i:s', strtotime(date("Y-m-d", strtotime($dataEntrega ." + 1 days")).' 08:00:00'));
            if($diaSemana === 6) //sabado
                $dataEntrega = date('Y-m-d H:i:s', strtotime(date("Y-m-d", strtotime($dataEntrega ." + 2 days")).' 08:00:00'));
            $fimExpediente = date('Y-m-d H:i:s',strtotime(date("Y-m-d", strtotime($dataEntrega)).' 18:00:00'));
            $segundosRestantesAteFimExpediente = strtotime($fimExpediente) - strtotime($dataEntrega);
            if($segundos < $segundosRestantesAteFimExpediente )
                $dataEntrega = date('Y-m-d H:i:s', strtotime($dataEntrega . " + {$segundos} seconds"));
            else{
                $dataEntrega = date('Y-m-d H:i:s', strtotime($dataEntrega . " + 1 days"));
                $dataEntrega = date('Y-m-d H:i:s',strtotime(date("Y-m-d", strtotime($dataEntrega)).' 08:00:00'));
            }
            $segundos=$segundos-$segundosRestantesAteFimExpediente;          
        }
        return $dataEntrega;
    }

    public function calculaDataPrevistaEntregaHorasCorridas($horas){ //não desconta horários após 18h nem finais de semana.
        $segundos = intval($horas) * 60 * 60;
        $dataEntrega = date('Y-m-d H:i:s');
        $dataEntregaHorasCorrida = date('Y-m-d H:i:s', strtotime($dataEntrega . " + {$segundos} seconds"));
        return $dataEntregaHorasCorrida;
    }

    public function getStatusServicoFrete($id){
        $sql = 'SELECT SS.ID, TS.DESCRICAO AS SERVICO, S.DESCRICAO AS STATUS, SS.SEQUENCIA
                FROM FAT_PEDIDO P
                LEFT JOIN FAT_STATUS_SERVICO SS ON P.ID_STATUS_SERVICO_FRETE = SS.ID
                LEFT JOIN FAT_STATUS S ON SS.ID_STATUS = S.ID
                LEFT JOIN FAT_TIPOSERVICO TS ON TS.ID = SS.ID_TIPOSERVICO
                WHERE P.ID = '.$id;           
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function getStatusServicos($id){
        $sql = "SELECT TS.DESCRICAO AS SERVICO_ROUPAS,
                    S.DESCRICAO AS STATUS_ROUPAS,
                    SS.ID AS ID_STATUS_SERVICO_ROUPAS,
                    SS.SEQUENCIA AS SEQUENCIA_ROUPAS,
                    TSF.DESCRICAO AS SERVICO_FRETE,
                    SF.DESCRICAO AS STATUS_FRETE,
                    SSF.ID AS ID_STATUS_SERVICO_FRETE,
                    SSF.SEQUENCIA AS SEQUENCIA_FRETE
                FROM FAT_PEDIDO P
                LEFT JOIN FAT_STATUS_SERVICO SS 
                    ON P.ID_STATUS_SERVICO_ROUPAS = SS.ID
                LEFT JOIN FAT_STATUS_SERVICO SSF 
                    ON P.ID_STATUS_SERVICO_FRETE = SSF.ID
                LEFT JOIN FAT_STATUS S 
                    ON SS.ID_STATUS = S.ID
                LEFT JOIN FAT_STATUS SF 
                    ON SSF.ID_STATUS = SF.ID
                LEFT JOIN FAT_TIPOSERVICO TS 
                    ON TS.ID = SS.ID_TIPOSERVICO
                LEFT JOIN FAT_TIPOSERVICO TSF 
                    ON TSF.ID = SSF.ID_TIPOSERVICO
                WHERE P.ID = {$id}";           
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function decrementServicoFrete($id){
        $pedido = $this->getStatusServicos($id)->getData()->data[0];
        if(!$pedido->SEQUENCIA_ROUPAS || !$pedido->SEQUENCIA_FRETE)
            return Handles::jsonResponse(false, 'Pedido '.$id.' está com status cancelado!', $pedido, 500);
        $sqlFrete = "SELECT ID_TIPOSERVICO, SEQUENCIA FROM FAT_STATUS_SERVICO WHERE ID = {$pedido->ID_STATUS_SERVICO_FRETE}";
        $frete = DB::select($sqlFrete);
        $sqlFreteNew = 'SELECT ID FROM FAT_STATUS_SERVICO WHERE ID_TIPOSERVICO = '.$frete[0]->ID_TIPOSERVICO.' AND SEQUENCIA = '.($frete[0]->SEQUENCIA-1);
        $freteNew = DB::select($sqlFreteNew);
        if(!isset($freteNew[0]))
        return Handles::jsonResponse(false, "O serviço de " . strtolower($pedido->SERVICO_FRETE) . " do pedido {$id} já está em seu estado mínimo: " . strtolower($pedido->STATUS_FRETE) . "!", $pedido, 500);
        $response = DB::table('FAT_PEDIDO')->where('ID',$id)->update(['ID_STATUS_SERVICO_FRETE'=>$freteNew[0]->ID]);
        return $response 
            ? Handles::jsonResponse(true, 'Pedido '.$id.' atualizado!', $freteNew[0]->ID, 200)
            : Handles::jsonResponse(false, "Não foi possível atualizar o servico do pedido {$id}!", $freteNew, 500);
    }

    public function incrementServicoFrete($id){
        $pedido = $this->getStatusServicos($id)->getData()->data[0];
        if(!$pedido->SEQUENCIA_ROUPAS || !$pedido->SEQUENCIA_FRETE)
            return Handles::jsonResponse(false, 'Pedido '.$id.' está com status cancelado!', $pedido, 500);
        $sqlFrete = 'SELECT ID_TIPOSERVICO, SEQUENCIA FROM FAT_STATUS_SERVICO WHERE ID = '.$pedido->ID_STATUS_SERVICO_FRETE;
        $frete = DB::select($sqlFrete);
        $sqlFreteNew = "SELECT ID FROM FAT_STATUS_SERVICO WHERE ID_TIPOSERVICO = {$frete[0]->ID_TIPOSERVICO} AND SEQUENCIA = ({$frete[0]->SEQUENCIA}+1) LIMIT 1";
        $sqlChecaSeEstaFinalizando = "SELECT ID FROM FAT_STATUS_SERVICO WHERE ID_TIPOSERVICO = {$frete[0]->ID_TIPOSERVICO} AND SEQUENCIA = ({$frete[0]->SEQUENCIA}+2) LIMIT 1";
        $freteNew = DB::select($sqlFreteNew);
        $naoFinalizando = DB::select($sqlChecaSeEstaFinalizando);
        if(!isset($freteNew[0]))
        return Handles::jsonResponse(false, "O serviço de " . strtolower($pedido->SERVICO_FRETE) . " do pedido {$id} já está em seu estado máximo: " . strtolower($pedido->STATUS_FRETE) . "!", $pedido, 500);
        $response = isset($naoFinalizando[0]) 
            ? DB::table('FAT_PEDIDO')->where('ID',$id)->update(['ID_STATUS_SERVICO_FRETE'=>$freteNew[0]->ID])
            : DB::table('FAT_PEDIDO')->where('ID',$id)->update(['ID_STATUS_SERVICO_FRETE'=>$freteNew[0]->ID,'DATAFECHAMENTO'=>now()]);
        return $response 
            ? Handles::jsonResponse(true, 'Pedido '.$id.' atualizado!', $freteNew[0]->ID, 200)
            : Handles::jsonResponse(false, "Não foi possível atualizar o servico do pedido {$id}!", $freteNew, 500);
    }

    public function decrementServicoRoupa($id){
        $pedido = $this->getStatusServicos($id)->getData()->data[0];
        if(!$pedido->SEQUENCIA_ROUPAS || !$pedido->SEQUENCIA_FRETE)
            return Handles::jsonResponse(false, 'Pedido '.$id.' está com status cancelado!', $pedido, 500);
        $sqlRoupas = 'SELECT ID_TIPOSERVICO, SEQUENCIA FROM FAT_STATUS_SERVICO WHERE ID = '.$pedido->ID_STATUS_SERVICO_ROUPAS;
        $roupas = DB::select($sqlRoupas);
        $sqlRoupasNew = 'SELECT ID FROM FAT_STATUS_SERVICO WHERE ID_TIPOSERVICO = '.$roupas[0]->ID_TIPOSERVICO.' AND SEQUENCIA = '.($roupas[0]->SEQUENCIA-1);
        $roupasNew = DB::select($sqlRoupasNew);
        if(!isset($roupasNew[0]))
        return Handles::jsonResponse(false, "O serviço de " . strtolower($pedido->SERVICO_ROUPAS) . " do pedido {$id} já está em seu estado mínimo: " . strtolower($pedido->STATUS_ROUPAS) . "!", $pedido, 500);
        $response = DB::table('FAT_PEDIDO')->where('ID',$id)->update(['ID_STATUS_SERVICO_ROUPAS'=>$roupasNew[0]->ID]);
        return $response 
            ? Handles::jsonResponse(true, 'Pedido '.$id.' atualizado!', $roupasNew[0]->ID, 200)
            : Handles::jsonResponse(false, "Não foi possível atualizar o servico do pedido {$id}!", $roupasNew, 500);
    }

    public function incrementServicoRoupa($id){
        $pedido = $this->getStatusServicos($id)->getData()->data[0];
        if(!$pedido->SEQUENCIA_ROUPAS || !$pedido->SEQUENCIA_FRETE)
            return Handles::jsonResponse(false, 'Pedido '.$id.' está com status cancelado!', $pedido, 500);
        $sqlRoupas = 'SELECT ID_TIPOSERVICO, SEQUENCIA FROM FAT_STATUS_SERVICO WHERE ID = '.$pedido->ID_STATUS_SERVICO_ROUPAS;
        $roupas = DB::select($sqlRoupas);
        $sqlRoupasNew = 'SELECT ID FROM FAT_STATUS_SERVICO WHERE ID_TIPOSERVICO = '.$roupas[0]->ID_TIPOSERVICO.' AND SEQUENCIA = '.($roupas[0]->SEQUENCIA+1);
        $roupasNew = DB::select($sqlRoupasNew);
        if(!isset($roupasNew[0]))
        return Handles::jsonResponse(false, "O serviço de " . strtolower($pedido->SERVICO_ROUPAS) . " do pedido {$id} já está em seu estado máximo: " . strtolower($pedido->STATUS_ROUPAS) . "!", $pedido, 500);
        $response = DB::table('FAT_PEDIDO')->where('ID',$id)->update(['ID_STATUS_SERVICO_ROUPAS'=>$roupasNew[0]->ID]);
        return $response 
            ? Handles::jsonResponse(true, 'Pedido '.$id.' atualizado!', array('ID_STATUS_SERVICO_ROUPAS' => $roupasNew[0]->ID), 200)
            : Handles::jsonResponse(false, "Não foi possível atualizar o servico do pedido {$id}!", $roupasNew, 500);
    }

    // altera o status do pedido OS 20000240
    public function alteraStatusPedido($id, $idStatus){
        $response = DB::table('FAT_PEDIDO')->where('ID',$id)->update(['STATUS'=>$idStatus]);
        return $response 
            ? Handles::jsonResponse(true, 'Pedido '.$id.' Status atualizado!', $idStatus, 200)
            : Handles::jsonResponse(false, "Não foi possível atualizar o status do pedido {$id}!", $idStatus, 500);
    }

    // altera o status da avaliação do pedido OS 20000240
    public function alteraStatusAvaliacaoPedido($id, $idStatusAvaliacao){
        $response = DB::table('FAT_PEDIDO')->where('ID',$id)->update(['STATUS_AVALIACAO'=>$idStatusAvaliacao]);
        return $response 
            ? Handles::jsonResponse(true, 'Pedido '.$id.' Status avaliação atualizado!', '', 200)
            : Handles::jsonResponse(false, "Não foi possível atualizar o status do pedido {$id}!", '', 500);
    }


    public function getSenhaLocker($id){
        $sql = "SELECT 
                    P.ID AS ID_PEDIDO,
                    GE.ID AS ID_ARMARIO_ENTREGA,
                    P.ID_GAVETA_ARMARIO_ENTREGA,
                    GE.SENHA AS SENHA_GAVETA_ENTREGA,
                    EE.ENDERECO AS ENDERECO_ARMARIO_ENTREGA,
                    GR.ID AS ID_ARMARIO_RETIRADA,
                    ER.ENDERECO AS ENDERECO_ARMARIO_RETIRADA,
                    P.ID_GAVETA_ARMARIO_RETIRADA,
                    GR.SENHA AS SENHA_GAVETA_RETIRADA
                FROM
                    FAT_PEDIDO P
                        LEFT JOIN
                    EST_ARMARIO_GAVETA GE ON GE.ID = P.ID_GAVETA_ARMARIO_ENTREGA
                        LEFT JOIN
                    EST_ARMARIO_GAVETA GR ON GR.ID = P.ID_GAVETA_ARMARIO_RETIRADA
                        LEFT JOIN
                    CRM_PESSOA PE ON GE.ID_PESSOA = PE.ID
                        AND PE.TIPOPESSOA = 'W'
                        LEFT JOIN
                    CRM_PESSOA PR ON GR.ID_PESSOA = PR.ID
                        AND PR.TIPOPESSOA = 'W'
                        LEFT JOIN
                    CRM_ENDERECO_PESSOA EE ON PE.ID = EE.ID_PESSOA
                        LEFT JOIN
                    CRM_ENDERECO_PESSOA ER ON PR.ID = ER.ID_PESSOA
                WHERE
                    (P.ID_GAVETA_ARMARIO_ENTREGA IS NOT NULL
                        OR P.ID_GAVETA_ARMARIO_RETIRADA IS NOT NULL)
                    AND P.ID = {$id}
                LIMIT 1";
        $response = DB::select($sql);
        return $response 
            ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function calculaTotalPedido(Request $request){
        $payload = $request->all();
        $ID_PESSOA_PESSOAPRESTADOR = $payload['ID_PESSOA_PESSOAPRESTADOR'];
        $ID_TEMPO_ENTREGA = $payload['ID_TEMPO_ENTREGA'];
        $ID_TIPOSERVICO_FRETE = $payload['ID_TIPOSERVICO_FRETE'];
        // $ID_TIPOSERVICO_ROUPAS = $payload['ID_TIPOSERVICO_ROUPAS'];
        $ID_CUPOM_PESSOA = isset($payload['ID_CUPOM_PESSOA']) ? $payload['ID_CUPOM_PESSOA'] : false;
        
        $roupa = new RoupaController();
        $totalRoupasReais = isset($payload['VALOR_TOTAL_ROUPAS']) ? 
                            (float) $payload['VALOR_TOTAL_ROUPAS'] 
                            : $roupa->somaValores($payload['ROUPAS']);

        $prestador = new PrestadorController();
        $descontoTempoEntregaPercent = $prestador->getDescontoTempoEntrega($ID_PESSOA_PESSOAPRESTADOR,$ID_TEMPO_ENTREGA)->getData();
        $descontoTempoEntregaPercent = $descontoTempoEntregaPercent->success ? 
            $descontoTempoEntregaPercent->data[0]->DESCONTO : 0;
        
        $taxaEntregaReais = $prestador->getPrestadorTaxaEntrega($ID_PESSOA_PESSOAPRESTADOR,$ID_TIPOSERVICO_FRETE)->getData();
        $taxaEntregaReais = $taxaEntregaReais->success ? 
            $taxaEntregaReais->data[0]->PRESTADOR_TAXA_ENTREGA_REAIS : 0;

        if($ID_CUPOM_PESSOA){
            $default = new DefaultController();
            $cupomPessoa = $default->show('cupompessoa',$ID_CUPOM_PESSOA)->getData();
            $cupom = $cupomPessoa->success ? !$cupomPessoa->data->UTILIZADO ? $cupomPessoa->data->ID_CUPOM : 'utilizado' : false;
            if($cupom == 'utilizado')
                return Handles::jsonResponse(false, 'Cupom já utilizado!', $cupomPessoa, 500);
            if($cupom){
                $cupom = $default->show('cupom',$cupom)->getData();
                if($cupom->success){
                    $cupom = $cupom->data;
                    $cupomDataValidade = $cupom->VALIDADEFIM;
                    if($cupomDataValidade <= date('Y-m-d h:i:s'))
                        return Handles::jsonResponse(false, 'Cupom vencido!', $cupom, 500);
                    $cupomDesconto = $cupom->DESCONTO > 0 ? $cupom->DESCONTO : 0;
                    $cupomUnidade = $cupom->PERCENTUAL_OU_REAL;
                }
            }
        }
        //ATENÇÃO! O cupom não influencia a taxa de entrega
        if(isset($cupomDesconto) && isset($cupomUnidade)){
            if($cupomUnidade == 'P'){
                $totalDescontosPercent = $descontoTempoEntregaPercent + $cupomDesconto;
                $total = ($totalRoupasReais * (100-$totalDescontosPercent) / 100 ) + $taxaEntregaReais;
                $cupom = $cupomDesconto . '%';
            }else if($cupomUnidade == 'R'){
                $total = (($totalRoupasReais - $cupomDesconto) * (100-$descontoTempoEntregaPercent) / 100 ) + $taxaEntregaReais;
                $cupom = 'R$ ' . $cupomDesconto;
            }else{
                $total = ($totalRoupasReais * (100-$descontoTempoEntregaPercent) / 100 ) + $taxaEntregaReais;
                $cupom = false;
            }
        }else{
            $total = ($totalRoupasReais * (100-$descontoTempoEntregaPercent) / 100 ) + $taxaEntregaReais;
            $cupom = false;
        }
        $response = array(
            'prestador' => $ID_PESSOA_PESSOAPRESTADOR,
            'cupom' => $cupom,
            'totalRoupas' => $totalRoupasReais,
            'descontoPrestadorTempoEntrega' => $descontoTempoEntregaPercent,
            'taxaEntregaPrestadorReais' => $taxaEntregaReais,
            'total' => $total
        );
        return $total 
            ? Handles::jsonResponse(true, 'Valor calculado!', $response, 200) 
            : Handles::jsonResponse(false, 'Não foi possível realizar o cálculo!', [], 500);
    }
}
