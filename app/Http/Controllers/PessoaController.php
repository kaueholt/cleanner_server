<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;
use App\Http\Controllers\PrestadorController;
use App\Http\Controllers\PedidoController;

class PessoaController extends Controller{


    // busca prestadores aplicando filtro
    // calcula total do pedido por prestador selecionado
    public function getPrestadoresFiltro_admin(Request $request){
        $payload = $request->all();
        // sempre retorno ordenado pelo menor preço
        // cria lista de id_roupa
        $listaDeIdRoupas = [];
        $roupaQuant = [];
        for($i=0; $i<count($payload['ROUPAS']); $i++){
            $listaDeIdRoupas[$i] = $payload['ROUPAS'][$i]['ID_ROUPA'];
            $roupaQuant[$payload['ROUPAS'][$i]['ID_ROUPA']] = $payload['ROUPAS'][$i]['QUANTIDADE'];
        }
        $idRoupasStringged = '('. implode(',',$listaDeIdRoupas).')'; //Ex. (1,2,33,44)

        // tipo de pessoa ( P - prestador / W - locker)
        $tipoPessoa = isset($payload['TIPO_PESSOA']) ? strtoupper($payload['TIPO_PESSOA']) : false;
        $idEnderecoLocker = isset($payload['ID_ENDERECO_LOCKER']) ? strtoupper($payload['ID_ENDERECO_LOCKER']) : false;

        // OK - cidade
        $cidade = isset($payload['CIDADE']) ? strtoupper($payload['CIDADE']) : false;
        // OK - tipo de serviço
        $lavar = isset($payload['LAVAR']) ? $payload['LAVAR'] : false;
        $passar = isset($payload['PASSAR']) ? $payload['PASSAR'] : false;
        $campoPreco = ($lavar && $passar) ? 'PRECO_LAVAR_E_PASSAR': ($lavar ? 'PRECO_LAVAR' : 'PRECO_PASSAR');
        
        // OK - tipo entrega
        $entregaClienteBuscaPedido = isset($payload['ENTREGA_CLIENTE_BUSCA_PEDIDO']) ? $payload['ENTREGA_CLIENTE_BUSCA_PEDIDO'] : false;
        $entregaDomicilio = isset($payload['ENTREGA_DOMICILIO']) ? $payload['ENTREGA_DOMICILIO'] : false;
        $entregaArmario = isset($payload['ENTREGA_ARMARIO']) ? $payload['ENTREGA_ARMARIO'] : false;

        // OK - id tempo entrega, para verificar se o prestador entrega no tempo solicitado
        $tempoMaximoEntregaHoras = isset($payload['TEMPO_MAXIMO_ENTREGA_HORAS']) ? $payload['TEMPO_MAXIMO_ENTREGA_HORAS'] : false;
        $idTempoEntrega = isset($payload['ID_TEMPO_ENTREGA']) ? $payload['ID_TEMPO_ENTREGA'] : false;

        // distancia máxima para buscar prestador
        $distanciaMaxima = isset($payload['DISTANCIA_MAXIMA']) ? $payload['DISTANCIA_MAXIMA'] : false;

        // id endereço cliente para buscar latitude e longitude para calcular a distancia 
        // SQL para buscar latitude e longitude do cliente        
        $idEnderecoCliente = $payload['ID_ENDERECO_CLIENTE'];
        $sqlEndCliente = "SELECT ID_PESSOA, LATITUDE, LONGITUDE FROM CRM_ENDERECO_PESSOA WHERE ID=".$idEnderecoCliente;
        $responseEnderecoCliente = DB::select($sqlEndCliente);
        // testa Prestador endereço cliente
        if(!count($responseEnderecoCliente))
            return Handles::jsonResponse(false, 'Endereço do cliente não encontrado!', [], 404);
        $idCliente = $responseEnderecoCliente[0]->ID_PESSOA;
        
        // Select prestador por filtros
        $sqlPrestador = "SELECT DISTINCT PS.ID, PS.NOME, PS.NOMEREDUZIDO, US.TIPO, FV.ID as ID_FAVORITO, IF(FV.ID,TRUE,FALSE) AS FAVORITO, ";
        $sqlPrestador .= "PP.MAX_CLIENTES_DIA, PP.MAX_DISTANCIA_ATENDIMENTO, PP.NUMERO_DE_RATINGS, PP.RATING, PP.VALOR_MINIMO_PEDIDO, PP.PAUSADO, PP.LAVA, PP.PASSA, PP.ENTREGA_ARMARIO, ";
        $sqlPrestador .= "PP.ENTREGA_DOMICILIO, PP.ENTREGA_CLIENTE_BUSCA_PEDIDO, PP.TAXA_ENTREGA_ARMARIO_REAIS, PP.TAXA_ENTREGA_DOMICILIO_REAIS, PP.TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS, ";
        $sqlPrestador .= "PE.LONGITUDE, PE.LATITUDE, PE.ENDERECO, PE.NUMERO, PE.COMPLEMENTO, PE.BAIRRO, PE.CIDADE, PE.UF, PE.CEP, PE.TELEFONE, PT.ID_TEMPOENTREGA, PT.ID AS ID_PRESTADOR_TEMPO, PT.DESCONTO ";
        $sqlPrestador .= "FROM admser_cleaner.CRM_PESSOA PS ";
        $sqlPrestador .= "INNER JOIN CRM_PESSOA_PRESTADOR PP ON (PP.ID_PESSOA=PS.ID) ";
        $sqlPrestador .= "INNER JOIN CRM_ENDERECO_PESSOA PE ON (PE.ID_PESSOA=PS.ID) ";
        $sqlPrestador .= "INNER JOIN AMB_USUARIO US ON (US.ID_PESSOA=PS.ID) ";
        $sqlPrestador .= "LEFT JOIN CRM_PRESTADOR_FAVORITO_CLIENTE FV 
                            ON PS.ID = FV.ID_PESSOA_PRESTADOR 
                            AND FV.ID_PESSOA_CLIENTE = $idCliente ";
        $sqlPrestador .= "INNER JOIN FAT_PRESTADOR_TEMPO_ENTREGA PT ON (PT.ID_PESSOA_PESSOAPRESTADOR=PS.ID) ";
        $sqlPrestador .= "WHERE PS.TIPOPESSOA='P' AND PP.PAUSADO = FALSE ";
        // $sqlPrestador .= "AND PP.MAX_DISTANCIA_ATENDIMENTO>=$distanciaMaxima ";
        $sqlPrestador .= "AND PT.ID_TEMPOENTREGA=$idTempoEntrega ";
        $sqlPrestador .= (($cidade <> false) ? "AND PE.CIDADE='$cidade' " : null);
        $sqlPrestador .= $lavar ? "AND PP.LAVA = TRUE " : null;
        $sqlPrestador .= $passar ? "AND PP.PASSA = TRUE " : null;
        $sqlPrestador .= $entregaClienteBuscaPedido ? "AND PP.ENTREGA_CLIENTE_BUSCA_PEDIDO = TRUE " : null;
        $sqlPrestador .= $entregaDomicilio ? "AND PP.ENTREGA_DOMICILIO = TRUE " : null;
        $sqlPrestador .= $entregaArmario ? "AND PP.ENTREGA_ARMARIO = TRUE " : null;
        $sqlPrestador .= "ORDER BY FAVORITO DESC";
        
        $responsePrestador = DB::select($sqlPrestador);
        if(!count($responsePrestador))
            return Handles::jsonResponse(false, 'Nenhum prestador encontrado!', [], 404);
        
        // calcula total de pedidos por prestador e distancia entre cliente e prestador
        for($i=0; $i < count($responsePrestador); $i++){
            $distancia = 
            $this->calculaDistanciaAPartirDasLatitudesLongitudes(
                $responseEnderecoCliente[0]->LATITUDE, 
                $responseEnderecoCliente[0]->LONGITUDE, 
                $responsePrestador[$i]->LATITUDE, 
                $responsePrestador[$i]->LONGITUDE);
            if ($distancia <= $distanciaMaxima){
                $responsePrestador[$i]->DISTANCIAKM = $distancia;
                // calcular o valor do pedido, selecionar somente prestador com valores > 0
                // OK - verificar se o prestador atende todas as roupas do pedido, verificar por grupo
                // CALCULA TOTAL PEDIDO POR PRESTADOR
                $desconto = $responsePrestador[$i]->DESCONTO / 100;
                $idPrestador = $responsePrestador[$i]->ID;
                $sqlPrecoRoupas = "SELECT R.ID, R.DESCRICAO, P.COMISSAO, G.DESCONTO, {$campoPreco}, ";
                $sqlPrecoRoupas .= "(R.{$campoPreco} - (R.{$campoPreco} * {$desconto}) - ((R.{$campoPreco} * G.DESCONTO)/100)) AS TOTALROUPAUNITARIO ";
                $sqlPrecoRoupas .= "FROM admser_cleaner.EST_ROUPA R ";
                $sqlPrecoRoupas .= "INNER JOIN EST_GRUPO_PRESTADOR G ON (R.GRUPO=G.ID_GRUPO) ";
                $sqlPrecoRoupas .= "INNER JOIN EST_GRUPO P ON (R.GRUPO=P.GRUPO) ";
                $sqlPrecoRoupas .= "WHERE G.ID_PESSOA_PESSOAPRESTADOR = {$idPrestador} and R.ID IN {$idRoupasStringged} ";
                $responsePrecoRoupas = DB::select($sqlPrecoRoupas);
                if(count($payload['ROUPAS']) != sizeof($responsePrecoRoupas)){
                    array_splice($responsePrestador, $i--, 1);
                }else{
                    $totalPedido = 0;
                    $descontoReais = 0;
                    $totalRoupasSemDesconto = 0;
                    for($r=0; $r < sizeof($responsePrecoRoupas); $r++){
                        $totalRoupa = $responsePrecoRoupas[$r]->TOTALROUPAUNITARIO * $roupaQuant[$responsePrecoRoupas[$r]->ID];
                        $totalRoupaSemDesconto = $responsePrecoRoupas[$r]->{$campoPreco} * $roupaQuant[$responsePrecoRoupas[$r]->ID];
                        $totalPedido += $totalRoupa;
                        // var_dump($responsePrecoRoupas[$r]->{$campoPreco} - $responsePrecoRoupas[$r]->TOTALROUPA);
                        $totalRoupasSemDesconto += $totalRoupaSemDesconto;
                        $responsePrecoRoupas[$r]->QUANT = $roupaQuant[$responsePrecoRoupas[$r]->ID]; // BUSCA A QUANTIDADE DA LISTA
                        $responsePrecoRoupas[$r]->TOTALROUPA = $totalRoupa;
                        // $descontoReais += ($responsePrecoRoupas[$r]->{$campoPreco} - $responsePrecoRoupas[$r]->TOTALROUPA) * $roupaQuant[$responsePrecoRoupas[$r]->ID];
                    }
                    $taxaEntrega = $entregaClienteBuscaPedido
                        ?   $responsePrestador[$i]->TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS 
                        :   ($entregaDomicilio 
                            ?   $responsePrestador[$i]->TAXA_ENTREGA_DOMICILIO_REAIS 
                            :   ($responsePrestador[$i]->ENTREGA_ARMARIO
                                ?   $responsePrestador[$i]->TAXA_ENTREGA_ARMARIO_REAIS
                                :   0));
                    $responsePrestador[$i]->DESCONTO_REAIS = $totalRoupasSemDesconto - $totalPedido;
                    $responsePrestador[$i]->VALOR_TOTAL_ROUPAS = $totalPedido;
                    $responsePrestador[$i]->TOTALPEDIDO = $totalPedido + $taxaEntrega;
                    $responsePrestador[$i]->ROUPAS = $responsePrecoRoupas;
                }                
            } else {
                array_splice($responsePrestador, $i--, 1);
            }
        }
        // var_dump(count($responsePrestador));
        // var_dump(gettype($responsePrestador));
        $responsePrestador = isset($responsePrestador[0]) 
            ?   $responsePrestador
            :   json_decode(json_encode($responsePrestador),true);
        // var_dump($responsePrestador);
        // var_dump(json_decode(json_encode($responsePrestador),true)); //antônio pediu para transformar a resposta de objeto de objetos para array de objetos
        // $response = [
        //   $pedido = $responsePrestador,
        //   $roupas = $responsePrecoRoupas
        // ];
        return sizeof($responsePrestador) 
            ?   Handles::jsonResponse(true, 'Registros encontrados!', $responsePrestador, 200) 
            :   Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }


    // busca prestadores aplicando filtro
    // calcula total do pedido por prestador selecionado
    public function getLockerFiltro(Request $request){
        $payload = $request->all();
        // OK - cidade
        $cidade = isset($payload['CIDADE']) ? strtoupper($payload['CIDADE']) : false;
        $distanciaMaxima = isset($payload['DISTANCIA_MAXIMA']) ? $payload['DISTANCIA_MAXIMA'] : false;
        // id endereço cliente para buscar latitude e longitude para calcular a distancia 
        // SQL para buscar latitude e longitude do cliente        
        $idEnderecoCliente = $payload['ID_ENDERECO_CLIENTE'];
        $sqlEndCliente = "SELECT LATITUDE, LONGITUDE FROM CRM_ENDERECO_PESSOA WHERE ID=".$idEnderecoCliente;
        $responseEnderecoCliente = DB::select($sqlEndCliente);
        // testa Prestador endereço cliente
        if(!count($responseEnderecoCliente))
            return Handles::jsonResponse(false, 'Endereço do cliente não encontrado!', [], 404);
        
            // Select Locker por filtros
        $sqlLocker = "SELECT DISTINCT PS.ID, PS.NOME, PE.ID AS ID_END_LOCKER, ";
        $sqlLocker .= "PE.LONGITUDE, PE.LATITUDE, PE.ENDERECO, PE.NUMERO, PE.COMPLEMENTO, PE.BAIRRO, PE.CIDADE, PE.UF, PE.CEP ";
        $sqlLocker .= "FROM admser_cleaner.CRM_PESSOA PS ";
        $sqlLocker .= "INNER JOIN CRM_PESSOA_PRESTADOR PP ON (PP.ID_PESSOA=PS.ID) ";
        $sqlLocker .= "INNER JOIN CRM_ENDERECO_PESSOA PE ON (PE.ID_PESSOA=PS.ID) ";
        $sqlLocker .= "WHERE PS.TIPOPESSOA='W' AND PP.PAUSADO = FALSE ";
        $sqlLocker .= (($cidade <> false) ? "AND PE.CIDADE='$cidade' " : null);
        
        $responseLocker = DB::select($sqlLocker);
        if(!count($responseLocker))
            return Handles::jsonResponse(false, 'Nenhum LOCKER encontrado!', [], 404);

        // calcula a distancia entre cliente e locker
        $count=count($responseLocker);
        for($i=0; $i < $count; $i++){
            $distancia = 
                $this->calculaDistanciaAPartirDasLatitudesLongitudes(
                    $responseEnderecoCliente[0]->LATITUDE, 
                    $responseEnderecoCliente[0]->LONGITUDE, 
                    $responseLocker[$i]->LATITUDE, 
                    $responseLocker[$i]->LONGITUDE);
            // testa distancia solicitado 
            if ($distancia <= $distanciaMaxima){
                $responseLocker[$i]->DISTANCIAKM =  $distancia;
            }
            else{
                unset($responseLocker[$i]);}
        }
        return $responseLocker;
    }



    //////////////////////////////////////
    public function getPrestadoresFiltro(Request $request){
        $payload = $request->all();
        $listaDeIdRoupas = [];
        for($i=0; $i<count($payload['ROUPAS']); $i++){
            $listaDeIdRoupas[$i] = $payload['ROUPAS'][$i]['ID_ROUPA'];           
        }
        $idRoupasStringged = '('. implode(',',$listaDeIdRoupas).')'; //Ex. (1,2,33,44)
        $roupa = new RoupaController();
        $roupaValores = $roupa->somaValoresRoupas($payload['ROUPAS'],$listaDeIdRoupas);
        if(!$roupaValores->getData()->success)
            return $roupaValores; //é um Handles
        $roupaValores = $roupaValores->getData();
        $valorTotalRoupasLavar = $roupaValores->data->valorTotalRoupasLavar;
        $valorTotalRoupasPassar = $roupaValores->data->valorTotalRoupasPassar;
        $valorTotalRoupasLavarEPassar = $roupaValores->data->valorTotalRoupasLavarEPassar;
        $idEnderecoCliente = $payload['ID_ENDERECO_CLIENTE'];
        $entregaClienteBuscaPedido = isset($payload['ENTREGA_CLIENTE_BUSCA_PEDIDO']) ? $payload['ENTREGA_CLIENTE_BUSCA_PEDIDO'] : false;
        $entregaDomicilio = isset($payload['ENTREGA_DOMICILIO']) ? $payload['ENTREGA_DOMICILIO'] : false;
        $entregaArmario = isset($payload['ENTREGA_ARMARIO']) ? $payload['ENTREGA_ARMARIO'] : false;
        $lavar = isset($payload['LAVAR']) ? $payload['LAVAR'] : false;
        $passar = isset($payload['PASSAR']) ? $payload['PASSAR'] : false;
        $distanciaMaxima = isset($payload['DISTANCIA_MAXIMA']) ? $payload['DISTANCIA_MAXIMA'] : false;
        $tempoMaximoEntregaHoras = isset($payload['TEMPO_MAXIMO_ENTREGA_HORAS']) ? $payload['TEMPO_MAXIMO_ENTREGA_HORAS'] : false;
        if($lavar && $passar){//tabela FAT_TIPOSERVICO->ID (TABELA ESTÁTICA)
            $tipoServicoRoupas = 6;
        }else if($passar){
            $tipoServicoRoupas = 2;
        }else{
            $lavar = true;
            $tipoServicoRoupas = 1;
        }
        $sqlCli = "SELECT ID_PESSOA, LATITUDE, LONGITUDE FROM CRM_ENDERECO_PESSOA WHERE ID = {$idEnderecoCliente} LIMIT 1";
        $responseEnderecoCliente = DB::select($sqlCli);
        if(!count($responseEnderecoCliente))
            return Handles::jsonResponse(false, 'Endereço do cliente não encontrado!', [], 404);
        $idCliente = $responseEnderecoCliente[0]->ID_PESSOA;
        $sql = "SELECT DISTINCT P.ID as ID_PESSOA, EP.ID AS ID_ENDEREÇO, EP.CEP, EP.CIDADE,
                    EP.LATITUDE, EP.LONGITUDE, EP.ENDERECO, EP.NUMERO, EP.BAIRRO, EP.TELEFONE,
                    PP.CNPJ, PP.LOGO, PP.MAX_CLIENTES_DIA, PP.MAX_DISTANCIA_ATENDIMENTO, PP.NOMECONTATO, PP.RATING,
                    PP.VALOR_MINIMO_PEDIDO, PP.PAUSADO, PP.LAVA, PP.PASSA, FV.ID as ID_FAVORITO, IF(FV.ID,TRUE,FALSE) AS FAVORITO,
                    PP.ENTREGA_CLIENTE_BUSCA_PEDIDO, PP.ENTREGA_DOMICILIO, PP.ENTREGA_ARMARIO,
                    IF(PP.TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS,PP.TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS,0) AS TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS, 
                    IF(PP.TAXA_ENTREGA_DOMICILIO_REAIS,PP.TAXA_ENTREGA_DOMICILIO_REAIS,0) AS TAXA_ENTREGA_DOMICILIO_REAIS, 
                    IF(PP.TAXA_ENTREGA_ARMARIO_REAIS,PP.TAXA_ENTREGA_ARMARIO_REAIS,0) AS TAXA_ENTREGA_ARMARIO_REAIS
                FROM CRM_PESSOA P 
                LEFT JOIN CRM_ENDERECO_PESSOA EP ON EP.ID_PESSOA = P.ID 
                LEFT JOIN CRM_PESSOA_PRESTADOR PP ON PP.ID_PESSOA = P.ID 
                LEFT JOIN CRM_PRESTADOR_FAVORITO_CLIENTE FV 
                    ON P.ID = FV.ID_PESSOA_PRESTADOR
                    AND FV.ID_PESSOA_CLIENTE = {$idCliente}
                LEFT JOIN EST_ROUPA R ON R.ID IN {$idRoupasStringged} 
                LEFT JOIN EST_GRUPO G ON R.GRUPO = G.GRUPO 
                LEFT JOIN EST_GRUPO_PRESTADOR GP ON GP.ID_PESSOA_PESSOAPRESTADOR = P.ID AND G.GRUPO = GP.ID_GRUPO 
                WHERE P.TIPOPESSOA in ('P') AND PP.BLOQUEADO IS NOT TRUE AND IF(ISNULL(GP.ATIVO),TRUE,GP.ATIVO <> FALSE) 
                AND R.ID IN {$idRoupasStringged} AND EP.LATITUDE IS NOT NULL AND EP.LONGITUDE IS NOT NULL ";
                $lavar ? $sql .= "AND PP.LAVA = TRUE " : null;
                $passar ? $sql .= "AND PP.PASSA = TRUE " : null;
                $entregaClienteBuscaPedido ? $sql .= "AND PP.ENTREGA_CLIENTE_BUSCA_PEDIDO = TRUE " : null;
                $entregaDomicilio ? $sql .= "AND PP.ENTREGA_DOMICILIO = TRUE " : null;
                $entregaArmario ? $sql .= "AND PP.ENTREGA_ARMARIO = TRUE " : null;
            $sql .= "ORDER BY P.ID ASC";
        $response = DB::select($sql);

        if(!count($response))
            return Handles::jsonResponse(false, 'Nenhum prestador encontrado!', [], 404);
        for($i=0; $i < sizeof($response); $i++){
            $response[$i]->DISTANCIAKM = $this->calculaDistanciaAPartirDasLatitudesLongitudes($responseEnderecoCliente[0]->LATITUDE, $responseEnderecoCliente[0]->LONGITUDE, $response[$i]->LATITUDE, $response[$i]->LONGITUDE);
            if($response[$i]->DISTANCIAKM > 1000){
                array_splice($response, $i--, 1);
            }else if(isset($distanciaMaxima)){
                if($response[$i]->DISTANCIAKM > $distanciaMaxima){
                    array_splice($response, $i--, 1);
                }
            }
        }
        $prestador = new PrestadorController();  
        $pedido = new PedidoController();         
        for($i=0; $i < sizeof($response); $i++){
            $tempoMinimo = $prestador->getTempoMinimoPrestadorParaListaDeRoupasEspecificada($payload['ROUPAS'],$response[$i]->ID_PESSOA)->getData();
            if(!$tempoMinimo->success){
                array_splice($response, $i--, 1);
            }else if($tempoMaximoEntregaHoras){
                if($tempoMaximoEntregaHoras < $tempoMinimo->data->TEMPO_MINIMO){
                    array_splice($response, $i--, 1);
                }else{
                    $response[$i]->TEMPO_MINIMO_ENTREGA_HORAS = $tempoMinimo->data->TEMPO_MINIMO;
                    $response[$i]->PERCENTUAL_DESCONTO_ENTREGA = $tempoMinimo->data->DESCONTO;
                }
            }else{
                $response[$i]->TEMPO_MINIMO_ENTREGA_HORAS = $tempoMinimo->data->TEMPO_MINIMO;
                $response[$i]->PERCENTUAL_DESCONTO_ENTREGA = $tempoMinimo->data->DESCONTO;
            }
        }
        $sqlGrupoRoupas = " SELECT DISTINCT
                                R.GRUPO
                            FROM
                                EST_ROUPA R
                            WHERE
                                R.ID IN {$idRoupasStringged}";
        $responseGrupoRoupas = DB::select($sqlGrupoRoupas);
        if(count($responseGrupoRoupas)){
            for($i=0; $i < sizeof($response); $i++){
                $temposQueOPrestadorTrabalhaERespectivosDescontos = $prestador->showPrestadorGrupoTempos($response[$i]->ID_PESSOA, $responseGrupoRoupas)->getData();
                if(!$temposQueOPrestadorTrabalhaERespectivosDescontos->success){
                    array_splice($response, $i--, 1);
                }else{
                    $count=1;
                    $arrayTemposMinimosBuscaPedido = [];
                    $arrayTemposMinimosEntregaDomicilio = []; 
                    $arrayTemposMinimosEntregaArmario = [];
                    $arrayValores = [];
                    $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO = false;
                    $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO = false;
                    $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO = false;                 
                    foreach ($temposQueOPrestadorTrabalhaERespectivosDescontos->data as $tempoPrestador){     
                        if(!isset($tempoPrestador->DESCONTO)){
                            $tempoPrestador->DESCONTO = 0;
                        }                            
                        if($tempoPrestador->TEMPO_ENTREGA_HORAS>=$response[$i]->TEMPO_MINIMO_ENTREGA_HORAS){
                            $requestCalculo = new \Illuminate\Http\Request();
                            $requestCalculo->setMethod('POST');
                            $requestCalculo->request->add(['ID_PESSOA_PESSOAPRESTADOR' => $response[$i]->ID_PESSOA]);
                            $requestCalculo->request->add(['ID_TEMPO_ENTREGA' => $tempoPrestador->ID_TEMPO_ENTREGA]);
                            // $requestCalculo->request->add(['ID_CUPOM_PESSOA' = ]);
                            switch($tipoServicoRoupas){
                                case 1: //lavar
                                    $requestCalculo->request->add(['VALOR_TOTAL_ROUPAS' => $valorTotalRoupasLavar]);

                                    if($entregaClienteBuscaPedido){

                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 3]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_BALCAO'] = $valorTotal->data->total;                                            
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){
                                                $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO = $valorTotal->data->total;
                                                $arrayTemposMinimosBuscaPedido = [];
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){                                                
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){
                                                $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosBuscaPedido);
                                                $arrayTemposMinimosBuscaPedido = [];
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                    if($entregaDomicilio){
                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 4]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_DOMICILIO'] = $valorTotal->data->total;
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;                                            
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO = $valorTotal->data->total;
                                                $arrayTemposMinimosEntregaDomicilio = [];
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){                                                
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosEntregaDomicilio);
                                                $arrayTemposMinimosEntregaDomicilio = [];
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                    if($entregaArmario){
                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 5]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_ARMARIO'] = $valorTotal->data->total;
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO = $valorTotal->data->total;
                                                $arrayTemposMinimosEntregaArmario = [];
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){                                                
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosEntregaArmario);
                                                $arrayTemposMinimosEntregaArmario = [];
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                break;
                                case 2: //passar 
                                    $requestCalculo->request->add(['VALOR_TOTAL_ROUPAS' => $valorTotalRoupasPassar]);
                                    if($entregaClienteBuscaPedido){
                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 3]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();                                        
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_BALCAO'] = $valorTotal->data->total;
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){
                                                $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO = $valorTotal->data->total;
                                                $arrayTemposMinimosBuscaPedido = [];
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){                                                
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){
                                                $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosBuscaPedido);
                                                $arrayTemposMinimosBuscaPedido = [];
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                    if($entregaDomicilio){
                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 4]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_DOMICILIO'] = $valorTotal->data->total;
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO = $valorTotal->data->total;
                                                $arrayTemposMinimosEntregaDomicilio = [];
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){                                                
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosEntregaDomicilio);
                                                $arrayTemposMinimosEntregaDomicilio = [];
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                    if($entregaArmario){
                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 5]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_ARMARIO'] = $valorTotal->data->total;
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO = $valorTotal->data->total;
                                                $arrayTemposMinimosEntregaArmario = [];
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){                                                
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosEntregaArmario);
                                                $arrayTemposMinimosEntregaArmario = [];
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                break;
                                case 6: //lavar e passar                                 
                                    $requestCalculo->request->add(['VALOR_TOTAL_ROUPAS' => $valorTotalRoupasLavarEPassar]);

                                    if($entregaClienteBuscaPedido){
                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 3]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();                                        
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_BALCAO'] = $valorTotal->data->total;
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){
                                                $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO = $valorTotal->data->total;
                                                $arrayTemposMinimosBuscaPedido = [];
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){                                                
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO){
                                                $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosBuscaPedido);
                                                $arrayTemposMinimosBuscaPedido = [];
                                                array_push($arrayTemposMinimosBuscaPedido,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                    if($entregaDomicilio){
                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 4]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_DOMICILIO'] = $valorTotal->data->total;
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO = $valorTotal->data->total;
                                                $arrayTemposMinimosEntregaDomicilio = [];
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){                                                
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosEntregaDomicilio);
                                                $arrayTemposMinimosEntregaDomicilio = [];
                                                array_push($arrayTemposMinimosEntregaDomicilio,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                    if($entregaArmario){
                                        $requestCalculo->request->add(['ID_TIPOSERVICO_FRETE' => 5]);
                                        $valorTotal = $pedido->calculaTotalPedido($requestCalculo)->getData();
                                        if($valorTotal->success){
                                            $arrayValores[$count-1]['VALOR_TOTAL_ENTREGA_ARMARIO'] = $valorTotal->data->total;
                                            $arrayValores[$count-1]['DESCONTO'] = $tempoPrestador->DESCONTO;
                                            $arrayValores[$count-1]['TEMPO_ENTREGA_HORAS'] = $tempoPrestador->TEMPO_ENTREGA_HORAS;
                                            $arrayValores[$count-1]['ID_PRAZO'] = $tempoPrestador->ID_PRESTADOR_TEMPO_ENTREGA;
                                            if(!$VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO = $valorTotal->data->total;
                                                $arrayTemposMinimosEntregaArmario = [];
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total == $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){                                                
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }else if($valorTotal->data->total < $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO){
                                                $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO = $valorTotal->data->total;
                                                unset($arrayTemposMinimosEntregaArmario);
                                                $arrayTemposMinimosEntregaArmario = [];
                                                array_push($arrayTemposMinimosEntregaArmario,$tempoPrestador->TEMPO_ENTREGA_HORAS);
                                            }
                                        }
                                    }
                                break;
                            }
                            $count+=1;
                            unset($requestCalculo);
                        }
                    }
                    ($entregaClienteBuscaPedido && count($arrayTemposMinimosBuscaPedido)) ? $BUSCA_PEDIDO = array('VALOR_TOTAL_MINIMO' => $VALOR_TOTAL_MINIMO_BUSCA_PEDIDO,
                        'TEMPO_MINIMO_NESSE_VALOR' => min($arrayTemposMinimosBuscaPedido)) : $BUSCA_PEDIDO = [];
                    ($entregaDomicilio && count($arrayTemposMinimosEntregaDomicilio)) ? $ENTREGA_DOMICILIO = array('VALOR_TOTAL_MINIMO' => $VALOR_TOTAL_MINIMO_ENTREGA_DOMICILIO,
                        'TEMPO_MINIMO_NESSE_VALOR' => min($arrayTemposMinimosEntregaDomicilio)) : $ENTREGA_DOMICILIO = [];
                    ($entregaArmario && count($arrayTemposMinimosEntregaArmario)) ? $ENTREGA_ARMARIO = array('VALOR_TOTAL_MINIMO' => $VALOR_TOTAL_MINIMO_ENTREGA_ARMARIO,
                        'TEMPO_MINIMO_NESSE_VALOR' => min($arrayTemposMinimosEntregaArmario)) : $ENTREGA_ARMARIO = [];
                    $menorDiferencaTempo = false;
                    $indexDoTempoIdeal = false;
                    // var_dump($response[$i]->ID_PESSOA);
                    // var_dump($arrayValores);
                    foreach($arrayValores as $key=>$valor){
                        $tempoMaximoEntregaHoras ? $diff = ($tempoMaximoEntregaHoras - $valor['TEMPO_ENTREGA_HORAS']) 
                        : $diff = $valor['TEMPO_ENTREGA_HORAS'];
                        if($menorDiferencaTempo === false){
                            $menorDiferencaTempo = $diff >= 0 ? $diff : -$diff;
                            $indexDoTempoIdeal = $key;
                        }else if($diff >= 0 && $diff < $menorDiferencaTempo){
                            $menorDiferencaTempo = $diff;
                            $indexDoTempoIdeal = $key;
                        }
                    }
                    $response[$i]->POSSIBILIDADES = $arrayValores;
                    $indexDoTempoIdeal != false ? $response[$i]->ORCAMENTO_IDEAL = $arrayValores[$indexDoTempoIdeal] : $response[$i]->ORCAMENTO_IDEAL = $arrayValores[0];
                    $response[$i]->VALORES_MINIMOS = array(
                        'BUSCA_PEDIDO' => $BUSCA_PEDIDO,
                        'ENTREGA_DOMICILIO' => $ENTREGA_DOMICILIO,
                        'ENTREGA_ARMARIO' => $ENTREGA_ARMARIO);
                } 
            }
        }
        return count($response) ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            :   Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function calculaDistanciaAPartirDasLatitudesLongitudes($lat_inicial, $long_inicial, $lat_final, $long_final){
        $d2r = 0.017453292519943295769236;
        $dlong = ($long_final - $long_inicial) * $d2r;
        $dlat = ($lat_final - $lat_inicial) * $d2r;
        $temp_sin = sin($dlat/2.0);
        $temp_cos = cos($lat_inicial * $d2r);
        $temp_sin2 = sin($dlong/2.0);
        $a = ($temp_sin * $temp_sin) + ($temp_cos * $temp_cos) * ($temp_sin2 * $temp_sin2);
        $c = 2.0 * atan2(sqrt($a), sqrt(1.0 - $a));
        return round((6368.1 * $c),2);
    }

    public function getEnderecosPessoa($id){
        $sql = "SELECT * FROM CRM_ENDERECO_PESSOA WHERE ID_PESSOA = {$id} ORDER BY FAVORITO DESC";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }    

    public function getCupomCliente($id){
        $sql = "SELECT 
                    CP.ID AS ID_CUPOM_PESSOA,
                    IF(CP.UTILIZADO <> FALSE, TRUE, FALSE) AS UTILIZADO,
                    C.DESCRICAO,
                    C.DESCONTO,
                    IF(C.PERCENTUAL_OU_REAL = 'R', 'R', 'P') AS PERCENTUAL_OU_REAL,
                    C.VALIDADEFIM,
                    IF(NOW() > VALIDADEFIM, FALSE, TRUE) AS VALIDO
                FROM FAT_CUPOM_PESSOA CP
                LEFT JOIN FAT_CUPOM C 
                    ON CP.ID_CUPOM = C.ID
                WHERE CP.ID_PESSOA = {$id}";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }   

    function urlExists($url=NULL){  
        if($url == NULL) return false;  
        $ch = curl_init($url);  
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);  
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
        $data = curl_exec($ch);  
        $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
        curl_close($ch);  
        if($httpcode>=200 && $httpcode<300){
            return true;
        } else {  
            return false;  
        }  
    }  
    
    public function getFeedCliente($id){
        $sql = "SELECT 
                    F.ID AS ID_FEED,
                    M.ID AS ID_MENSAGEM,
                    M.MENSAGEM,
                    M.LINK,
                    F.ID_PESSOA_DESTINATARIA,
                    F.DATA_ENVIO,
                    F.CREATED_AT AS DATA_CRIACAO,
                    F.UPDATED_AT AS DATA_EDICAO
                FROM FAT_FEED F
                LEFT JOIN FAT_MENSAGEM M ON F.ID_MENSAGEM = M.ID
                LEFT JOIN FAT_TIPO_MENSAGEM TM ON M.ID_TIPO_MENSAGEM = TM.ID
                WHERE F.ID_PESSOA_DESTINATARIA = {$id}
                    AND M.MENSAGEM IS NOT NULL
                    AND TM.DESCRICAO = 'FEED'";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
            : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }
}
