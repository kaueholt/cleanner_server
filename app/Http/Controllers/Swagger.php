<?php

namespace App\Http\Controllers;

/*------------------------------------------------------------------------------------------------------------------------------*/

     /**
     * @OA\Info(
     *      version="1.0.0",
     *      title="Cleaner API",
     *      description="Cleaner Swagger Api - By 33 Robotics",
     *      @OA\Contact(
     *          email="marcio.sergio@admsistema.com.br"
     *      ),
     *     @OA\License(
     *         name="Apache 2.0",
     *         url="http://www.apache.org/licenses/LICENSE-2.0.html"
     *     )
     * )
     */
      
     /**
     * @OA\Delete(
     *     path="/api/v1/default/{table}/{ID}",
     *     summary="Deleta o registro da tabela {table} com o ID {ID}",
     *     operationId="deletePet",
     *     tags={"Default"},
     *     @OA\Parameter(
     *         name="table",
     *         in="path",
     *         description="Classes php referentes às tabelas do BD ==> armariogaveta, atividade, chat, classe, cupom, cupompessoa, ddm, enderecopessoa, feed, form, grupo, grupoprestador, historicopedido, imagem, mensagem, pagamentoiugu, pedido, pedidoitem, pedidolocker, pessoa, pessoaprestador, prestadorfavoritocliente, prestadortempoentrega, roupa, roupasprestador, status, tempoentrega, tipomensagem, tiposervico",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="cupompessoa"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID a ser deletado",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=42
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Registro excluído com sucesso."
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Registro {ID} não encontrado",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Registro não excluído - Erro de validação",
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/default/{table}",
     *     summary="Retorna TODOS os registros da tabela {table}",
     *     tags={"Default"},
     *     description="Retorna TODOS os registros da tabela {table}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="table",
     *         in="path",
     *         description="Classes php referentes às tabelas do BD ==> armariogaveta, atividade, chat, classe, cupom, cupompessoa, ddm, enderecopessoa, feed, form, grupo, historicopedido, imagem, mensagem, pagamentoiugu, pedido, pedidoitem, pedidolocker, pessoa, pessoaprestador, prestadorfavoritocliente, prestadortempoentrega, roupa, roupasprestador, status, tempoentrega, tipomensagem, tiposervico",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="pessoas"
     *         )
     *     ),
     * )
    */
    
     /**
     * @OA\Get(
     *     path="/api/v1/default/{table}/{ID}",
     *     summary="Retorna o registro com id {ID} da tabela {table}",
     *     tags={"Default"},
     *     description="Retorna o registro especificado por {ID}, na tabela {table}. Para dados detalhados do PEDIDO, utilize a rota /api/v1/pedido/{ID}.",
     *     @OA\Response(
     *          response=200,
     *          description="Registro encontrado!",
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Registro não encontrado!",
     *     ),
     *     @OA\Parameter(
     *         name="table",
     *         in="path",
     *         description="Classes php referentes às tabelas do BD ==> armariogaveta, atividade, chat, classe, cupom, cupompessoa, ddm, enderecopessoa, feed, form, grupo, historicopedido, imagem, mensagem, pagamentoiugu, pedido, pedidoitem, pedidolocker, pessoa, pessoaprestador, prestadortempoentrega, roupa, roupasprestador, status, tempoentrega, tipomensagem, tiposervico",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="cupom"
     *         )
     *     ),      
     *     @OA\Parameter(
     *         name="ID",
     *         description="ID a ser buscado na tabela {table}. Geralmente é o próprio campo ID, menos para as tabelas ATIVIDADE e GRUPO (cujos ID's se chamam ATIVIDADE varchar(4), e GRUPO varchar(15))",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="1"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/armariogaveta",
     *     summary="Adiciona uma nova linha à tabela armario_gaveta",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da armario_gaveta",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 60 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DISPONIVEL",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO",
     *         description="tamanho: 70 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="SENHA",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/armariogaveta/{ID}",
     *     summary="Edita a linha especificada da tabela armario_gaveta",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da armario_gaveta",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 60 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DISPONIVEL",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO",
     *         description="tamanho: 70 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="SENHA",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/atividade",
     *     summary="Adiciona uma nova linha à tabela atividade",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da atividade",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ATIVIDADE",
     *         description="tamanho: 4 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 30 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TIPO",
     *         description="tamanho: 1 caracter",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USERINSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USERUPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     )
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/atividade/{ATIVIDADE}",
     *     summary="Edita a linha especificada da tabela atividade",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da atividade",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ATIVIDADE",
     *         description="tamanho: 4 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 30 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TIPO",
     *         description="tamanho: 1 caracter",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USERINSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USERUPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     )
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/chat",
     *     summary="Adiciona uma nova linha à tabela chat",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da chat",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_PESSOA_DESTINATARIA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *     @OA\Parameter(
     *         name="DATA_ENVIO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),

     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/chat/{ID}",
     *     summary="Edita a linha especificada da tabela chat",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da chat",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_PESSOA_DESTINATARIA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *     @OA\Parameter(
     *         name="DATA_ENVIO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),

     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/classe",
     *     summary="Adiciona uma nova linha à tabela classe",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da classe",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         description="tamanho: 2 caracteres",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="BLOQUEADO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=0
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/classe/{ID}",
     *     summary="Edita a linha especificada da tabela classe",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da classe",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         description="tamanho: 2 caracteres",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="BLOQUEADO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=0
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *)
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/cupom",
     *     summary="Adiciona uma nova linha à tabela cupom",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da cupom",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCONTO",
     *         in="query",
     *         description="decimal(10,2)",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERCENTUAL_OU_REAL",
     *         description="tamanho: 1 caracter, 'P (ercentual), R (eal)'",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="R"
     *         )
     *    ),
     *     @OA\Parameter(
     *         name="VALIDADEINICIO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="VALIDADEFIM",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_CREATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/cupom/{ID}",
     *     summary="Edita a linha especificada da tabela cupom",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da cupom",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCONTO",
     *         in="query",
     *         description="decimal(10,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERCENTUAL_OU_REAL",
     *         description="tamanho: 1 caracter, 'P (ercentual), R (eal)'",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="R"
     *         )
     * ),
     *     @OA\Parameter(
     *         name="VALIDADEINICIO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="VALIDADEFIM",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_CREATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/cupompessoa",
     *     summary="Adiciona uma nova linha à tabela cupom_pessoa",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da cupom_pessoa",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_CUPOM",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="UTILIZADO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/cupompessoa/{ID}",
     *     summary="Edita a linha especificada da tabela cupom_pessoa",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da cupom_pessoa",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_CUPOM",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="UTILIZADO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/ddm",
     *     summary="Adiciona uma nova linha à tabela ddm",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da ddm",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ALIAS",
     *         description="tamanho: 25 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NRCAMPO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CAMPO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TIPO",
     *         description="tamanho: 4 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FORMATO",
     *         description="tamanho: 8 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="REQUERIDO",
     *         description="tamanho: 1 caracter",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PADRAO",
     *         description="tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="LEGENDA",
     *         description="tamanho: 25 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MASCARA",
     *         description="tamanho: 25 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOMEEXTERNO",
     *         description="tamanho: 40 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ALTERACAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2020-12-31"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/ddm/{ID}",
     *     summary="Edita a linha especificada da tabela ddm",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da ddm",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ALIAS",
     *         description="tamanho: 25 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NRCAMPO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CAMPO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TIPO",
     *         description="tamanho: 4 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FORMATO",
     *         description="tamanho: 8 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="REQUERIDO",
     *         description="tamanho: 1 caracter",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PADRAO",
     *         description="tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="LEGENDA",
     *         description="tamanho: 25 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MASCARA",
     *         description="tamanho: 25 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOMEEXTERNO",
     *         description="tamanho: 40 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ALTERACAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2020-12-31"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/enderecopessoa",
     *     summary="Adiciona uma nova linha à tabela endereco_pessoa",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da endereco_pessoa",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FAVORITO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ENDERECO",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NUMERO",
     *         description="tamanho: 20 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="COMPLEMENTO",
     *         description="tamanho: 30 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="BAIRRO",
     *         description="tamanho: 80 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CEP",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="LATITUDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=-49.2076890
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="LONGITUDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=-25.5388740
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CIDADE",
     *         description="tamanho: 80 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="UF",
     *         description="tamanho: 2 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TELEFONE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/enderecopessoa/{ID}",
     *     summary="Edita a linha especificada da tabela endereco_pessoa",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da endereco_pessoa",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FAVORITO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ENDERECO",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NUMERO",
     *         description="tamanho: 20 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="COMPLEMENTO",
     *         description="tamanho: 30 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="BAIRRO",
     *         description="tamanho: 80 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CEP",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="LATITUDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=-49.2076890
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="LONGITUDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=-25.5388740
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CIDADE",
     *         description="tamanho: 80 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="UF",
     *         description="tamanho: 2 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TELEFONE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/feed",
     *     summary="Adiciona uma nova linha à tabela feed",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da feed",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_MENSAGEM",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_PESSOA_DESTINATARIA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/feed/{ID}",
     *     summary="Edita a linha especificada da tabela feed",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da feed",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_MENSAGEM",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_PESSOA_DESTINATARIA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/form",
     *     summary="Adiciona uma nova linha à tabela form",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da form",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="NOMEFORM",
     *         description="tamanho: 30 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/form/{ID}",
     *     summary="Edita a linha especificada da tabela form",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da form",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOMEFORM",
     *         description="tamanho: 30 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/grupo",
     *     summary="Adiciona uma nova linha à tabela grupo",
     *     description="Adiciona grupo",
     *     tags={"Default"},
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos "
     *     ),
     *     @OA\Response(
     *         response=421,
     *         description="Registro não inserido - Chave existente"
     *     ),
     *     @OA\Parameter(
     *         name="GRUPO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="AC"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TIPO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="V"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NIVEL",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="COMISSAO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=15.5
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/grupo/{GRUPO}",
     *     summary="Edita a linha especificada da tabela grupo",
     *     description="Adiciona grupo",
     *     tags={"Default"},
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos "
     *     ),
     *     @OA\Response(
     *         response=421,
     *         description="Registro não inserido - Chave existente"
     *     ),
     *     @OA\Parameter(
     *         name="GRUPO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="AC"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TIPO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="V"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NIVEL",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="COMISSAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=15.5
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/grupoprestador",
     *     summary="Adiciona uma nova linha à tabela grupoprestador",
     *     description="Adiciona uma configuração pessoal do prestador em relação a um grupo",
     *     tags={"Default"},
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido"
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos "
     *     ),
     *     @OA\Response(
     *         response=421,
     *         description="Registro não inserido - Chave existente"
     *     ),
     *     @OA\Parameter(
     *         name="LISTADEGRUPOPRESTADOR",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="object",
     *             example="[{'ID_GRUPO':'GG', 'ID_TEMPO_ENTREGA_MINIMO':1, 'ID_PESSOA_PESSOAPRESTADOR':999, 'ATIVO':1},{'ID_GRUPO':'GP01', 'ID_TEMPO_ENTREGA_MINIMO':1, 'ID_PESSOA_PESSOAPRESTADOR':999, 'ATIVO':1}]"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/grupoprestador/{ID}",
     *     summary="Edita a linha {ID} da tabela grupoprestador",
     *     description="Edita a linha {ID} da tabela grupoprestador",
     *     tags={"Default"},
     *     @OA\Response(
     *         response=200,
     *         description="Registro atualizado!"
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="LISTADEGRUPOPRESTADOR",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="object",
     *             example="[{'ID_GRUPO':'GG', 'ID_TEMPO_ENTREGA_MINIMO':1, 'ID_PESSOA_PESSOAPRESTADOR':999, 'ATIVO':1},{'ID_GRUPO':'GP01', 'ID_TEMPO_ENTREGA_MINIMO':1, 'ID_PESSOA_PESSOAPRESTADOR':999, 'ATIVO':1}]"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/historicopedido",
     *     summary="Adiciona uma nova linha à tabela historico_pedido",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da historico_pedido",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_STATUS_ANTERIOR",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/historicopedido/{ID}",
     *     summary="Edita a linha especificada da tabela historico_pedido",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da historico_pedido",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_STATUS_ANTERIOR",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/imagem",
     *     summary="Adiciona uma nova linha à tabela imagem",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da imagem",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_DOC",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MODULO",
     *         description="tamanho: 3 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESTAQUE",
     *         description="tamanho: 1 caracter",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/imagem/{ID}",
     *     summary="Edita a linha especificada da tabela imagem",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da imagem",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_DOC",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MODULO",
     *         description="tamanho: 3 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESTAQUE",
     *         description="tamanho: 1 caracter",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/mensagem",
     *     summary="Adiciona uma nova linha à tabela mensagem",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da mensagem",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_TIPO_MENSAGEM",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MENSAGEM",
     *         description="text",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="LINK",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/mensagem/{ID}",
     *     summary="Edita a linha especificada da tabela mensagem",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da mensagem",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_TIPO_MENSAGEM",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MENSAGEM",
     *         description="text",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="LINK",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
     
     /**
     * @OA\Post(
     *     path="/api/v1/default/pagamentoiugu",
     *     summary="Adiciona uma nova linha à tabela pagamentoiugu",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pagamentoiugu",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOME_OPERADORA",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TOKEN_VALIDACAO",
     *         description="tamanho: 150 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NUMERO_CONTA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Put(
     *     path="/api/v1/default/pagamentoiugu/{ID}",
     *     summary="Edita a linha especificada da tabela pagamentoiugu",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pagamentoiugu",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOME_OPERADORA",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TOKEN_VALIDACAO",
     *         description="tamanho: 150 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NUMERO_CONTA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
     
     // 
     //   OA\Post(
          // path="/api/v1/default/parametro",
          // summary="Adiciona uma nova linha à tabela parametro",
          // tags={"Default"},
          // description="Os parâmetros devem ser os mesmos da parametro",
          //  OA\Response(
          //     response=200,
          //     description="Registro inserido",
          // ),
          //  OA\Response(
          //     response=422,
          //     description="Dados inválidos, verifique os campos.",
          // ),
          //  OA\Parameter(
          //     name="API_GOOGLE_KEY",
          //     in="query",
          //     description="tamanho: 70 caracteres",
          //     required=true,
          //      OA\Schema(
               //    type="string",
               //    example=1
          //     )
          // ),
     //  )
     // 

     /**
     * @OA\Put(
     *     path="/api/v1/default/parametro/{ID}",
     *     summary="Edita a linha especificada da tabela parametro",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da parametro",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="API_GOOGLE_KEY",
     *         in="path",
     *         description="tamanho: 70 caracteres",
     *         required=false,
     *         @OA\Schema(
     *             type="varchar",
     *             example=70
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Post(
     *     path="/api/v1/default/pedido",
     *     summary="Adiciona uma nova linha à tabela pedido, e N novas linhas à tabela pedidoItem, recebidos como array de objetos. ( PEDIDOITEM: [{}]  )",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pedido, somados com um array de itens do pedido, no formato (exemplo) 'PEDIDOITEM': [{'ID_ROUPA':1, 'QUANTIDADE':1},{'ID_ROUPA':2, 'QUANTIDADE':2}, ...]",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="TESTE",
     *         description="0 ou 1. Se true(1), irá apenas simular a gravação do pedido.",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_CLIENTE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_COMISSAO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_CUPOM_PESSOA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_ENDERECO_PESSOA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=18
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_GAVETA_ARMARIO_ENTREGA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_GAVETA_ARMARIO_RETIRADA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_PRAZO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_PESSOA_PESSOAPRESTADOR",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_STATUS_SERVICO_ROUPAS",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_STATUS_SERVICO_FRETE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=11
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_TIPO_SERVICO_ROUPAS",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_TIPO_SERVICO_FRETE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *     @OA\Parameter(
     *         name="COR_HEXADECIMAL",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="0xFFFFFF"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="COR_DESCRICAO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="preto"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PEDIDOITEM",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="object",
     *             example="{""ROUPAS"":{""ID_ROUPA"":4, ""QUANTIDADE"":1}}"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PAGAMENTO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="object",
     *             example="{""PAGAMENTO"":{""api_token"": ""baaa1b0fe1f3936b273da3f0179c7a03"",""token"": ""dbc01dfa-23db-49fc-b0a7-72d6ceb89c4c"",""customer_id"": ""5FD4C941E24A4BF595C2BFEE7745E8D2"",""invoice_id"": ""DE6F154797AC4FC5AE2359BB4A100324"",""email"":  ""teste@teste.com"",""discount_cents"": 0,""payer"": {""cpf_cnpj"": ""08581026923"",""name"": ""teste"",""phone_prefix"": ""41"",""phone"": ""988888888"",""email"": ""t@teste.com"",""address"": {""street"": ""Rua Monteiro Lobato"",""number"": ""82"",""district"": ""Colônia Rio Grande"",""city"": ""São José dos Pinhais"",""state"": ""PR"",""zip_code"": 82120340,""complement"": ""casa""}}}}"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INVOICE_IUGU",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="123123123"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATAFECHAMENTO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="LINKQRCODE",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PONTO_REFERENCIA_ENTREGA",
     *         description="tamanho: 60 caracteres, ponto de referência para entrega",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TOTAL",
     *         description="decimal(9,2)",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TAXA_ENTREGA_FRETE_REAIS",
     *         description="decimal(9,2)",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERCENT_DESCONTO_PRESTADOR_TEMPO_ENTREGA",
     *         description="decimal(9,2)",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATA_ENTREGA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATA_RETIRADA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/pedido/{ID}",
     *     summary="Edita a linha especificada da tabela pedido",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pedido",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_CLIENTE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_COMISSAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_CUPOM_PESSOA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_ENDERECO_PESSOA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_GAVETA_ARMARIO_ENTREGA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_GAVETA_ARMARIO_RETIRADA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_PRAZO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_PESSOA_PESSOAPRESTADOR",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_STATUS_SERVICO_ROUPAS",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=11
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_STATUS_SERVICO_FRETE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=11
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_TIPO_SERVICO_ROUPAS",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_TIPO_SERVICO_FRETE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="COR_HEXADECIMAL",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="0xFFFFFF"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="COR_DESCRICAO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="preto"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PEDIDOITEM",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="object",
     *             example="[{'ID_ROUPA':1, 'QUANTIDADE':1},{'ID_ROUPA':2, 'QUANTIDADE':2}]"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INVOICE_IUGU",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="123123123"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATAFECHAMENTO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="LINKQRCODE",
     *         description="tamanho: 200 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PONTO_REFERENCIA_ENTREGA",
     *         description="tamanho: 60 caracteres, ponto de referência para entrega",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TOTAL",
     *         description="decimal(9,2)",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATA_ENTREGA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATA_RETIRADA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Post(
     *     path="/api/v1/default/pedidoavaliacao",
     *     summary="Adiciona uma nova linha à tabela pedidoavaliacao",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pedidoavaliacao",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA1",
     *         description="Tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="pergunta 1?"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA2",
     *         description="Tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="pergunta 2?"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA3",
     *         description="Tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="pergunta 3?"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA1",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA2",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=5
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA3",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=3
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MEDIA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=3.0
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO",
     *         description="Tamanho: 300 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="Observacoes"
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Put(
     *     path="/api/v1/default/pedidoavaliacao/{ID}",
     *     summary="Altera uma linha específica databela pedidoavaliacao",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pedidoavaliacao",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA1",
     *         description="Tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="pergunta 1?"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA2",
     *         description="Tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="pergunta 2?"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PERGUNTA3",
     *         description="Tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="pergunta 3?"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA1",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA2",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=5
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RESPOSTA3",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=3
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MEDIA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=3.0
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO",
     *         description="Tamanho: 300 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="Observacoes"
     *         )
     *     ),
     * )
     */

          
     /**
     * @OA\Post(
     *     path="/api/v1/default/pedidoitem",
     *     summary="Adiciona uma nova linha à tabela pedido_item",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pedido_item",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_ROUPA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="STATUS",
     *         in="query",
     *         description="default is 0",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="QUANTIDADE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="VALOR_UNITARIO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=1.0
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="VALOR_TOTAL",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=2.0
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="COMISSAO",
     *        description="decimal(5,2)",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=15.5
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO_CANCELAMENTO",
     *         in="query",
     *         description="text",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO_CLIENTE",
     *         in="query",
     *         description="text",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO_LAVANDERIA",
     *         in="query",
     *         description="text",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOVA_OBSERVACAO_CLIENTE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOVA_OBSERVACAO_LAVANDERIA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/pedidoitem/{ID}",
     *     summary="Edita a linha especificada da tabela pedido_item",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pedido_item",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="ID_ROUPA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="STATUS",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="QUANTIDADE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="VALOR_UNITARIO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.0
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="VALOR_TOTAL",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=2.0
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="COMISSAO",
     *         description="decimal(5,2)",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=15.5
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO_CANCELAMENTO",
     *         in="query",
     *         description="text",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO_CLIENTE",
     *         in="query",
     *         description="text",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO_LAVANDERIA",
     *         in="query",
     *         description="text",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOVA_OBSERVACAO_CLIENTE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOVA_OBSERVACAO_LAVANDERIA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Post(
     *     path="/api/v1/default/pedidolocker",
     *     summary="Adiciona uma nova linha à tabela pedido_locker",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pedido_locker",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PEDIDO",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="LOCKER_NAME",
     *         description="varchar 80",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="SHOPPING CURITIBA"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="IDENTIFICACAO_GAVETA",
     *         description="varchar 80",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="GAVETA 1"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="STATUS",
     *         description="varchar 1",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="RESERVATION_CODE",
     *         description="varchar 80",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="F12E918E-4754-43B7-A44C-9F12EC992C88"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="DESCRIPTION",
     *         description="varchar 180",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="F12E918E-4754-43B7-A44C-9F12EC992C88"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="KEY_DELIVERER",
     *         description="varchar 20",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="F12E918E-4754-43B7-A44C-9F12EC992C88"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="KEY_RECEIVER",
     *         description="varchar 20",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="F12E918E-4754-43B7-A44C-9F12EC992C88"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="DATE_RESERVE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="01-01-2001"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/pedidolocker/{ID}",
     *     summary="Edita a linha especificada da tabela pedido_locker",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pedido_locker",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="LOCKER_NAME",
     *         description="varchar 80",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="SHOPPING CURITIBA"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="IDENTIFICACAO_GAVETA",
     *         description="varchar 80",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="GAVETA 1"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="STATUS",
     *         description="varchar 1",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="RESERVATION_CODE",
     *         description="varchar 80",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="F12E918E-4754-43B7-A44C-9F12EC992C88"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="DESCRIPTION",
     *         description="varchar 180",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="F12E918E-4754-43B7-A44C-9F12EC992C88"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="KEY_DELIVERER",
     *         description="varchar 20",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="F12E918E-4754-43B7-A44C-9F12EC992C88"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="KEY_RECEIVER",
     *         description="varchar 20",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="F12E918E-4754-43B7-A44C-9F12EC992C88"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="DATE_RESERVE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="01-01-2001"
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *      @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/pessoa",
     *     summary="Adiciona uma nova linha à tabela pessoa",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pessoa",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="CUSTOMER_INVOICE_IUGU",
     *         description="tamanho: 150 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CLASSE",
     *         description="tamanho: 2 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ATIVIDADE",
     *         description="tamanho: 4 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="EMAIL",
     *         description="tamanho: 60 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="SENHA",
     *         description="tamanho: 30 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOME",
     *         description="tamanho: 75 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOMEREDUZIDO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIREBASE_USER",
     *         description="tamanho: 100 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="Aasd1231dasd1ASad2311"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TIPOPESSOA",
     *         description="tamanho: 1 caractere -> (C - CLIENTE, P - PRESTADOR, A- ADMIN, W-Wordrobe(Armário,Locker))",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="C"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CPF",
     *         description="tamanho: 14 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INSCESTRG",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INSCMUNICIPAL",
     *         description="tamanho: 14 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CELULAR",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TELEFONE_FIXO",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="DATA_NASCIMENTO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2020-12-31"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="SEXO",
     *         description="tamanho: 1 caracter",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="UID",
     *         description="tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/pessoa/{ID}",
     *     summary="Edita a linha especificada da tabela pessoa",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pessoa",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CUSTOMER_INVOICE_IUGU",
     *         description="tamanho: 150 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CLASSE",
     *         description="tamanho: 2 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ATIVIDADE",
     *         description="tamanho: 4 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="EMAIL",
     *         description="tamanho: 60 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="SENHA",
     *         description="tamanho: 30 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOME",
     *         description="tamanho: 75 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOMEREDUZIDO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIREBASE_USER",
     *         description="tamanho: 100 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="Aasd1231dasd1ASad2311"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TIPOPESSOA",
     *         description="tamanho: 1 caractere -> (C - CLIENTE, P - PRESTADOR, A- ADMIN, W-Wordrobe(Armário,Locker))",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="C"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CPF",
     *         description="tamanho: 14 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INSCESTRG",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INSCMUNICIPAL",
     *         description="tamanho: 14 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CELULAR",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TELEFONE_FIXO",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="DATA_NASCIMENTO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2020-12-31"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="SEXO",
     *         description="tamanho: 1 caracter",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="UID",
     *         description="tamanho: 255 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/pessoaprestador",
     *     summary="Adiciona uma nova linha à tabela pessoa_prestador",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pessoa_prestador",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CNPJ",
     *         description="tamanho: 18 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MAX_CLIENTES_DIA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOMECONTATO",
     *         description="tamanho: 65 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="RATING",
     *         in="query",
     *         description="decimal(5,2) rating(avaliação) de preço - cálcular a média dos preços do fornecedor para ter um rating ($ || $$ || $$$ || $$$$ || $$$$$)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="VALOR_MINIMO_PEDIDO",
     *         in="query",
     *         description="decimal(5,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CODIGO_BANCO",
     *         description="tamanho: 3 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="AGENCIA_BANCO",
     *         description="tamanho: 6 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CONTA_BANCO",
     *         description="tamanho: 12 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIOSABADOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMSABADOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIOSABADOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMSABADOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIODOMINGOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMDOMINGOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIODOMINGOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMDOMINGOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DIASEMANA",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="BLOQUEADO",
     *         description="admin bloqueou este prestador",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PAUSADO",
     *         description="prestador pausou seus serviços",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/pessoaprestador/{ID}",
     *     summary="Edita a linha especificada da tabela pessoa_prestador",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da pessoa_prestador",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CNPJ",
     *         description="tamanho: 18 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MAX_CLIENTES_DIA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="NOMECONTATO",
     *         description="tamanho: 65 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *    @OA\Parameter(
     *         name="RATING",
     *         in="query",
     *         description="decimal(5,2) rating(avaliação) de preço - cálcular a média dos preços do fornecedor para ter um rating ($ || $$ || $$$ || $$$$ || $$$$$)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="VALOR_MINIMO_PEDIDO",
     *         in="query",
     *         description="decimal(5,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CODIGO_BANCO",
     *         description="tamanho: 3 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="AGENCIA_BANCO",
     *         description="tamanho: 6 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="CONTA_BANCO",
     *         description="tamanho: 12 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIOSABADOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMSABADOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIOSABADOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMSABADOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIODOMINGOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMDOMINGOMANHA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="INICIODOMINGOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FIMDOMINGOTARDE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DIASEMANA",
     *         description="tamanho: 15 caracteres (0->segunda a 6->domingo)",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="0,1,2,3,4,5,6"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="BLOQUEADO",
     *         description="admin bloqueou este prestador",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PAUSADO",
     *         description="prestador pausou seus serviços",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/prestadorfavoritocliente",
     *     summary="Adiciona uma nova linha à tabela prestador_favorito_cliente",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da prestador_tempo_entrega",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA_CLIENTE",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example="999"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA_PRESTADOR",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example="1219"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/prestadorfavoritocliente/{ID}",
     *     summary="Edita a linha especificada da tabela prestador_favorito_cliente",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da prestador_favorito_cliente",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA_CLIENTE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example="999"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA_PRESTADOR",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example="1219"
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Post(
     *     path="/api/v1/default/prestadortempoentrega",
     *     summary="Adiciona uma nova linha à tabela prestador_tempo_entrega",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da prestador_tempo_entrega",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="LISTADETEMPOSDEENTREGA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="object",
     *             example="[{'ID_PESSOA_PESSOAPRESTADOR': 999, 'ID_TEMPOENTREGA': 1, 'ATIVO': 1, 'DESCONTO': 10, 'UNIDADE_DESCONTO': 'P'}]"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/prestadortempoentrega/{ID}",
     *     summary="Edita a linha especificada da tabela prestador_tempo_entrega",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da prestador_tempo_entrega",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="LISTADETEMPOSDEENTREGA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="object",
     *             example="[{'ID_PESSOA_PESSOAPRESTADOR': 999, 'ID_TEMPOENTREGA': 1, 'ATIVO': 1, 'DESCONTO': 10, 'UNIDADE_DESCONTO': 'P'}]"
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/roupa",
     *     summary="Adiciona uma nova linha à tabela roupa",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da roupa",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),

     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 100 caracteres",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="GRUPO",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO",
     *         description="text",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PRECO_LAVAR",
     *         in="query",
     *         description="decimal(9,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PRECO_PASSAR",
     *         in="query",
     *         description="decimal(9,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PRECO_LAVAR_E_PASSAR",
     *         in="query",
     *         description="decimal(9,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="UNIDADE",
     *         description="tamanho: 3 caracteres, unidade de medida da roupa: PAR(es), PÇ(peças, unidades), etc",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="PÇ"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="COMISSAO",
     *         in="query",
     *         description="decimal(5,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/roupa/{ID}",
     *     summary="Edita a linha especificada da tabela roupa",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da roupa",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 100 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="GRUPO",
     *         description="tamanho: 15 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="OBSERVACAO",
     *         description="text",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PRECO_LAVAR",
     *         in="query",
     *         description="decimal(9,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PRECO_PASSAR",
     *         in="query",
     *         description="decimal(9,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PRECO_LAVAR_E_PASSAR",
     *         in="query",
     *         description="decimal(9,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="UNIDADE",
     *         description="tamanho: 3 caracteres, unidade de medida da roupa: PAR(es), PÇ(peças, unidades), etc",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="PÇ"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="COMISSAO",
     *         in="query",
     *         description="decimal(5,2)",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/roupasprestador",
     *     summary="Adiciona uma nova linha à tabela roupas_prestador",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da roupas_prestador",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID_ROUPA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_TEMPO_ENTREGA",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_PESSOA_PESSOAPRESTADOR",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *    @OA\Parameter(
     *         name="PRECO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PRECO_PROMOCAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATA_FIM_PROMOCAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATA_INICIO_PROMOCAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/roupasprestador/{ID}",
     *     summary="Edita a linha especificada da tabela roupas_prestador",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da roupas_prestador",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_ROUPA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_TEMPO_ENTREGA",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *      @OA\Parameter(
     *         name="ID_PESSOA_PESSOAPRESTADOR",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *      ),
     *    @OA\Parameter(
     *         name="PRECO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="PRECO_PROMOCAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="number",
     *             example=1.01
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATA_FIM_PROMOCAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DATA_INICIO_PROMOCAO",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="2010-01-01 08:00:00"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/status",
     *     summary="Adiciona uma nova linha à tabela status",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da status",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 75 caracteres",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/status/{ID}",
     *     summary="Edita a linha especificada da tabela status",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da status",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 75 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/tempoentrega",
     *     summary="Adiciona uma nova linha à tabela tempo_entrega",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da tempo_entrega",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="TEMPO_ENTREGA_HORAS",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/tempoentrega/{ID}",
     *     summary="Edita a linha especificada da tabela tempo_entrega",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da tempo_entrega",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="TEMPO_ENTREGA_HORAS",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/tipomensagem",
     *     summary="Adiciona uma nova linha à tabela tipo_mensagem",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da tipo_mensagem",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/tipomensagem/{ID}",
     *     summary="Edita a linha especificada da tabela tipo_mensagem",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da tipo_mensagem",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/default/tiposervico",
     *     summary="Adiciona uma nova linha à tabela tiposervico",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da tiposervico",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=true,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FRETE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/default/tiposervico/{ID}",
     *     summary="Edita a linha especificada da tabela tiposervico",
     *     tags={"Default"},
     *     description="Os parâmetros devem ser os mesmos da tiposervico",
     *     @OA\Response(
     *         response=200,
     *         description="Registro inserido",
     *     ),
     *     @OA\Response(
     *         response=405,
     *         description="Erro de SQL",
     *     ),
     *     @OA\Response(
     *         response=422,
     *         description="Dados inválidos, verifique os campos.",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="DESCRICAO",
     *         description="tamanho: 45 caracteres",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="string",
     *             example="A"
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="FRETE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_INSERT",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="USER_UPDATE",
     *         in="query",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    

/*------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------------------*/

     /**
     * @OA\Get(
     *     path="/armarios/raio/{raio}/pessoa/endereco/{ID}",
     *     summary="Retorna os endereços e as quantidade de gavetas disponíveis e indisponíveis dos lockers que estão dentro do raio {RAIO} especificado com centro no endereço {ID} (do endereço)",
     *     tags={"Custom"},
     *     description="Retorna os endereços e as quantidade de gavetas disponíveis e indisponíveis que estão dentro do raio {RAIO} especificado com centro no endereço {ID} (do endereço)",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Nenhum registro encontrado",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do endereço do cliente. Tabela CRM_ENDERECO_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=18
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RAIO",
     *         in="path",
     *         description="RAIO máximo de distância até o armário (default 100 km)",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=30
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/cupom/cliente/pessoa/{ID}",
     *     summary="Retorna os cupons da cliente com pessoa {ID}",
     *     tags={"Custom"},
     *     description="Retorna os cupons da cliente com pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da pessoa",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=24
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/feed/cliente/pessoa/{ID}",
     *     summary="Retorna as mensagens de FEED enviadas ao cliente com pessoa {ID}",
     *     tags={"Custom"},
     *     description="Retorna as mensagens de FEED enviadas ao cliente com pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da pessoa",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1219
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/fluxo/servico/{ID}",
     *     summary="Retorna os detalhes de fluxo do TIPOSERVICO {ID}",
     *     tags={"Custom"},
     *     description="Retorna os detalhes de fluxo do TIPOSERVICO {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do TIPOSERVICO",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/fluxos",
     *     summary="Retorna TODOS os fluxos possíveis de frete e roupas",
     *     tags={"Custom"},
     *     description="Retorna TODOS os fluxos possíveis de frete e roupas",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}",
     *     summary="Retorna dados detalhados do pedido {ID}.",
     *     tags={"Custom"},
     *     description="Retorna dados detalhados do pedido {ID}.",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=15
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}/cancelar",
     *     summary="Certifica-se de que o pedido {ID} pode ser cancelado (ainda não foi pago), e o cancela.",
     *     tags={"Custom"},
     *     description="Certifica-se de que o pedido {ID} pode ser cancelado (ainda não foi pago), e o cancela.",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}/cancelavel",
     *     summary="Retorna CANCELAVEL = true caso o pedido esteja em um estado possível de ser cancelado.",
     *     tags={"Custom"},
     *     description="Retorna CANCELAVEL = true caso o pedido esteja em um estado possível de ser cancelado.",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}/comissao",
     *     summary="Retorna o valor total e a % da comissão do pedido ID",
     *     tags={"Custom"},
     *     description="Retorna o valor total e a % da comissão do pedido ID.",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=215
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}/locker",
     *     summary="Retorna os endereços e senhas dos armários relacionados com o pedido {ID}.",
     *     tags={"Custom"},
     *     description="Retorna os endereços e senhas dos armários relacionados com o pedido {ID}.",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=61
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}/roupas",
     *     summary="Retorna detalhes das roupas do pedido {ID}.",
     *     tags={"Custom"},
     *     description="Retorna detalhes das roupas do pedido {ID}.",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=87
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}/servico/frete",
     *     summary="Retorna os detalhes do serviço de frete do pedido {ID}",
     *     tags={"Custom"},
     *     description="Retorna os detalhes do serviço de frete do pedido {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}/servico/roupa",
     *     summary="Retorna os detalhes do serviço das roupas do pedido {ID}",
     *     tags={"Custom"},
     *     description="Retorna os detalhes do serviço das roupas do pedido {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/{ID}/servicos",
     *     summary="Retorna os detalhes dos serviços de frete e das roupas do pedido {ID}",
     *     tags={"Custom"},
     *     description="Retorna os detalhes dos serviços de frete e das roupas do pedido {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/pessoa/{ID}",
     *     summary="Retorna TODOS os pedidos do CLIENTE com pessoa id {ID}",
     *     tags={"Custom"},
     *     description="Retorna TODOS os pedidos do CLIENTE com pessoa id {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA que é cliente. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pedido/prestador/pessoa/{ID}",
     *     summary="Retorna TODOS os pedidos do PRESTADOR com pessoa {ID}",
     *     tags={"Custom"},
     *     description="Retorna TODOS os pedidos do PRESTADOR com pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA que é prestador. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/pessoa/enderecos/{ID}",
     *     summary="Retorna TODOS os enderecos da pessoa {ID}",
     *     tags={"Custom"},
     *     description="Retorna TODOS os enderecos da pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da pessoa. (inclusiva pessoas com tipoPessoa = W -> armário)",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1063
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/prestador/pessoa/{id}/roupas",
     *     summary="Calcula o valor das roupas por tempo de entrega do prestador.",
     *     tags={"Custom"},
     *     description="Exibe todas as Roupas dos Grupos que são atendidas pelo prestador e estão dentro de seus tempos de entrega configurados.",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID (ou CRM_PESSOA_PRESTADOR.ID_PESSOA)",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1219
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/prestador/pessoa/{ID}",
     *     summary="Exibe as informações básicas do prestador a partir do ID da pessoa {ID}",
     *     tags={"Custom"},
     *     description="Exibe as informações básicas do prestador a partir do ID da pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         description="ID da pessoa do prestador (CRM_PESSOA.ID)",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     )
     * )
     */     
    
     /**
     * @OA\Get(
     *     path="/api/v1/prestador/pessoa/{ID}/avaliacao/{ANO}/{MES}",
     *     summary="Retorna as avaliações do prestadador com pessoa {ID} no ano {ANO} e mês {MES}",
     *     tags={"Custom"},
     *     description="Retorna as avaliações do prestadador com pessoa {ID} no ano {ANO} e mês {MES}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1219
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ANO",
     *         in="path",
     *         description="Ano de fechamento do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2020
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="MES",
     *         in="path",
     *         description="Mês de fechamento do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=4
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/prestador/pessoa/{ID}/grupo",
     *     summary="Retorna os grupos personalizados pelo prestador com pessoa {ID}",
     *     tags={"Custom"},
     *     description="Retorna os grupos personalizados pelo prestador com pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
     
     /**
     * @OA\Get(
     *     path="/api/v1/prestador/pessoa/{ID}/grupo/tempos",
     *     summary="Retorna os tempos mínimos de cada grupo do prestador {ID}",
     *     tags={"Custom"},
     *     description="Retorna os tempos mínimos de cada grupo do prestador {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA do prestadora. (CRM_PESSOA.ID)",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1219
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/prestador/pessoa/{ID}/rating",
     *     summary="Exibe as informações de rating do prestador a partir do ID da pessoa {ID}",
     *     tags={"Custom"},
     *     description="Exibe as informações de rating do prestador a partir do ID da pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         description="ID da pessoa do prestador (CRM_PESSOA.ID)",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1219
     *         )
     *     )
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/prestador/pessoa/{ID}/tempos",
     *     summary="Exibe as informações de tempos de entrega do prestador a partir do ID da pessoa {ID}",
     *     tags={"Custom"},
     *     description="Exibe as informações de tempos de entrega do prestador a partir do ID da pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         description="ID da pessoa do prestador (CRM_PESSOA.ID)",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     )
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/prestador/verifica/grupos/pessoa/{ID}",
     *     summary="Retorna TRUE caso o prestador esteja com os grupos personalizados atualizados com a tabela de grupo",
     *     tags={"Custom"},
     *     description="Retorna TRUE caso o prestador esteja com os grupos personalizados atualizados com a tabela de grupo",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/prestador/verifica/tempos/pessoa/{ID}",
     *     summary="Retorna TRUE caso o prestador esteja com os tempos personalizados atualizados com a tabela de tempos",
     *     tags={"Custom"},
     *     description="Retorna TRUE caso o prestador esteja com os tempos personalizados atualizados com a tabela de tempos",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/prestadores/raio/{RAIO}/pessoa/endereco/{ID}",
     *     summary="Retorna os endereços dos prestadores que estão dentro do raio {RAIO} especificado com centro no endereço {ID} (do endereço)",
     *     tags={"Custom"},
     *     description="Retorna os endereços dos prestadores que estão dentro do raio {RAIO} especificado com centro no endereço {ID} (do endereço)",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Response(
     *          response=404,
     *          description="Nenhum registro encontrado",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do endereço do cliente. Tabela CRM_ENDERECO_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=18
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="RAIO",
     *         in="path",
     *         description="RAIO máximo a ser aplicado no filtro (default 100 km)",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=30
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Get(
     *     path="/api/v1/roupa",
     *     summary="Retorna TODAS as roupas com suas respectivas imagens",
     *     tags={"Custom"},
     *     description="Retorna TODOS os registros da tabela roupa, com suas respectivas imagens",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     )
     * )
     */
    
     /**
     * @OA\Get(
     *     path="/api/v1/roupa/{ID}",
     *     summary="Retorna a roupa ID com sua respectiva imagem",
     *     tags={"Custom"},
     *     description="Retorna a roupa ID com sua respectiva imagem",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         description="ID da roupa (EST_ROUPA.ID)",
     *         in="path",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     )
     * )
     */
    
     /**
     * @OA\Post(
     *     path="/api/v1/prestador/calculo",
     *     summary="Calcula o valor total do pedido com base nos parametros",
     *     tags={"Custom"},
     *     description="Calcula o valor total do pedido com base nos parametros",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID_TEMPO_ENTREGA",
     *         in="query",
     *         description="ID do tempo de entrega escolhido FAT_TEMPO_ENTREGA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=2
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_TIPOSERVICO_FRETE",
     *         in="query",
     *         description="ID do tipo de servico do frete",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=4
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_PESSOA_PESSOAPRESTADOR",
     *         in="query",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1219
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="ID_CUPOM_PESSOA",
     *         in="query",
     *         description="ID dO CUPOMPESSOA a ser utilizado no cálculo. CUPOM_PESSOA.ID",
     *         required=false,
     *         @OA\Schema(
     *             type="integer",
     *             example=1
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="VALOR_TOTAL_ROUPAS",
     *         in="query",
     *         description="Valor total das roupas de acordo com o tipo de servico escolhido",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example="50.0"
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Post(
     *     path="/api/v1/prestador/pessoa/{ID}/roupas/tempoMinimo",
     *     summary="Retorna o tempo mínimo do prestador para aquele grupo de roupas (O gargalo é o grupo com o maior tempo)",
     *     tags={"Custom"},
     *     description="Retorna o tempo mínimo do prestador para aquele grupo de roupas (O gargalo é o grupo com o maior tempo)",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1219
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="roupas",
     *         in="query",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="object",
     *             example="[{'ID_ROUPA':1},{'ID_ROUPA':4},{'ID_ROUPA':5}]"
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Post(
     *     path="/api/v1/prestadores/filtro",
     *     summary="Retorna os prestadores de acordo com os filtros da tela de filtros",
     *     tags={"Custom"},
     *     description="ENTREGA_CLIENTE_BUSCA_PEDIDO (true,false), ID_ENDERECO_CLIENTE (id endereço atual do cliente), ENTREGA_DOMICILIO (true,false), ENTREGA_ARMARIO (true,false), ROUPAS[{ID_ROUPA,QUANTIDADE}](para fins de filtro e cálculo dos valores),LAVAR (true,false), PASSAR (true,false), DISTANCIA_MAXIMA (int), TEMPO_MAXIMO_ENTREGA_HORAS (int, tempo máximo de demora da lavanderia para as roupas enviadas neste mesmo filtro)",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="query",
     *         description="ID da PESSOA que é prestadora. Tabela CRM_PESSOA.ID",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    

/*------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------------------*/

/*------------------------------------------------------------------------------------------------------------------------------*/

     /**
     * @OA\Put(
     *     path="/api/v1/pedido/{ID}/servico/frete/decrementar",
     *     summary="Decrementa o status do serviço escolhido para as roupas, do pedido {ID}",
     *     tags={"Custom"},
     *     description="Decrementa o status do serviço escolhido para as roupas, do pedido {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/pedido/{ID}/servico/frete/incrementar",
     *     summary="Incrementa o status do serviço escolhido para as roupas, do pedido {ID}",
     *     tags={"Custom"},
     *     description="Incrementa o status do serviço escolhido para as roupas, do pedido {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */
    
     /**
     * @OA\Put(
     *     path="/api/v1/pedido/{ID}/servico/roupa/decrementar",
     *     summary="Decrementa o status do serviço escolhido para as roupas, do pedido {ID}",
     *     tags={"Custom"},
     *     description="Decrementa o status do serviço escolhido para as roupas, do pedido {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Put(
     *     path="/api/v1/pedido/{ID}/servico/roupa/incrementar",
     *     summary="Incrementa o status do serviço escolhido para as roupas, do pedido {ID}",
     *     tags={"Custom"},
     *     description="Incrementa o status do serviço escolhido para as roupas, do pedido {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do pedido",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=999
     *         )
     *     ),
     * )
     */

     /**
     * @OA\Put(
     *     path="/api/v1/prestador/pessoa/{ID}/rating",
     *     summary="Atualiza o rating do prestador com pessoa {ID}",
     *     tags={"Custom"},
     *     description="Atualiza o rating do prestador com pessoa {ID}",
     *     @OA\Response(
     *          response=200,
     *          description="Record list!",
     *     ),
     *     @OA\Parameter(
     *         name="ID",
     *         in="path",
     *         description="ID do prestador",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             example=1219
     *         )
     *     ),
     *     @OA\Parameter(
     *         name="rating",
     *         in="query",
     *         description="rating de 0 a 5",
     *         required=true,
     *         @OA\Schema(
     *             type="number",
     *             example=2.5
     *         )
     *     ),
     * )
     */

     
    
