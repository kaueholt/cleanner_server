<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;
use App\Http\Controllers\PessoaController;

class LockerController extends Controller{
  private function countGavetasDisponiveis($id){
    $sqlGavetas = " SELECT COUNT(G.ID) AS GAVETAS_DISPONIVEIS
                    FROM EST_ARMARIO_GAVETA G
                    WHERE G.DISPONIVEL = TRUE
                      AND G.ID_PESSOA = $id";
    $responseGavetas = DB::select($sqlGavetas);
    return $responseGavetas ? $responseGavetas[0]->GAVETAS_DISPONIVEIS : 0;
  }

  private function countGavetasIndisponiveis($id){
    $sqlGavetas = " SELECT COUNT(G.ID) AS GAVETAS_INDISPONIVEIS
                    FROM EST_ARMARIO_GAVETA G
                    WHERE G.DISPONIVEL = FALSE
                      AND G.ID_PESSOA = $id";
    $responseGavetas = DB::select($sqlGavetas);
    return $responseGavetas ? $responseGavetas[0]->GAVETAS_INDISPONIVEIS : 0;
  }

  // a.k.a wordrobe, armário
  public function getArmariosRaioDoClienteEndereco($raio = 100,$id){
    $sqlCliente = "SELECT LATITUDE, LONGITUDE FROM CRM_ENDERECO_PESSOA WHERE ID = {$id} LIMIT 1";
    $responseCliente = DB::select($sqlCliente);
    $sqlArmarios = " SELECT E.*, P.NOME, P.OBSERVACAO
                      FROM CRM_ENDERECO_PESSOA E
                      LEFT JOIN CRM_PESSOA P 
                        ON P.ID = E.ID_PESSOA
                      WHERE P.TIPOPESSOA = 'W'";
    $responseArmarios = DB::select($sqlArmarios);
    if(!$responseCliente || !$responseArmarios)
        return Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    $pessoa = new PessoaController();  
    for($i=0; $i < sizeof($responseArmarios); $i++){
        $responseArmarios[$i]->DISTANCIAKM = $pessoa->calculaDistanciaAPartirDasLatitudesLongitudes($responseCliente[0]->LATITUDE, $responseCliente[0]->LONGITUDE, $responseArmarios[$i]->LATITUDE, $responseArmarios[$i]->LONGITUDE);
        $responseArmarios[$i]->DISTANCIAKM > $raio ? array_splice($responseArmarios, $i--, 1) : null;  
    }
    for($i=0; $i < sizeof($responseArmarios); $i++){
      $responseArmarios[$i]->GAVETAS_DISPONIVEIS = $this->countGavetasDisponiveis($responseArmarios[$i]->ID_PESSOA);
      $responseArmarios[$i]->GAVETAS_INDISPONIVEIS = $this->countGavetasIndisponiveis($responseArmarios[$i]->ID_PESSOA);
    }
    return $responseArmarios ? Handles::jsonResponse(true, 'Registros encontrados!', $responseArmarios, 200)
        : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
} 
  
    
}
