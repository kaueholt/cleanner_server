<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;


class RoupaController extends Controller{
    
    public function index(){
        $sis_url = env('SISTEMA_URL');
        $sql  = "SELECT R.ID as ID_ROUPA, R.GRUPO, G.DESCRICAO AS DESCGRUPO, I.ID as ID_IMAGEM, R.PRECO_LAVAR, R.PRECO_PASSAR, R.PRECO_LAVAR_E_PASSAR, R.UNIDADE, R.COMISSAO, R.DESCRICAO, IF( (I.ID_DOC <> '' and I.ID_DOC is not null), CONCAT('".$sis_url."','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK ";
        $sql .= "FROM EST_ROUPA R ";
        $sql .= "INNER JOIN EST_GRUPO G ON R.GRUPO = G.GRUPO ";
        $sql .= "LEFT JOIN AMB_IMAGEM I ON R.ID = I.ID_DOC AND I.MODULO = 'EST' AND I.DESTAQUE='S' AND I.MODULO='EST'";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
        : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }

    public function somaValores($listaRoupas){
        $valorBaseRoupas = 0;
        for($i=0; $i<count($listaRoupas); $i++){
            $valorBaseRoupas += (isset($listaRoupas[$i]['PRECOVENDA']) && isset($listaRoupas[$i]['QUANTIDADE'])) ? 
                                $listaRoupas[$i]['PRECOVENDA'] * $listaRoupas[$i]['QUANTIDADE'] : 0;
        }
        return $valorBaseRoupas;
    }

    public function somaValoresRoupas($listaRoupas,$listaDeIdRoupas = false){
        $valorBaseRoupasLavar = 0;
        $valorBaseRoupasPassar = 0;
        $valorBaseRoupasLavarEPassar = 0;
        if(!$listaDeIdRoupas){
            $listaDeIdRoupas = [];
            for($i=0; $i<count($listaRoupas); $i++){
                $listaDeIdRoupas[$i] = $listaRoupas[$i]['ID_ROUPA'];                
            }
        }
        $idRoupasStringged = '('. implode(',',$listaDeIdRoupas).')';
        $sqlValoresRoupas = "SELECT DISTINCT
                                ID,
                                PRECO_LAVAR,
                                PRECO_PASSAR,
                                PRECO_LAVAR_E_PASSAR
                            FROM
                                EST_ROUPA
                            WHERE ID IN {$idRoupasStringged}";
        $valoresRoupas = DB::select($sqlValoresRoupas);
        foreach($valoresRoupas as $roupa){
            $indexArrayParametros = array_search($roupa->ID, array_column($listaRoupas,'ID_ROUPA'));
            if($indexArrayParametros>=0){
                $valorBaseRoupasLavar += $roupa->PRECO_LAVAR ? $roupa->PRECO_LAVAR*$listaRoupas[$indexArrayParametros]['QUANTIDADE'] : 0;
                $valorBaseRoupasPassar += $roupa->PRECO_PASSAR ? $roupa->PRECO_PASSAR*$listaRoupas[$indexArrayParametros]['QUANTIDADE'] : 0;
                $valorBaseRoupasLavarEPassar += $roupa->PRECO_LAVAR_E_PASSAR ? $roupa->PRECO_LAVAR_E_PASSAR*$listaRoupas[$indexArrayParametros]['QUANTIDADE'] : 0;
            }
        }
        $totais = array(
            'valorTotalRoupasLavar' => $valorBaseRoupasLavar,
            'valorTotalRoupasPassar' => $valorBaseRoupasPassar,
            'valorTotalRoupasLavarEPassar' => $valorBaseRoupasLavarEPassar
        );
        return ($valorBaseRoupasLavar || $valorBaseRoupasPassar || $valorBaseRoupasLavarEPassar) 
                ? Handles::jsonResponse(true, "Valor total das roupas calculado!", $totais, 200)
                : Handles::jsonResponse(false, "Valores zerados!", $totais, 404);
    }

    public function getRoupasPedido($id){
        $sis_url = env('SISTEMA_URL');
        $sql  = "SELECT 
                    PI.STATUS,
                    PI.QUANTIDADE,
                    PI.VALOR_UNITARIO,
                    PI.VALOR_TOTAL,
                    PI.OBSERVACAO_CANCELAMENTO,
                    PI.OBSERVACAO_CLIENTE,
                    PI.NOVA_OBSERVACAO_CLIENTE,
                    PI.OBSERVACAO_LAVANDERIA,
                    PI.NOVA_OBSERVACAO_LAVANDERIA,
                    PI.COMISSAO,
                    PI.ID AS ID_PRODUTOITEM,
                    R.ID AS ID_ROUPA,
                    I.ID AS ID_IMAGEM,
                    -- R.PRECO_LAVAR,
                    -- R.PRECO_PASSAR,
                    -- R.PRECO_LAVAR_E_PASSAR,
                    R.UNIDADE,
                    R.DESCRICAO,
                    IF((I.ID_DOC <> '' AND I.ID_DOC IS NOT NULL),
                        CONCAT('{$sis_url}',
                                '/images/doc/',
                                LOWER(I.MODULO),
                                '/',
                                I.ID_DOC,
                                '/',
                                I.ID,
                                '.jpg'),
                        '') AS LINK
                FROM
                    FAT_PEDIDO_ITEM PI
                        LEFT JOIN
                    EST_ROUPA R ON PI.ID_ROUPA = R.ID
                        LEFT JOIN
                    AMB_IMAGEM I ON R.ID = I.ID_DOC AND I.MODULO = 'EST'
                WHERE
                    PI.ID_PEDIDO = {$id}";
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
        : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }
    
    public function show($id){
        $sis_url = env('SISTEMA_URL');
        $sql  = "SELECT R.ID as ID_ROUPA, I.ID as ID_IMAGEM, R.PRECO_LAVAR, R.PRECO_PASSAR, R.PRECO_LAVAR_E_PASSAR, ";
        $sql .= "R.UNIDADE, R.DESCRICAO, ";
        $sql .= "IF( (I.ID_DOC <> '' and I.ID_DOC is not null), CONCAT('".$sis_url."','/images/doc/',LOWER(I.MODULO),'/',I.ID_DOC,'/',I.ID,'.jpg'), '') as LINK ";
        $sql .= "FROM EST_ROUPA R ";
        $sql .= "LEFT JOIN AMB_IMAGEM I ON R.ID = I.ID_DOC AND I.MODULO = 'EST' ";
        $sql .= "WHERE R.ID = ".$id;
        $response = DB::select($sql);
        return $response ? Handles::jsonResponse(true, 'Registros encontrados!', $response, 200)
        : Handles::jsonResponse(false, 'Nenhum registro encontrado!', [], 404);
    }
}
