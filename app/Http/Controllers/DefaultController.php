<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Utils\Handles;
use App\Http\Controllers\PrestadorController;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\PagamentoController;

class DefaultController extends Controller{
    public function destroy($table, $id){
        $model = "App\\Models\\".$table;
        try {
            $query = new $model();
            $response = $model::find($id);
            if(!$response) {
                return Handles::jsonResponse(false, 'Registro não encontrado na tabela de endereços.', $response, 404);
            }
            if($table == 'enderecopessoa'){
                $modelPedido = "App\\Models\\pedido";
                $possuiPedidoNesteEndereco = $modelPedido::where('ID_ENDERECO_PESSOA', $id)->first();
                if($possuiPedidoNesteEndereco){
                    return Handles::jsonResponse(false, 'Este endereço possui pedidos associados.', $possuiPedidoNesteEndereco, 403);
                }
                if($response->FAVORITO){
                    $newFavorito = $model::where('ID_PESSOA',$response->ID_PESSOA)->where('ID','<>',$id)->first();
                    if($newFavorito){
                        $newFavorito->FAVORITO = true;
                        $responseNovoFavoritoForcado = $newFavorito->save();
                    }
                }
            }
            $deleteResponse = $response->delete();
            return Handles::jsonResponse(true, 'Registro excluído com sucesso!', $deleteResponse, 200);
        } catch (\Illuminate\Database\QueryException $exception) {
            return Handles::jsonResponse(false, 'Registro não excluído da tabela de endereços. Erro de validação.', $exception, 405);
        }
    }

    public function index(Request $request, $table){
        $model = "App\\Models\\".$table;
        switch($table){
            case 'tempoentrega':  
                $response = $model::orderby('TEMPO_ENTREGA_HORAS','asc')->get();
            break;
            case 'roupa':
                $response = $model::select('EST_ROUPA.*','EST_GRUPO.DESCRICAO AS GRUPO_DESCRICAO')
                    ->join('EST_GRUPO','EST_ROUPA.GRUPO','EST_GRUPO.GRUPO') //SOLICITADO POR ANTONIO EM 27/04/2020 16:40
                    ->get();
            break;
            default:
                $response = $model::all();
            break;
        }            
        if($response){
            unset($response['PASSWORD']);
            unset($response['SENHA']);
            unset($response['EMAILSENHA']);
            unset($response['FONE']);
            unset($response['CELULAR']);
        }
        return Handles::jsonResponse(true, 'Registros encontrados!', $response, 200);
    }

    public function show($table, $id){        
        $model = "App\\Models\\".$table;
        $response = $model::find($id);
        if(!$response) {
            return Handles::jsonResponse(false, 'Registro '.$id.' não encontrado!', $response, 404);
        }
        unset($response['PASSWORD']);
        unset($response['SENHA']);
        unset($response['EMAILSENHA']);
        unset($response['FONE']);
        unset($response['CELULAR']);
        return Handles::jsonResponse(true, 'Registro '.$id.' encontrado!', $response, 200);
    }
    
    public function store(Request $request, $table){
        $model = "App\\Models\\".$table;
        $payload = $request->all();
        $payload['CREATED_AT'] = date('Y-m-d h:i:s');
        DB::beginTransaction();
        try {
            $query = new $model();
            //verifica se nos parâmetros está sendo passada a chave primária, se estiver, verifica se já não existe chave igual
            $chavePrimariaJaExiste = isset($payload[$query->primaryKey]) ? $model::find($payload[$query->primaryKey]) : null;
            if ($chavePrimariaJaExiste){
                return Handles::jsonResponse(false, 'Registro não inserido - Chave '. $query->primaryKey .'->\''.$payload[$query->primaryKey] .'\' já existe!', $chavePrimariaJaExiste, 421);
            }
            switch($table){
                case 'pessoa':                    
                    //Ao adicionar pessoa, adicionar também usuário.
                    unset($payload['DATA_NASCIMENTO']);
                    $query->fill($payload);
                    // SALVA PESSOA
                    $pessoaResponse = $query->save();
                    // SALVA USUÁRIO
                    $modelUsuario = "App\\Models\\usuario";
                    $queryUsuario = new $modelUsuario();
                    $payload['LOGIN'] = $payload['EMAIL'];
                    $payload['ID_PESSOA'] = $query->getKey();
                    $payload['SITUACAO'] = 'A';
                    // $payload['TIPO'] = 'Z'; //FISICO / JURIDICO
                    $queryUsuario->fill($payload);
                    $usuarioResponse = $queryUsuario->save();
                    // SALVA PRESTADOR
                    if($payload['TIPOPESSOA'] == 'P'){ // PRESTADOR
                        $modelPrestador = "App\\Models\\pessoaprestador";
                        $queryPrestador = new $modelPrestador();
                        $payload['NOMECONTATO'] = isset($payload['NOMECONTATO']) ? $payload['NOMECONTATO'] : substr($payload['NOME'],0,65);
                        $payload['MAX_CLIENTES_DIA'] = isset($payload['MAX_CLIENTES_DIA']) ? $payload['MAX_CLIENTES_DIA'] : 15;
                        $payload['MAX_DISTANCIA_ATENDIMENTO'] = isset($payload['MAX_DISTANCIA_ATENDIMENTO']) ? $payload['MAX_DISTANCIA_ATENDIMENTO'] : 15;
                        $payload['VALOR_MINIMO_PEDIDO'] = isset($payload['VALOR_MINIMO_PEDIDO']) ? $payload['VALOR_MINIMO_PEDIDO'] : 30.00;
                        $payload['INICIOMANHA'] = isset($payload['INICIOMANHA']) ? $payload['INICIOMANHA'] : '06:00:00';
                        $payload['FIMMANHA'] = isset($payload['FIMMANHA']) ? $payload['FIMMANHA'] : '12:00:00';  
                        $payload['INICIOTARDE'] = isset($payload['INICIOTARDE']) ? $payload['INICIOTARDE'] : '12:00:00';
                        $payload['FIMTARDE'] = isset($payload['FIMTARDE']) ? $payload['FIMTARDE'] : '20:00:00';
                        $payload['INICIOSABADOMANHA'] = isset($payload['INICIOSABADOMANHA']) ? $payload['INICIOSABADOMANHA'] : '06:00:00';
                        $payload['FIMSABADOMANHA'] = isset($payload['FIMSABADOMANHA']) ? $payload['FIMSABADOMANHA'] : '12:00:00';
                        $payload['INICIOSABADOTARDE'] = isset($payload['INICIOSABADOTARDE']) ? $payload['INICIOSABADOTARDE'] : '12:00:00';
                        $payload['FIMSABADOTARDE'] = isset($payload['FIMSABADOTARDE']) ? $payload['FIMSABADOTARDE'] : '20:00:00';
                        $payload['INICIODOMINGOMANHA'] = isset($payload['INICIODOMINGOMANHA']) ? $payload['INICIODOMINGOMANHA'] : '06:00:00';
                        $payload['FIMDOMINGOMANHA'] = isset($payload['FIMDOMINGOMANHA']) ? $payload['FIMDOMINGOMANHA'] : '12:00:00';
                        $payload['INICIODOMINGOTARDE'] = isset($payload['INICIODOMINGOTARDE']) ? $payload['INICIODOMINGOTARDE'] : '12:00:00';
                        $payload['FIMDOMINGOTARDE'] = isset($payload['FIMDOMINGOTARDE']) ? $payload['FIMDOMINGOTARDE'] : '20:00:00';
                        $payload['DIASEMANA'] = isset($payload['DIASEMANA']) ? $payload['DIASEMANA'] : '0,1,2,3,4,5,6';
                        $payload['BLOQUEADO'] = isset($payload['BLOQUEADO']) ? $payload['BLOQUEADO'] : false;
                        $payload['PAUSADO'] = isset($payload['PAUSADO']) ? $payload['PAUSADO'] : false;
                        $payload['TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS'] = isset($payload['TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS']) ? $payload['TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS'] : 0;
                        $payload['TAXA_ENTREGA_DOMICILIO_REAIS'] = isset($payload['TAXA_ENTREGA_DOMICILIO_REAIS']) ? $payload['TAXA_ENTREGA_DOMICILIO_REAIS'] : 0;
                        $payload['TAXA_ENTREGA_ARMARIO_REAIS'] = isset($payload['TAXA_ENTREGA_ARMARIO_REAIS']) ? $payload['TAXA_ENTREGA_ARMARIO_REAIS'] : 0;
                        $queryPrestador->fill($payload);
                        $prestadorResponse = $queryPrestador->save();

                        $modelEndereco = "App\\Models\\enderecopessoa";
                        $queryEndereco = new $modelEndereco();
                        $payload['FAVORITO'] = true;
                        $payload['ICONE'] = 'business';
                        $payload['TAG'] = substr($payload['NOMEREDUZIDO'],0,30);
                        $queryEndereco->fill($payload);

                        $sqlAPI_GOOGLE_KEY = "SELECT API_GOOGLE_KEY FROM AMB_PARAMETRO WHERE API_GOOGLE_KEY IS NOT NULL LIMIT 1";
                        $responseAPI_GOOGLE_KEY = DB::select($sqlAPI_GOOGLE_KEY);
                        if(!isset($responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY)){
                            return Handles::jsonResponse(false, 'Nenhuma chave de API do google maps configurada!', $responseAPI_GOOGLE_KEY, 400);
                        }
                        $APIKEY = $responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY;
                        $urlMapsGeocodingAPI = str_replace (" ","+","https://maps.googleapis.com/maps/api/geocode/json?address='{$payload['ENDERECO']},{$payload['NUMERO']},{$payload['CIDADE']},{$payload['UF']}&key={$APIKEY}");
                        
                        $googleAPI = curl_init();
                        curl_setopt($googleAPI, CURLOPT_URL, $urlMapsGeocodingAPI);
                        curl_setopt($googleAPI, CURLOPT_RETURNTRANSFER, true);
                        $googleAPI = json_decode(curl_exec($googleAPI));
                        
                        if(!isset($googleAPI->results[0])){
                            return Handles::jsonResponse(false, isset($googleAPI->status) ? $googleAPI->status : 'Endereço inválido!', $googleAPI, 400);
                        }

                        $queryEndereco['LATITUDE'] = round($googleAPI->results[0]->geometry->location->lat, 7);
                        $queryEndereco['LONGITUDE'] = round($googleAPI->results[0]->geometry->location->lng, 7);
                        $enderecoResponse = $queryEndereco->save();

                        $response = array(
                            'ID_pessoa' => $query->getKey(), 
                            'pessoa' => $pessoaResponse,
                            'usuario' => $usuarioResponse,
                            'endereco' => $enderecoResponse,
                            'prestador' => $prestadorResponse);
                    }else{
                        $response = array(
                            'pessoa' => $pessoaResponse,
                            'ID_pessoa' => $query->getKey(),
                            'usuario' => $usuarioResponse);
                    }
                    DB::commit();
                    return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $response, 200);
                break;
                case 'pedido':
                    // if(!isset($payload['PAGAMENTO']))
                    //     return Handles::jsonResponse(false, 'Dados de pagamento faltantes. ( PAGAMENTO: {Iuggu iugu} )', $payload, 472);
                    $payload['ID_PESSOA_PESSOAPRESTADOR'] = isset($payload['ID_PESSOA_PRESTADOR']) 
                        ? $payload['ID_PESSOA_PRESTADOR'] 
                        : $payload['ID_PESSOA_PESSOAPRESTADOR'];
                    $payload['ID_PESSOA_PRESTADOR'] = isset($payload['ID_PESSOA_PRESTADOR']) 
                        ? $payload['ID_PESSOA_PRESTADOR']
                        : $payload['ID_PESSOA_PESSOAPRESTADOR'];
                    $payload['COMISSAO'] = $payload['ID_PESSOA_PRESTADOR'];
                    $prestador = new PrestadorController();
                    if($prestador->idPessoaPrestadorBloqueado($payload['ID_PESSOA_PESSOAPRESTADOR']))
                        return Handles::jsonResponse(false, 'Conta bloqueada.', [], 470);
                    if(isset($payload['ID_CUPOM_PESSOA'])){
                        $modelCupomPessoa = "App\\Models\\cupompessoa";
                        $queryCupom = $modelCupomPessoa::where('ID',$payload['ID_CUPOM_PESSOA'])
                                ->where('ID_PESSOA',$payload['ID_CLIENTE'])
                                ->first(); 
                        if(isset($queryCupom)){
                            if(!$queryCupom['UTILIZADO']){
                                $sqlValidadeCupom = "SELECT 
                                            ID,
                                            DESCRICAO,
                                            VALIDADEFIM,
                                            IF(NOW() > VALIDADEFIM, FALSE, TRUE) AS VALIDO
                                        FROM
                                            FAT_CUPOM
                                        WHERE
                                            ID = {$queryCupom['ID_CUPOM']} 
                                        LIMIT 1";
                                $cupomDentroDaValidade = DB::select($sqlValidadeCupom);
                                if($cupomDentroDaValidade[0]->VALIDO){
                                    $queryCupom['UTILIZADO'] = 1;
                                    $cupomResponse = $queryCupom->save();
                                    if(!$cupomResponse){
                                        DB::rollback();
                                        return Handles::jsonResponse(false, "Erro ao atualizar o cupom {$payload['ID_CUPOM_PESSOA']}", $cupomResponse, 500);
                                    }
                                }else{
                                    DB::rollback();
                                    return Handles::jsonResponse(false, "O cupom {$payload['ID_CUPOM_PESSOA']} já não é mais válido.", $cupomDentroDaValidade[0], 505);
                                }
                            }else{
                                DB::rollback();
                                return Handles::jsonResponse(false, "Cupom {$payload['ID_CUPOM_PESSOA']} já utilizado!", $queryCupom, 422);
                            }
                        }else{
                            DB::rollback();
                            return Handles::jsonResponse(false, "Cupom {$payload['ID_CUPOM_PESSOA']} não encontrado!", [], 404);
                        }                            
                    }                 
                    $sqlFrete = "SELECT ID FROM FAT_STATUS_SERVICO WHERE ID_TIPOSERVICO = {$payload['ID_TIPO_SERVICO_FRETE']} AND SEQUENCIA = 1 LIMIT 1";
                    $sqlRoupas = "SELECT ID FROM FAT_STATUS_SERVICO WHERE ID_TIPOSERVICO = {$payload['ID_TIPO_SERVICO_ROUPAS']} AND SEQUENCIA = 1 LIMIT 1";
                    $idStatusServicoFrete = DB::select($sqlFrete);
                    $idStatusServicoRoupas = DB::select($sqlRoupas);
                    $payload['ID_STATUS_SERVICO_FRETE'] = $idStatusServicoFrete[0]->ID;
                    $payload['ID_STATUS_SERVICO_ROUPAS'] = $idStatusServicoRoupas[0]->ID;                                                                                
                    $taxas = $prestador->getPrestadorTaxasEntrega($payload['ID_PESSOA_PESSOAPRESTADOR'])->getData();
                    if($taxas->success){
                        switch($payload['ID_TIPO_SERVICO_FRETE']){
                            case '3':
                                $payload['TAXA_ENTREGA_FRETE_REAIS'] = is_null($taxas->data[0]->PRESTADOR_TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS) ?
                                    0 : $taxas->data[0]->PRESTADOR_TAXA_ENTREGA_CLIENTE_BUSCA_PEDIDO_REAIS;
                            break;
                            case '4':
                                $payload['TAXA_ENTREGA_FRETE_REAIS'] = is_null($taxas->data[0]->PRESTADOR_TAXA_ENTREGA_DOMICILIO_REAIS) ?
                                    0 : $taxas->data[0]->PRESTADOR_TAXA_ENTREGA_DOMICILIO_REAIS;
                            break;
                            case '5':
                                $payload['TAXA_ENTREGA_FRETE_REAIS'] = is_null($taxas->data[0]->PRESTADOR_TAXA_ENTREGA_ARMARIO_REAIS) ?
                                    0 : $taxas->data[0]->PRESTADOR_TAXA_ENTREGA_ARMARIO_REAIS;
                            break;
                        }
                    }else{
                        $payload['TAXA_ENTREGA_FRETE_REAIS'] = 0;
                    }
                    // $descontoTempo = $prestador->getDescontoTempoEntrega($payload['ID_PESSOA_PESSOAPRESTADOR'],$payload['ID_PRAZO'])->getData();
                    // if($descontoTempo->success){
                    //     $payload['PERCENT_DESCONTO_PRESTADOR_TEMPO_ENTREGA'] = $descontoTempo->data[0]->DESCONTO;
                    // }else{
                    //     $payload['PERCENT_DESCONTO_PRESTADOR_TEMPO_ENTREGA'] = 0;
                    // } //comentado por agora o PERCENT_DESCONTO_PRESTADOR_TEMPO_ENTREGA vem direto do front
                    $prazoHoras = $prestador->getPrazoPrestadorEmHoras($payload['ID_PRAZO'],$payload['ID_PESSOA_PESSOAPRESTADOR']);
                    if(!$prazoHoras->getData()->success){
                        DB::rollback();
                        return $prazoHoras; //Handles
                    } 
                    $pedido = new PedidoController();
                    $payload['DATA_ENTREGA'] = $pedido->calculaDataPrevistaEntregaHorasCorridas($prazoHoras->getData()->data[0]->TEMPO_ENTREGA_HORAS);
                    $query->fill($payload);
                    $pedidoResponse = $query->save();
                    $modelPedidoItem = "App\\Models\\pedidoitem";                    
                    $ID_PEDIDO = $query->getKey();
                    $pedidoItem = $payload['PEDIDOITEM'];
                    switch($payload['ID_TIPO_SERVICO_ROUPAS']){
                        case 1:
                            $tipoServicoRoupas = "PRECO_LAVAR";
                        break;
                        case 2:
                            $tipoServicoRoupas = "PRECO_PASSAR";
                        break;
                        case 6:
                            $tipoServicoRoupas = "PRECO_LAVAR_E_PASSAR";
                        break;
                    }
                    //PEDIDOITEM PODE VIR ARRAY DE OBJETOS OU SÓ OBJETO -> SE FOR OBJETO É TRANSFORMADO PARA ARRAY DE OBJETOS
                    ( (!isset($pedidoItem[0])) && (isset($pedidoItem['ID_ROUPA'])) ) ? $pedidoItem = array($pedidoItem) : null; 

                    for ($i=0; $i<count($pedidoItem); $i++) {
                        $sqlValorRoupa = "  SELECT 
                                                R.DESCRICAO,
                                                R.{$tipoServicoRoupas} as PRECOVENDA,
                                                IF(R.COMISSAO,R.COMISSAO,G.COMISSAO) AS COMISSAO   
                                            FROM EST_ROUPA R
                                            LEFT JOIN EST_GRUPO G
                                                ON R.GRUPO = G.GRUPO
                                            WHERE ID = {$pedidoItem[$i]['ID_ROUPA']}
                                            LIMIT 1";
                        $valorRoupa = DB::select($sqlValorRoupa);
                        if(!isset($valorRoupa[0])){
                            $sqlRoupas = "SELECT ID FROM EST_ROUPA LIMIT 1000";
                            $todasAsRoupas = DB::select($sqlRoupas);
                            $listaDeIdRoupas = [];
                            for($j=0; $j<count($todasAsRoupas); $j++){
                                $listaDeIdRoupas[$j] = $todasAsRoupas[$j]->ID;
                            }
                            $idRoupasStringged = '('. implode(',',$listaDeIdRoupas).')';
                            throw new \Exception("Roupa ID {$pedidoItem[$i]['ID_ROUPA']} não encontrada. ID's existentes: {$idRoupasStringged}",404);
                        }
                        $pedidoItem[$i]['ID_PEDIDO'] = $ID_PEDIDO;                        
                        $pedidoItem[$i]['DESCRICAO'] = $valorRoupa[0]->DESCRICAO;                        
                        $pedidoItem[$i]['COMISSAO'] = $valorRoupa[0]->COMISSAO;
                        $pedidoItem[$i]['VALOR_UNITARIO'] = $pedidoItem[$i]['TOTALROUPAUNITARIO'];
                        $pedidoItem[$i]['VALOR_TOTAL'] = $pedidoItem[$i]['TOTALROUPA'];
                        $pedidoItem[$i]['QUANTIDADE'] = $pedidoItem[$i]['QUANT'];
                        // $pedidoItem[$i]['VALOR_UNITARIO'] = $valorRoupa[0]->PRECOVENDA;
                        // $pedidoItem[$i]['VALOR_TOTAL'] = $valorRoupa[0]->PRECOVENDA * $pedidoItem[$i]['QUANTIDADE'];
                        //IUGU
                        $pedidoItem[$i]['price_cents'] = $pedidoItem[$i]['TOTALROUPAUNITARIO']*100;
                        // $pedidoItem[$i]['quantity'] = $pedidoItem[$i]['QUANTIDADE'];
                        $pedidoItem[$i]['quantity'] = $pedidoItem[$i]['QUANT'];
                        $pedidoItem[$i]['description'] = $pedidoItem[$i]['DESCRICAO'];
                        $queryPedidoItem = new $modelPedidoItem();
                        $queryPedidoItem->fill($pedidoItem[$i]);
                        $pedidoItemResponse = $queryPedidoItem->save();
                        unset($pedidoItem[$i]['VALOR_TOTAL']);
                        unset($pedidoItem[$i]['COMISSAO']);
                        unset($pedidoItem[$i]['DESCRICAO']);
                        unset($pedidoItem[$i]['ID_PEDIDO']);
                        unset($pedidoItem[$i]['ID_ROUPA']);
                        unset($pedidoItem[$i]['QUANTIDADE']);
                        unset($pedidoItem[$i]['QUANT']);
                        unset($pedidoItem[$i]['VALOR_UNITARIO']);
                        unset($pedidoItem[$i]['TOTALROUPAUNITARIO']);
                        unset($pedidoItem[$i]['TOTALROUPA']);
                    }
                    if(!$pedidoItemResponse){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Tempo de entrega do prestador incorreto!', [], 425);
                    }
                    $payload['PAGAMENTO']['items'] = $pedidoItem;
                    $pagamento = new PagamentoController();     
                    $respostaIugu = $pagamento->cobrancaDireta($payload['PAGAMENTO']);
                    $msgErroIugu = isset($respostaIugu->errors) 
                        ? json_encode($respostaIugu->errors)
                        : 'Erro na comunicação com o Iugu';
                    if(!isset($respostaIugu->success) || !isset($respostaIugu->invoice_id)){
                        DB::rollback();
                        return Handles::jsonResponse(false, $msgErroIugu, $respostaIugu, 408);
                    }
                    if(!$respostaIugu->success || !$respostaIugu->invoice_id){
                        DB::rollback();
                        return Handles::jsonResponse(false, $msgErroIugu, $respostaIugu, 408);
                    }
                    $additionalPayload = array('INVOICE_IUGU'=> $respostaIugu->invoice_id);
                    $query->fill($additionalPayload);
                    $pedidoResponse = $query->save();
                    $response = array(
                        'ID_PEDIDO' => $ID_PEDIDO,
                        'pedido' => $pedidoResponse,
                        'pedidoItem' => $pedidoItemResponse,
                        'pagamento' => $respostaIugu,
                        'cupom' => isset($cupomResponse) 
                            ? $cupomResponse 
                            : false,
                        'teste' => isset($payload['TESTE']) 
                            ? $payload['TESTE'] 
                            : false
                    );
                    isset($payload['TESTE']) 
                        ? $payload['TESTE'] 
                            ? DB::rollback() 
                            : DB::commit() 
                        : DB::commit();
                    return isset($payload['TESTE']) 
                        ? $payload['TESTE'] 
                            ? Handles::jsonResponse(false, 'Pedido do tipo orçamento.', $response, 422) 
                            : Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $response, 200) 
                        : Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $response, 200);
                break;
                case 'grupoprestador':
                    // $gruposPrestador = $payload['LISTADEGRUPOPRESTADOR'];
                    // $pessoaPrestador = $gruposPrestador[0]['ID_PESSOA_PESSOAPRESTADOR'];
                    // if(count($gruposPrestador) != DB::table('EST_GRUPO')->count())
                    //     return Handles::jsonResponse(false, 'O número de grupos personalizados (' . count($gruposPrestador) . ') não bate com o número de grupos existentes ('.DB::table('EST_GRUPO')->count().').', $gruposPrestador, 422);
                    // $response = true;
                    // // grava prestador
                    // for ($i=0; $i<count($gruposPrestador); $i++) {
                    //     if(!$response){
                    //         DB::rollback();
                    //         return Handles::jsonResponse(false, 'Erro com os dados do prestador, verifique os campos', [], 422);
                    //     }
                    //     $query = $model::where('ID_PESSOA_PESSOAPRESTADOR',$pessoaPrestador)
                    //                     ->where('ID_GRUPO',$gruposPrestador[$i]['ID_GRUPO'])
                    //                     ->first();
                    //     if(!$query)
                    //         $query = new $model();
                    //     $query->fill($gruposPrestador[$i]);
                    //     $response = $query->save();
                    // }
                    // if(!$response){
                    //     DB::rollback();
                    //     return Handles::jsonResponse(false, 'Dados do tempo de entrega do prestador incorretos, verifique os campos', [], 422);
                    // }   
                    // DB::commit();
                    // return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $response, 200);
                    $gruposPrestador = $payload['LISTADEGRUPOPRESTADOR'];
                    $pessoaPrestador = $gruposPrestador[0]['ID_PESSOA_PESSOAPRESTADOR'];

                    // delete grupos prestador
                    // $model = "App\\Models\\".$table;
                    $status = $model::where('ID_PESSOA_PESSOAPRESTADOR', $pessoaPrestador)->delete();

                    $response = true;
                    // grava prestador
                    for ($i=0; $i<count($gruposPrestador); $i++) {
                      $query = new $model();
                      if ($gruposPrestador[$i]['ATIVO'] == TRUE){
                          $gruposPrestador[$i]['ID_GRUPO'] = $gruposPrestador[$i]['GRUPO'];
                          $query->fill($gruposPrestador[$i]);
                          $response = $query->save();
                        }
                    }
                    if(!$response){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Dados do tempo de entrega do prestador incorretos, verifique os campos', [], 422);
                    }   
                    DB::commit();
                    return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $response, 200);
                break;
                case 'prestadortempoentrega':
                    // $temposEntregaPrestador = $payload['LISTADETEMPOSDEENTREGA'];
                    // $pessoaPrestador = $temposEntregaPrestador[0]['ID_PESSOA_PESSOAPRESTADOR'];
                    // $response = true;
                    // if(count($temposEntregaPrestador) != DB::table('FAT_TEMPO_ENTREGA')->count())
                    //     return Handles::jsonResponse(false, 'O número de tempos personalizados ('.count($temposEntregaPrestador).') não bate com o número de tempos existentes ('.DB::table('FAT_TEMPO_ENTREGA')->count().').', $temposEntregaPrestador, 422);
                    // for ($i=0; $i<count($temposEntregaPrestador); $i++) {
                    //     if(!$response){
                    //         DB::rollback();
                    //         return Handles::jsonResponse(false, 'Dados do tempo de entrega do prestador incorretos, verifique os campos', [], 422);
                    //     }
                    //     $query = $model::where('ID_PESSOA_PESSOAPRESTADOR',$pessoaPrestador)
                    //         ->where('ID_TEMPOENTREGA',$temposEntregaPrestador[$i]['ID_TEMPOENTREGA'])
                    //         ->first();
                    //     if(!$query)
                    //         $query = new $model();
                    //     $query->fill($temposEntregaPrestador[$i]);
                    //     $response = $query->create();
                    // }
                    // if(!$response){
                    //     DB::rollback();
                    //     return Handles::jsonResponse(false, 'Dados do tempo de entrega do prestador incorretos, verifique os campos', [], 422);
                    // }   
                    // DB::commit();
                    // return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $response, 200);
                    $temposEntregaPrestador = $payload['LISTADETEMPOSDEENTREGA'];
                    $pessoaPrestador = $temposEntregaPrestador[0]['ID_PESSOA_PESSOAPRESTADOR'];

                    // delete tempo entrega prestador
                    //$status = $model::where('ID_PESSOA_PESSOAPRESTADOR', $pessoaPrestador)->delete();

                    $response = true;
                    for ($i=0; $i<count($temposEntregaPrestador); $i++) {
                      $query = new $model();
                      //$query->fill($temposEntregaPrestador[$i]);
                      // var_dump($temposEntregaPrestador[$i]);
                      //var_dump($temposEntregaPrestador);
                      $response = $query::updateOrCreate(
                        ['ID' => $temposEntregaPrestador[$i]['ID']],
                        [
                            'ID_PESSOA_PESSOAPRESTADOR' => $temposEntregaPrestador[$i]['ID_PESSOA_PESSOAPRESTADOR'],
                            'ID_TEMPOENTREGA' => $temposEntregaPrestador[$i]['ID_TEMPOENTREGA'],
                            'DESCONTO' => $temposEntregaPrestador[$i]['DESCONTO'],
                            'UNIDADE_DESCONTO' => $temposEntregaPrestador[$i]['UNIDADE_DESCONTO'],
                            'ATIVO' => $temposEntregaPrestador[$i]['ATIVO']
                        ]);
                      // if ($temposEntregaPrestador[$i]['ATIVO'] == TRUE){
                      //     $query->fill($temposEntregaPrestador[$i]);
                      //     $response = $query->save();
                      //   }
                    }
                    if(!$response){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Dados do tempo de entrega do prestador incorretos, verifique os campos', [], 422);
                    }   
                    DB::commit();
                    return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $response, 200);
                break;
                case 'enderecopessoa':
                    $favoritoAntigo = $model::where('ID_PESSOA',$payload['ID_PESSOA'])->where('FAVORITO',true)->first();
                    if(isset($payload['FAVORITO']) && isset($payload['ID_PESSOA'])){
                        if($payload['FAVORITO'] == true){
                            if($favoritoAntigo){
                                $favoritoAntigo->FAVORITO = false;
                                $responseRemoveFavoritoAntigo = $favoritoAntigo->save();
                                if(!$responseRemoveFavoritoAntigo)
                                    return Handles::jsonResponse(false, 'Erro ao atualizar o antigo endereço marcado como favorito', $favoritoAntigo, 422);
                            }
                        }
                    }
                    if(!$favoritoAntigo){
                        $favoritoForcado = true;
                        $payload['FAVORITO'] = true;
                    }
                    $query->fill($payload);

                    $sqlAPI_GOOGLE_KEY = "SELECT API_GOOGLE_KEY FROM AMB_PARAMETRO WHERE API_GOOGLE_KEY IS NOT NULL LIMIT 1";
                    $responseAPI_GOOGLE_KEY = DB::select($sqlAPI_GOOGLE_KEY);
                    if(!isset($responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY)){
                        return Handles::jsonResponse(false, 'Nenhuma chave de API do google maps configurada!', $responseAPI_GOOGLE_KEY, 400);
                    }
                    $APIKEY = $responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY;
                    $urlMapsGeocodingAPI = str_replace (" ","+","https://maps.googleapis.com/maps/api/geocode/json?address='{$payload['ENDERECO']},{$payload['NUMERO']},{$payload['CIDADE']},{$payload['UF']}&key={$APIKEY}");
                    
                    $googleAPI = curl_init();
                    curl_setopt($googleAPI, CURLOPT_URL, $urlMapsGeocodingAPI);
                    curl_setopt($googleAPI, CURLOPT_RETURNTRANSFER, true);
                    $googleAPI = json_decode(curl_exec($googleAPI));
                     
                    if(!isset($googleAPI->results[0])){
                        return Handles::jsonResponse(false, isset($googleAPI->status) ? $googleAPI->status : 'Endereço inválido!', $googleAPI, 400);
                    }

                    $query['LATITUDE'] = round($googleAPI->results[0]->geometry->location->lat, 7);
                    $query['LONGITUDE'] = round($googleAPI->results[0]->geometry->location->lng, 7);
                    
                    $query->save();
                    $query->getKey() ? DB::commit() : DB::rollback();
                    return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $query, 200);
                break;
                case 'pedidoavaliacao':
                    !isset($payload['RESPOSTA1']) ? $payload['RESPOSTA1'] = 0 : null;
                    !isset($payload['RESPOSTA2']) ? $payload['RESPOSTA2'] = 0 : null;
                    !isset($payload['RESPOSTA3']) ? $payload['RESPOSTA3'] = 0 : null;
                    if(!isset($payload['ID_PEDIDO'])){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Pedido não informado!', $payload, 404);
                    }
                    $pedido = $this->show('pedido',$payload['ID_PEDIDO'])->getData();
                    if(!$pedido){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Pedido não encontrado!', $payload, 405);
                    }
                    if(!$pedido->success){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Pedido não encontrado!', $payload, 406);
                    }
                    $idPessoaPrestador = isset($pedido->data->ID_PESSOA_PRESTADOR) ? $pedido->data->ID_PESSOA_PRESTADOR : false;
                    if($payload['RESPOSTA1'] < 0 || $payload['RESPOSTA1'] > 5){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Rating deve ser entre 0 e 5', $payload, 500);
                    }
                    if($payload['RESPOSTA2'] < 0 || $payload['RESPOSTA2'] > 5){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Rating deve ser entre 0 e 5', $payload, 501);
                    }
                    if($payload['RESPOSTA3'] < 0 || $payload['RESPOSTA3'] > 5){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Rating deve ser entre 0 e 5', $payload, 502);
                    }
                    $payload['MEDIA'] = round((($payload['RESPOSTA1'] + $payload['RESPOSTA2'] + $payload['RESPOSTA3']) / 3), 2);
                    $query->fill($payload);
                    $query->save();
                    if(!$query){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Erro ao salvar avaliação do pedido.', $payload, 503);
                    }
                    $prestador = new PrestadorController();
                    $responseRating = $prestador->updateRating($idPessoaPrestador)->getData();
                    if(!$responseRating){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Falha ao atualizar o rating do prestador!', $payload, 504);
                    }
                    if(!$responseRating->success){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Falha ao atualizar o rating do prestador!', $payload, 505);
                    }
                    DB::commit();
                    return Handles::jsonResponse(
                            true,
                            'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(),
                            array('updateAvaliacao' => $query, 'updatePrestador' => $responseRating),
                            200
                        );
                break;
                default:
                    $query->fill($payload);
                    $query->save();
                    DB::commit();
                    return Handles::jsonResponse(true, 'Registro inserido! Tabela '.$query->table.', '.$query->primaryKey.' '. $query->getKey(), $query, 200);
                break;
            }
        } catch (\Illuminate\Database\QueryException $exception) {
            DB::rollback();
            return Handles::jsonResponse(false, $exception->errorInfo[2], [], 405);
        } catch (\InvalidArgumentException $exception){
            DB::rollback();
            return Handles::jsonResponse(false, 'Dados inválidos, verifique os campos '.$exception->getMessage(), [], 422);
        } catch (\Throwable $exception){
            DB::rollback();
            return Handles::jsonResponse(false, 'Erro. '.$exception->getMessage(), [], $exception->getCode() ? $exception->getCode() : 402);
        }
    }
    
    public function update(Request $request, $table, $id){
        $model = "App\\Models\\".$table;
        $payload = $request->all();
        $payload['UPDATED_AT'] = date('Y-m-d h:i:s');
        try {
            $response = $model::find($id);
            if(!$response) {
                return Handles::jsonResponse(false, 'Registro não encontrado!', $response, 404);
            }
            switch($table){
                case 'pessoa':
                    if($response->TIPOPESSOA === 'P'){
                        DB::beginTransaction();

                        $response->fill($payload);
                        $responsePessoa = $response->save();

                        $modelPrestador = "App\\Models\\pessoaprestador";
                        $prestador = $modelPrestador::where('ID_PESSOA',$id)->first();
                        $prestador->fill($payload);
                        $responsePrestador = $prestador->save();

                        if(isset($payload['ENDERECO'])){
                            // busca latitude longitude
                            $sqlAPI_GOOGLE_KEY = "SELECT API_GOOGLE_KEY FROM AMB_PARAMETRO WHERE API_GOOGLE_KEY IS NOT NULL LIMIT 1";
                            $responseAPI_GOOGLE_KEY = DB::select($sqlAPI_GOOGLE_KEY);
                            if(!isset($responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY)){
                                return Handles::jsonResponse(false, 'Nenhuma chave de API do google maps configurada!', $responseAPI_GOOGLE_KEY, 400);
                            }
                            $APIKEY = $responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY;
                            $urlMapsGeocodingAPI = str_replace (" ","+","https://maps.googleapis.com/maps/api/geocode/json?address='{$payload['ENDERECO']},{$payload['NUMERO']},{$payload['CIDADE']},{$payload['UF']}&key={$APIKEY}");
                            
                            $googleAPI = curl_init();
                            curl_setopt($googleAPI, CURLOPT_URL, $urlMapsGeocodingAPI);
                            curl_setopt($googleAPI, CURLOPT_RETURNTRANSFER, true);
                            $googleAPI = json_decode(curl_exec($googleAPI));
                            
                            if(!isset($googleAPI->results[0])){
                                return Handles::jsonResponse(false, isset($googleAPI->status) ? $googleAPI->status : 'Endereço inválido!', $googleAPI, 400);
                            }
    
                            $payload['LATITUDE'] = round($googleAPI->results[0]->geometry->location->lat, 7);
                            $payload['LONGITUDE'] = round($googleAPI->results[0]->geometry->location->lng, 7);
                                                
                            $modelEndereco = "App\\Models\\enderecopessoa";
                            $endereco = $modelEndereco::where('ID_PESSOA',$id)->first();
                            $endereco->fill($payload);
                            $responseEndereco = $endereco->save();
                        }

                        // USUARIO
                        $modelUsuario = "App\\Models\\usuario";
                        $usuario = $modelUsuario::where('ID_PESSOA',$id)->first();
                        $usuario->fill($payload);
                        $responseUsuario = $usuario->save();

                        $response = array(
                            'pessoa' => $responsePessoa,
                            'prestador' => $responsePrestador,
                            'endereco' => isset($responseEndereco) ? $responseEndereco : null,
                            'usuario' => $responseUsuario
                        );
                        DB::commit();
                        return Handles::jsonResponse(true, 'Registro atualizado!', $response, 200);
                    }else{
                        $response->fill($payload);
                        $response->save();
                        return Handles::jsonResponse(true, 'Registro '.$response->getKey().' atualizado!', $response, 200);
                    }
                break;
                case 'enderecopessoa':
                    $favoritoAntigo = $model::where('ID_PESSOA',$response->ID_PESSOA)->where('FAVORITO',true)->where('ID','<>',$id)->first();
                    if(isset($payload['FAVORITO'])){
                        if($payload['FAVORITO'] == true){
                            if($favoritoAntigo){
                                if($favoritoAntigo->ID != $id){
                                    $favoritoAntigo->FAVORITO = false;
                                    $responseRemoveFavoritoAntigo = $favoritoAntigo->save();
                                    if(!$responseRemoveFavoritoAntigo){
                                        return Handles::jsonResponse(false, 'Erro ao atualizar o endereço.', $favoritoAntigo, 422);
                                    }
                                }
                            }
                        }
                    }
                    if(!$favoritoAntigo){
                        $favoritoForcado = true;
                        $payload['FAVORITO'] = true;
                    }
                    $response->fill($payload);

                    // $client = new Client();    
                    $sqlAPI_GOOGLE_KEY = "SELECT API_GOOGLE_KEY FROM AMB_PARAMETRO WHERE API_GOOGLE_KEY IS NOT NULL LIMIT 1";
                    $responseAPI_GOOGLE_KEY = DB::select($sqlAPI_GOOGLE_KEY);
                    if(!isset($responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY)){
                        return Handles::jsonResponse(false, 'Nenhuma chave de API do google maps configurada!', $responseAPI_GOOGLE_KEY, 400);
                    }
                    $APIKEY = $responseAPI_GOOGLE_KEY[0]->API_GOOGLE_KEY;
                    $urlMapsGeocodingAPI = str_replace (" ","+","https://maps.googleapis.com/maps/api/geocode/json?address='{$response->ENDERECO},{$response->NUMERO},{$response->CIDADE},{$response->UF}&key={$APIKEY}");
                    
                    // $googleAPI = $client->get($urlMapsGeocodingAPI);
                    // if($googleAPI->getStatusCode() != 200 || $googleAPI->getReasonPhrase() != 'OK'){
                    //     return Handles::jsonResponse(false, 'A API do Google não encontrou o endereço especificado. ', array('message'=>getReasonPhrase()), $googleAPI->getStatusCode());
                    // }
                    // $googleAPI = json_decode($googleAPI->getBody()->getContents());
                    // if($googleAPI->status == 'ZERO_RESULTS'){
                    //     return Handles::jsonResponse(false, 'A API do Google não encontrou o endereço especificado. ', array('message'=>$googleAPI->status), 404);
                    // }   

                    $googleAPI = curl_init();
                    curl_setopt($googleAPI, CURLOPT_URL, $urlMapsGeocodingAPI);
                    curl_setopt($googleAPI, CURLOPT_RETURNTRANSFER, true);
                    $googleAPI = json_decode(curl_exec($googleAPI));

                    if(!isset($googleAPI->results[0])){
                        return Handles::jsonResponse(false, isset($googleAPI->status) ? $googleAPI->status : 'Endereço inválido!', $googleAPI, 400);
                    }

                    $response['LATITUDE'] = round($googleAPI->results[0]->geometry->location->lat, 7);
                    $response['LONGITUDE'] = round($googleAPI->results[0]->geometry->location->lng, 7);
                    $response->save();
                    return Handles::jsonResponse(true, 'Registro '.$response->getKey().' atualizado!', $response, 200);
                break;
                case 'pedidoavaliacao':
                    DB::beginTransaction();
                    if(isset($payload['ID_PEDIDO'])){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Não é permitido alterar o pedido!', $payload, 404);
                    }
                    $pedido = $this->show('pedido',$response->ID_PEDIDO)->getData();
                    if(!$pedido){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Pedido não encontrado!', $payload, 405);
                    }
                    if(!$pedido->success){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Pedido não encontrado!', $payload, 406);
                    }
                    $idPessoaPrestador = isset($pedido->data->ID_PESSOA_PRESTADOR) ? $pedido->data->ID_PESSOA_PRESTADOR : false;
                    !isset($payload['RESPOSTA1']) ? $payload['RESPOSTA1'] = $response->RESPOSTA1 : null;
                    !isset($payload['RESPOSTA2']) ? $payload['RESPOSTA2'] = $response->RESPOSTA2 : null;
                    !isset($payload['RESPOSTA3']) ? $payload['RESPOSTA3'] = $response->RESPOSTA3 : null;
                    if($payload['RESPOSTA1'] < 0 || $payload['RESPOSTA1'] > 5){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Rating deve ser entre 0 e 5', $payload, 500);
                    }
                    if($payload['RESPOSTA2'] < 0 || $payload['RESPOSTA2'] > 5){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Rating deve ser entre 0 e 5', $payload, 501);
                    }
                    if($payload['RESPOSTA3'] < 0 || $payload['RESPOSTA3'] > 5){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Rating deve ser entre 0 e 5', $payload, 502);
                    }
                    $payload['MEDIA'] = round((($payload['RESPOSTA1'] + $payload['RESPOSTA2'] + $payload['RESPOSTA3']) / 3), 2);
                    $response->fill($payload);
                    $response->save();
                    if(!$response)
                        return Handles::jsonResponse(false, 'Erro ao salvar avaliação do pedido.', $payload, 503);
                    $prestador = new PrestadorController();
                    $responseRating = $prestador->updateRating($idPessoaPrestador)->getData();
                    if(!$responseRating){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Falha ao atualizar o rating do prestador!', $payload, 504);
                    }
                    if(!$responseRating->success){
                        DB::rollback();
                        return Handles::jsonResponse(false, 'Falha ao atualizar o rating do prestador!', $responseRating, 505);
                    }
                    DB::commit();
                    return Handles::jsonResponse(
                            true,
                            'Registro inserido!',
                            array('updateAvaliacao' => $response, 'updatePrestador' => $responseRating),
                            200
                        );
                break;
                default:
                    $response->fill($payload);
                    $response->save();
                    return Handles::jsonResponse(true, 'Registro '.$response->getKey().' atualizado!', $response, 200);
                break;
            }            
        } catch (\Illuminate\Database\QueryException $exception) {
            // You can check get the details of the error using `errorInfo`:
            DB::transactionLevel() ? DB::rollback() : null;
            return Handles::jsonResponse(false, $exception->errorInfo[2], [], 405);
        } catch (\InvalidArgumentException $exception){
            DB::transactionLevel() ? DB::rollback() : null;
            return Handles::jsonResponse(false, 'Dados inválidos, verifique os campos.', $exception->getMessage(), 422);
        } catch (\GuzzleHttp\Exception\ClientException $exception){
            DB::transactionLevel() ? DB::rollback() : null;
            return Handles::jsonResponse(false, 'Erro na construção da URL da API do Google Maps.', $exception->getMessage(), 425);
        }
    }
}
