<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(array('prefix' => 'v1'), function(){
    Route::get('/', function () {
        return response()->json(['message' => 'Cleaner API', 'status' => 'Connected']);
    });

    //Route::resource('auth', 'UserAuthController');
    Route::post('/login', 'UserAuthController@login');
    Route::post('/loginPrestador', 'UserAuthController@loginPrestador');

    // Rotas default
    Route::delete('/default/{table}/{id}', 'DefaultController@destroy');
    Route::get('/default/{table}', 'DefaultController@index');
    Route::get('/default/{table}/{id}', 'DefaultController@show');
    Route::post('/default/{table}', 'DefaultController@store');
    Route::put('/default/{table}/{id}', 'DefaultController@update');
    
    // Rotas customizadas
    Route::get('/teste/{horas}', 'PedidoController@calculaDataPrevistaEntregaHorasCorridas');

    Route::get('/armarios/raio/{raio}/pessoa/endereco/{id}', 'LockerController@getArmariosRaioDoClienteEndereco');
    Route::get('/cupom/cliente/pessoa/{id}', 'PessoaController@getCupomCliente');
    Route::get('/feed/cliente/pessoa/{id}', 'PessoaController@getFeedCliente');
    Route::get('/fluxo/servico/{id}', 'PedidoController@getFluxoServico');
    Route::get('/fluxos', 'PedidoController@getFluxos');
    Route::get('/pedido/{id}', 'PedidoController@showPedidoDetalhado');
    Route::get('/pedido/{id}/cancelar', 'PedidoController@cancelarPedido');
    Route::get('/pedido/{id}/cancelavel', 'PedidoController@isCancelavel');
    Route::get('/pedido/{id}/comissao', 'PedidoController@calculaComissaoPedido');
    Route::get('/pedido/{id}/locker', 'PedidoController@getSenhaLocker');
    Route::get('/pedido/{id}/roupas', 'RoupaController@getRoupasPedido');
    Route::get('/pedido/{id}/servico/frete', 'PedidoController@getStatusServicoFrete');
    Route::get('/pedido/{id}/servico/roupa', 'PedidoController@getStatusServicoRoupa');
    Route::get('/pedido/{id}/servicos', 'PedidoController@getStatusServicos');
    Route::get('/pedido/pessoa/{id}', 'PedidoController@showPedidosCliente');
    Route::get('/pedido/prestador/pessoa/{id}', 'PedidoController@showPedidosPrestador');
    Route::get('/pedido/prestador/receber/{id}/{dataini}/{datafim}', 'PedidoController@showPedidosValorReceber');
    Route::get('/pessoa/enderecos/{id}', 'PessoaController@getEnderecosPessoa');        
    Route::get('/prestador/pessoa/{id}', 'PrestadorController@showPrestador');
    Route::get('/prestador/pessoa/{id}/avaliacao/{ano}/{mes}', 'PedidoController@getAvaliacao');
    Route::get('/prestador/pessoa/{id}/grupo', 'PrestadorController@showPrestadorGrupo');
    Route::get('/prestador/pessoa/{id}/grupo/tempos', 'PrestadorController@showGruposTemposMinimosPrestador');    
    Route::get('/prestador/pessoa/{id}/rating', 'PrestadorController@getRating');
    Route::get('/prestador/pessoa/{id}/roupas', 'PrestadorController@calculaValoresRoupasAtendidasPeloPrestador');
    Route::get('/prestador/pessoa/{id}/tempos', 'PrestadorController@showPrestadorTempoEntrega');
    Route::get('/prestador/verifica/grupos/pessoa/{id}', 'PrestadorController@prestadorPossuiGruposDesatualizados');
    Route::get('/prestador/verifica/tempos/pessoa/{id}', 'PrestadorController@prestadorPossuiTemposDesatualizados');  
    Route::get('/prestadores/raio/{raio}/pessoa/endereco/{id}', 'PrestadorController@getPrestadoresRaioDoClienteEndereco');
    Route::get('/roupa', 'RoupaController@index');
    Route::get('/roupa/{id}', 'RoupaController@show');
    Route::post('/locker/filtro', 'PessoaController@getLockerFiltro');
    Route::post('/prestador/calculo', 'PedidoController@calculaTotalPedido');
    Route::post('/prestador/pessoa/{id}/roupas/tempoMinimo', 'PrestadorController@getTempoMinimoPrestadorParaListaDeRoupasEspecificada');
    Route::post('/prestadores/filtro', 'PessoaController@getPrestadoresFiltro');
    Route::post('/prestadores/filtro/admin', 'PessoaController@getPrestadoresFiltro_admin');
    Route::put('/pedido/{id}/servico/frete/decrementar', 'PedidoController@decrementServicoFrete');
    Route::put('/pedido/{id}/servico/frete/incrementar', 'PedidoController@incrementServicoFrete');
    Route::put('/pedido/{id}/servico/roupa/decrementar', 'PedidoController@decrementServicoRoupa');
    Route::put('/pedido/{id}/servico/roupa/incrementar', 'PedidoController@incrementServicoRoupa');
    Route::put('/pedido/{id}/status/{idStatus}', 'PedidoController@alteraStatusPedido');
    Route::put('/pedido/{id}/statusavaliacao/{idStatusAvaliacao}', 'PedidoController@alteraStatusAvaliacaoPedido');
    Route::put('/prestador/pessoa/{id}/rating', 'PrestadorController@updateRating');

});

Route::get('/', function () {
    return redirect('api');
});
// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });
